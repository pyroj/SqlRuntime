package pyroj.sqlruntime.operator;

import pyroj.sqlruntime.properties.logical.LogicalProperty;

public abstract class Operator {
	protected int _numChildren;

	protected Operator(int numChildren) {
		this._numChildren = numChildren;
	}

	public int getNumChildren() {
		return this._numChildren;
	}

	abstract public void print(LogicalProperty schema);
}