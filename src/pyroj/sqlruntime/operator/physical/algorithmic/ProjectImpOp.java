package pyroj.sqlruntime.operator.physical.algorithmic;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.AttrMap_t;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class ProjectImpOp extends PhysicalOp {

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {
		// TODO: to make it at par with volcano, need to modify
	}

	private final AttrMap_t _projMap;

	public ProjectImpOp(AttrMap_t projMap) {
		super(1);
		_projMap = projMap;
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {
		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray(reqdProperties);
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": ProjectImpOp <");
		_projMap.print();
		System.out.print(">");
	}

}
