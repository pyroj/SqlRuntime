package pyroj.sqlruntime.operator.physical.algorithmic;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class SeqSelectOp extends PhysicalOp {

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {
		double outRelSize_Blocks = logProp.getNumBlocks();
		double inpRelSize_Blocks = childLogProp[0].getNumBlocks();

		boolean isPartitioned = !reqdProperties.isSerial();
		if (isPartitioned) {
			inpRelSize_Blocks = Math.ceil(inpRelSize_Blocks / Config.getDegreeOfParallelism());
			outRelSize_Blocks = Math.ceil(outRelSize_Blocks / Config.getDegreeOfParallelism());
		}

		processingLatency.add((outRelSize_Blocks + inpRelSize_Blocks) * Config.getProcessingTimePerBlock());
	}

	private final Predicate _predicate;

	public SeqSelectOp(Predicate predicate) {
		super(1);
		_predicate = predicate;
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( reqdProperties );
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": SeqSelectOp <");
		_predicate.print();
		System.out.print(">");

	}

}
