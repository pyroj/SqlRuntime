package pyroj.sqlruntime.operator.physical.algorithmic;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

/**
 * Physical NestedLoopJoin node. TODO Currently, it is just a placeholder. Will
 * need to add more fields as required.
 */
public class NestedLoopJoinOp extends JoinImp {

	public static boolean canDeliverProperties(JoinOp joinOp, LogicalProperty logicalProperties,
			LogicalProperty[] childrenLogicalProperties, PhysicalProperties reqdProperties, int outerRelationIndex) {

		if (reqdProperties.getIndexColumns() != null)
			return false;

		if (joinOp.getLeftEqAttributeBnd() == null) {
			computeEquilityPredicate(joinOp, childrenLogicalProperties[0], childrenLogicalProperties[1]);
		}

		if (!canDeliverPartitionProperty(logicalProperties, childrenLogicalProperties[0], childrenLogicalProperties[1], joinOp.getLeftEqAttributeBnd(),
				joinOp.getRightEqAttributeBnd(), reqdProperties)) {

			return false;
		}
		
		if (!canDeliverSortProperty(logicalProperties,childrenLogicalProperties[outerRelationIndex], reqdProperties.getSortColumns()))
			return false;

		return true;

	}

	public static void computeLocalCost(Object logProp, Object[] childLogProp, Object reqdProperties,
			int outerRelationIndex, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		double[] childSizeTuples = new double[2];
		double[] childSizeBlocks = new double[2];

		boolean isPartitioned = !((PhysicalProperties) reqdProperties).isSerial();

		double outSize_Blocks = ((LogicalProperty) logProp).getNumBlocks();
		if (isPartitioned)
			outSize_Blocks = Math.ceil(outSize_Blocks / Config.getDegreeOfParallelism());

		for (int i = 0; i < 2; i++) {
			childSizeTuples[i] = ((LogicalProperty) childLogProp[i]).getNumTuples();
			childSizeBlocks[i] = ((LogicalProperty) childLogProp[i]).getNumBlocks();

			if (isPartitioned) {
				childSizeTuples[i] = Math.ceil(childSizeTuples[i] / Config.getDegreeOfParallelism());
				childSizeBlocks[i] = Math.ceil(childSizeBlocks[i] / Config.getDegreeOfParallelism());
			}
		}

		int innerRelationIndex = 0;
		if (outerRelationIndex == 0)
			innerRelationIndex = 1;

		double blocks_processed = 0;

		blocks_processed = Math.ceil(childSizeTuples[outerRelationIndex] * childSizeBlocks[innerRelationIndex]
				+ outSize_Blocks);

		processingLatency.add(blocks_processed * Config.getProcessingTimePerBlock());

		if (childSizeBlocks[innerRelationIndex] > Config.getNumBuffers() * 0.5) {
			double io_Blocks = (childSizeBlocks[outerRelationIndex] * childSizeBlocks[innerRelationIndex])
					/ (Config.getNumBuffers() - 1);
			double io_cost = io_Blocks * Config.getReadTime();
			diskLatency.add(io_cost);
		}
	}

	private final int _outerRelationIndex;

	public NestedLoopJoinOp(JoinOp joinOp, int outerRelationIndex) {

		super(joinOp);
		_outerRelationIndex = outerRelationIndex;

	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		if (_joinOp.getLeftEqAttributeBnd() == null) {
			computeEquilityPredicate(_joinOp, childrenLogicalProperties[0], childrenLogicalProperties[1]);
		}
		int[] leftEqPredBnd = _joinOp.getLeftEqAttributeBnd();
		int[] rightEqPredBnd = _joinOp.getRightEqAttributeBnd();


		// Check partition property.
		PhysicalProperties[] reqdChildPartProp = getChildPartitionProp(logicalProperties, childrenLogicalProperties[0], childrenLogicalProperties[1],
				leftEqPredBnd, rightEqPredBnd, reqdProperties);
		

		// Satisfied sort properties, if outer relation contains all sort
		// columns

		int[] reqdSortProp = reqdProperties.getSortColumns();
		if (reqdSortProp != null) {

			int[] reqdChildSortProp = logicalProperties.translateBinding(reqdSortProp, 
					childrenLogicalProperties[_outerRelationIndex]);

			reqdChildPartProp[_outerRelationIndex].setSortProperty(reqdChildSortProp);
		}

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( reqdChildPartProp[0], reqdChildPartProp[1] );
	}

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {
		computeLocalCost(logProp, childLogProp, reqdProperties, _outerRelationIndex, memoryLatency, processingLatency,
				diskLatency, networkLatency);
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": NestedLoopJoinOp <outerRel = " + _outerRelationIndex + ">");
		super.print();
	}

}
