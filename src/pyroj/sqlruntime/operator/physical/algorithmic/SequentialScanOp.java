package pyroj.sqlruntime.operator.physical.algorithmic;

import java.util.ArrayList;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.BaseRelLogProp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class SequentialScanOp extends PhysicalOp {

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		LogicalProperty outLP = logProp;

		boolean isPartitioned = !reqdProperties.isSerial();

		double relSize_Blocks = outLP.getNumBlocks();
		if (isPartitioned)
			relSize_Blocks = Math.ceil(relSize_Blocks / Config.getDegreeOfParallelism());

		diskLatency.add(Config.getReadTime() * relSize_Blocks);
		processingLatency.add((relSize_Blocks) * Config.getProcessingTimePerBlock());
	}

	private final String _relationName;

	public SequentialScanOp(String relationName) {
		super(0);
		_relationName = relationName;
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		return null;
	}

	public static boolean canDeliverProperties(String relationName, Object logicalProperties, Object reqdProperties) {
		BaseRelLogProp relLogProp = Config.getCatalog().GetRelLogProp(relationName);

		ArrayList<PhysicalProperties> deliveredProperties = relLogProp.getPhysicalProperties();
		for (int i = 0; i < deliveredProperties.size(); i++) {
			if (deliveredProperties.get(i).covers((PhysicalProperties) reqdProperties))
				return true;
		}

		return false;
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": SequentialScanOp <" + _relationName + ">");

	}

}
