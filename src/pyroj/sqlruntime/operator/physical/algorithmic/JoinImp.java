package pyroj.sqlruntime.operator.physical.algorithmic;

import java.util.ArrayList;

import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;
import pyroj.sqlruntime.properties.physical.PhysicalProperties.PartitionType;

public abstract class JoinImp extends PhysicalOp {

	static final ArrayList<Integer> LeftBinding = new ArrayList<Integer>();
	static final ArrayList<Integer> RightBinding = new ArrayList<Integer>();

	protected static void computeEquilityPredicate(JoinOp joinOp, LogicalProperty leftSchema, LogicalProperty rightSchema) {
		// Haven't computed yet.

		LeftBinding.clear();
		RightBinding.clear();
		joinOp.getJoinPredicate().findEqPredicateBinding(leftSchema, rightSchema, LeftBinding, RightBinding);

		int numPred = LeftBinding.size();
		int[] leftEqPredBnd = new int[numPred];
		int[] rightEqPredBnd = new int[numPred];

		for (int i = 0; i < numPred; i++) {
			leftEqPredBnd[i] = LeftBinding.get(i);
			rightEqPredBnd[i] = RightBinding.get(i);
		}

		joinOp.setEqAttributeBnd(leftEqPredBnd, rightEqPredBnd);

	}

	/**
	 * Child must be partitioned on subset of equijoin columns (intersection of
	 * reqdPartProp & equijoin columns)
	 * 
	 * @param outSchema
	 * @param leftSchema
	 * @param rightSchema
	 * @param equiJoinColumns
	 * @param reqdPartProp
	 * @return
	 */
	protected static PhysicalProperties[] getChildPartitionProp(LogicalProperty outSchema, LogicalProperty leftSchema,
			LogicalProperty rightSchema, int[] leftEqPredBnd, int[] rightEqPredBnd, PhysicalProperties reqdPartProp) {

		PartitionType reqdPartitionType = reqdPartProp.getPartitionType();

		boolean isEquijoin = leftEqPredBnd.length > 0;

		if (reqdPartProp.isSerial()) {
			return new PhysicalProperties[] { new PhysicalProperties(true), new PhysicalProperties(true) };
		}
		else if (reqdPartitionType == PartitionType.RANDOM) {
			// TODO: If non equi-join, One input should be random and other
			// should be
			// serial.
			// both input must be partitioned on equijoin columns.
			if (isEquijoin) {
				int numJoinAttrs = leftEqPredBnd.length;

				int[] leftkeys = new int[numJoinAttrs];
				int[] rightkeys = new int[numJoinAttrs];
				for (int i = 0; i < numJoinAttrs; i++) {
					leftkeys[i] = leftEqPredBnd[i];
					rightkeys[i] = rightEqPredBnd[i];
				}
				return new PhysicalProperties[] { new PhysicalProperties(leftkeys, 1, PartitionType.HASH),
						new PhysicalProperties(rightkeys, 1, PartitionType.HASH) };
			}

			return null;
		}

		else {
			int numJoinAttrs = leftEqPredBnd.length;

			if (numJoinAttrs <= 0)
				return null;

			int[] reqdPartitionKey = reqdPartProp.getPartitionKeys();
			int numPartColumns = reqdPartitionKey.length;
			int minNumPartColumns = reqdPartProp.getMinNumPartitionKeys();

			if (numJoinAttrs < minNumPartColumns)
				return null;

			int[] leftkeys = new int[numPartColumns];
			int[] rightkeys = new int[numPartColumns];
			int numKeys = 0;

			for (int i = 0; i < numPartColumns; i++) {

				Attribute partAttr = outSchema.getAttribute(reqdPartitionKey[i]);
				boolean found = false;

				for (int j = 0; j < numJoinAttrs; j++) {
					Attribute leftJoinAttribute = leftSchema.getAttribute(leftEqPredBnd[j]);
					Attribute righJoinAttribute = rightSchema.getAttribute(rightEqPredBnd[j]);

					if (leftJoinAttribute.isEquivalent(partAttr) || righJoinAttribute.isEquivalent(partAttr)) {
						leftkeys[numKeys] = leftEqPredBnd[j];
						rightkeys[numKeys] = rightEqPredBnd[j];
						numKeys++;
						found = true;
					}
				}

				// RANGE partition use use prefix of partition keys with no
				// holes
				if (!found && reqdPartitionType == PartitionType.RANGE)
					break;
			}

			if (numKeys < minNumPartColumns)
				return null;

			if (numKeys < leftkeys.length) {
				int[] na1 = new int[numKeys];
				int[] na2 = new int[numKeys];

				for (int i = 0; i < numKeys; i++) {
					na1[i] = leftkeys[i];
					na2[i] = rightkeys[i];
				}

				leftkeys = na1;
				rightkeys = na2;
			}

			return new PhysicalProperties[] { new PhysicalProperties(leftkeys, minNumPartColumns, reqdPartitionType),
					new PhysicalProperties(rightkeys, minNumPartColumns, reqdPartitionType) };

		}
	}

	/**
	 * Child must be partitioned on subset of equijoin columns (intersection of
	 * reqdPartProp & equijoin columns)
	 * 
	 * @param outSchema
	 * @param leftSchema
	 * @param rightSchema
	 * @param equiJoinColumns
	 * @param reqdPartProp
	 * @return
	 */
	protected static boolean canDeliverPartitionProperty(LogicalProperty outSchema, LogicalProperty leftSchema, LogicalProperty rightSchema,
			int[] leftEqPredBnd, int[] rightEqPredBnd, PhysicalProperties reqdPartProp) {

		if (reqdPartProp.isSerial())
			return true;

		if (leftEqPredBnd.length <= 0)
			return false;

		PartitionType reqdPartitionType = reqdPartProp.getPartitionType();

		// Support partition only on equijoin columns.
		if (reqdPartitionType == PartitionType.RANDOM)
			return true;

		int numJoinAttrs = leftEqPredBnd.length;

		int minNumPartColumns = reqdPartProp.getMinNumPartitionKeys();

		if (numJoinAttrs < minNumPartColumns)
			return false;

		int[] reqdPartitionKey = reqdPartProp.getPartitionKeys();
		int numPartColumns = reqdPartitionKey.length;

		int numKeys = 0;

		for (int i = 0; i < numPartColumns; i++) {

			Attribute partAttr = outSchema.getAttribute(reqdPartitionKey[i]);
			boolean found = false;

			for (int j = 0; j < numJoinAttrs; j++) {
				Attribute leftJoinAttribute = leftSchema.getAttribute(leftEqPredBnd[j]);
				Attribute righJoinAttribute = rightSchema.getAttribute(rightEqPredBnd[j]);

				if (leftJoinAttribute.isEquivalent(partAttr) || righJoinAttribute.isEquivalent(partAttr)) {
					numKeys++;
					found = true;
				}
			}

			// RANGE partition use use prefix of partition keys with no
			// holes
			if (!found && reqdPartitionType == PartitionType.RANGE)
				break;
		}

		if (numKeys < minNumPartColumns)
			return false;

		return true;
	}

	/**
	 * Child must be sorted on subset of equi-join columns. ReqdSortOrder must
	 * be subset of equi-join columns.
	 * 
	 * @param outLP
	 * @param leftChildLP
	 * @param rightChildLP
	 * @param reqdSortProp
	 * @param equiJoinColumns
	 * @return
	 */
	static protected boolean canDeliverSortProp(LogicalProperty outSchema, LogicalProperty leftSchema, LogicalProperty rightSchema,
			int[] reqdSortProp, int[] leftEqPredBnd, int[] rightEqPredBnd) {

		if (reqdSortProp == null)
			return true;

		int numJoinAttrs = leftEqPredBnd.length;

		if (numJoinAttrs <= 0)
			return false;

		// must be sorted on subset of equi-join columns.
		int numReqdSortAttr = reqdSortProp.length;

		if (numReqdSortAttr > numJoinAttrs)
			return false;

		int[] reqdSortOrder = reqdSortProp;

		for (int i = 0; i < numReqdSortAttr; i++) {
			Attribute sortAttr = outSchema.getAttribute(reqdSortOrder[i]);

			boolean attrFound = false;

			for (int j = 0; j < numJoinAttrs; j++) {
				Attribute leftJoinAttribute = leftSchema.getAttribute(leftEqPredBnd[j]);
				Attribute righJoinAttribute = rightSchema.getAttribute(rightEqPredBnd[j]);

				if (leftJoinAttribute.isEquivalent(sortAttr) || righJoinAttribute.isEquivalent(sortAttr)) {
					attrFound = true;
				}
			}

			if (!attrFound)
				return false;
		}

		return true;

	}

	static protected boolean canDeliverSortProperty(LogicalProperty outSchema, LogicalProperty outerChildSchema, int[] reqdSortProp) {

		if (reqdSortProp == null)
			return true;

		for (int i = 0; i < reqdSortProp.length; i++) {
			Attribute attribute = outSchema.getAttribute(reqdSortProp[i]);
			int childBnd = outerChildSchema.getAttributeIndex(attribute);
			if (childBnd < 0)
				return false;
		}
		return true;
	}
	/**
	 * Child must be sorted on subset of equi-join columns. ReqdSortOrder must
	 * be subset of equi-join columns.
	 * 
	 * @param outLP
	 * @param leftChildLP
	 * @param rightChildLP
	 * @param reqdSortProp
	 * @param equiJoinColumns
	 * @return
	 */
	static protected int[][] getChildSortProp(LogicalProperty outSchema, LogicalProperty leftSchema, LogicalProperty rightSchema,
			int[] reqdSortProp, int[] leftEqPredBnd, int[] rightEqPredBnd) {

		int numJoinAttrs = leftEqPredBnd.length;

		if (numJoinAttrs <= 0)
			return null;

		// sort order for the input

		if (reqdSortProp != null) {
			int numReqdSortAttr = reqdSortProp.length;

			if (numReqdSortAttr > numJoinAttrs)
				return null;

			int[] reqdSortOrder = reqdSortProp;
			int[] leftkeys = new int[numReqdSortAttr];
			int[] rightkeys = new int[numReqdSortAttr];

			for (int i = 0; i < numReqdSortAttr; i++) {
				Attribute sortAttr = outSchema.getAttribute(reqdSortOrder[i]);

				boolean attrFound = false;

				for (int j = 0; j < numJoinAttrs; j++) {
					Attribute leftJoinAttribute = leftSchema.getAttribute(leftEqPredBnd[j]);
					Attribute righJoinAttribute = rightSchema.getAttribute(rightEqPredBnd[j]);

					if (leftJoinAttribute.isEquivalent(sortAttr) || righJoinAttribute.isEquivalent(sortAttr)) {
						leftkeys[i] = leftEqPredBnd[j];
						rightkeys[i] = rightEqPredBnd[j];
						attrFound = true;
					}
				}

				if (!attrFound)
					return null;
			}

			return new int[][] { leftkeys, rightkeys };

		}
		else {
			// TODO: Should use all join attributes, for now to be at par with
			// old code, we are using only one.
			numJoinAttrs = 1;
			int[] leftkeys = new int[numJoinAttrs];
			int[] rightkeys = new int[numJoinAttrs];

			for (int i = 0; i < numJoinAttrs; i++) {
				leftkeys[i] = leftEqPredBnd[i];
				rightkeys[i] = rightEqPredBnd[i];
			}
			return new int[][] { leftkeys, rightkeys };
		}
	}

	protected JoinOp _joinOp;

	protected JoinImp(JoinOp joinOp) {
		super(2);
		_joinOp = joinOp;
	}

	protected void print() {
		System.out.print("<");
		_joinOp.getJoinPredicate().print();
		System.out.print(">");
	}
}
