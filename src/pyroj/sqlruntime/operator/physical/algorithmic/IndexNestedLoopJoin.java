package pyroj.sqlruntime.operator.physical.algorithmic;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.properties.logical.AttributeProperty;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class IndexNestedLoopJoin extends JoinImp {

	private final int _outerRelationIndex;

	public IndexNestedLoopJoin(JoinOp joinOp, int outerRelationIndex) {

		super(joinOp);
		_outerRelationIndex = outerRelationIndex;

	}

	public static boolean canDeliverProperties(JoinOp joinOp, LogicalProperty logicalProperties,
			LogicalProperty[] childrenLogicalProperties, PhysicalProperties reqdProperties, int outerRelationIndex) {

		if (reqdProperties.getIndexColumns() != null)
			return false;

		if (joinOp.getLeftEqAttributeBnd() == null) {
			computeEquilityPredicate(joinOp, childrenLogicalProperties[0], childrenLogicalProperties[1]);
		}

		// Non-equijoin is not supported in index nested loop join
		if (joinOp.getLeftEqAttributeBnd().length <= 0)
			return false;

		if (!canDeliverPartitionProperty(logicalProperties, childrenLogicalProperties[0], childrenLogicalProperties[1], joinOp.getLeftEqAttributeBnd(),
				joinOp.getRightEqAttributeBnd(), reqdProperties)) {

			return false;
		}

		if (!canDeliverSortProperty(logicalProperties,childrenLogicalProperties[outerRelationIndex], reqdProperties.getSortColumns()))
			return false;
		
		return true;

	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		PhysicalProperties phyReqdProps = reqdProperties;

		LogicalProperty outSchema = logicalProperties;
		LogicalProperty leftSchema = childrenLogicalProperties[0];
		LogicalProperty rightSchema = childrenLogicalProperties[1];

		int[] leftEqPredBnd = _joinOp.getLeftEqAttributeBnd();
		int[] rightEqPredBnd = _joinOp.getRightEqAttributeBnd();

		// Check partition property.
		PhysicalProperties[] reqdChildPartProp = getChildPartitionProp(outSchema, leftSchema, rightSchema,
				leftEqPredBnd, rightEqPredBnd, phyReqdProps);

		// Satisfied sort properties, if outer relation contains all sort
		// columns

		int[] reqdSortProp = phyReqdProps.getSortColumns();

		if (reqdSortProp != null) {
			int[] reqdChildSortProp = logicalProperties.translateBinding(reqdSortProp, 
					childrenLogicalProperties[_outerRelationIndex]);

			reqdChildPartProp[_outerRelationIndex].setSortProperty(reqdChildSortProp);
		}

		if (_outerRelationIndex == 0)
			reqdChildPartProp[1].setIndexProperty(new int[] { rightEqPredBnd[0] });
		else
			reqdChildPartProp[0].setIndexProperty(new int[] { leftEqPredBnd[0] });

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( reqdChildPartProp[0], reqdChildPartProp[1] );
	}

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		if (_joinOp.getLeftEqAttributeBnd() == null) {
			computeEquilityPredicate(_joinOp, childLogProp[0],
					childLogProp[0]);
		}

		LogicalProperty innerRelationLP = null;
		LogicalProperty outerRelationLP = null;
		int order = -1;

		if (_outerRelationIndex == 0) {
			outerRelationLP = childLogProp[0];
			innerRelationLP = childLogProp[1];
			order = _joinOp.getRightEqAttributeBnd()[0];
		}
		else {
			outerRelationLP = childLogProp[1];
			innerRelationLP = childLogProp[0];
			order = _joinOp.getLeftEqAttributeBnd()[0];
		}

		LogicalProperty outLP = logProp;

		double indexSize_Blocks = innerRelationLP.getNumBlocks();
		double probeSize_Tuples = outerRelationLP.getNumTuples();
		double outSize_Blocks = outLP.getNumBlocks();

		double indexFanout = Config.getIndexFanout();

		// Note: We are assuming input is pipelined -- a relscan below
		// ensures this if select is on a db relation, and accounts
		// for the read cost
		// If input rel << memory, assume it's read only once
		// But add an
		// else assume at least all two levels of index are in memory

		AttributeProperty indexAttrProp = innerRelationLP.getAttributeProperties(order);

		double indexDV = indexAttrProp.getNumberOfDistinctValues();
		if (indexDV <= 0)
			return;

		double probeIO_Blocks = (((0) > ((Math.ceil(Math.log(indexSize_Blocks) / Math.log(indexFanout))) - 1)) ? (0)
				: ((Math.ceil(Math.log(indexSize_Blocks) / Math.log(indexFanout))) - 1)) + indexSize_Blocks / indexDV;

		double io_Blocks = probeSize_Tuples * probeIO_Blocks;

		// Assume index got partially cached in mem if smaller than mem size
		// The smaller the index the more of it is already in memory

		if (indexSize_Blocks < Config.getNumBuffers() * 0.5
				&& io_Blocks > (indexSize_Blocks / Config.getNumBuffers()) * indexSize_Blocks)
			io_Blocks = (indexSize_Blocks / Config.getNumBuffers()) * indexSize_Blocks;

		double iocost = (Config.getSeekTime() + Config.getReadTime()) * io_Blocks;
		diskLatency.add(iocost);

		processingLatency.add(Math.ceil(probeSize_Tuples * 0.05 + outSize_Blocks) * Config.getProcessingTimePerBlock());
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": IndexNestedLoopJoinOp <outerRel = " + _outerRelationIndex + ">");
		super.print();
	}

}
