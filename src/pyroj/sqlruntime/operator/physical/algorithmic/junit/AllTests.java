package pyroj.sqlruntime.operator.physical.algorithmic.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NestedLoopJoinTestReqdProp.class })
public class AllTests {

}
