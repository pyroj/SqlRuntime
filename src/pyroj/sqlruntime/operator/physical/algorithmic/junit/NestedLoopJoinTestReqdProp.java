package pyroj.sqlruntime.operator.physical.algorithmic.junit;

import static org.junit.Assert.fail;

import org.junit.Test;

import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.junit.SqlTestUtil;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.physical.algorithmic.NestedLoopJoinOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;
import pyroj.sqlruntime.properties.physical.PhysicalProperties.PartitionType;

public class NestedLoopJoinTestReqdProp {

	@Test
	public void test() {
		PhysicalProperties[] childrenReqdProperties = new PhysicalProperties[2];
		PhysicalProperties[] childrenExpectedReqdProp = new PhysicalProperties[2];

		Predicate joinPred = SqlTestUtil.CreateAnd(
				SqlTestUtil.CreateEq(SqlTestUtil.CreateColRef("A", "a"), SqlTestUtil.CreateColRef("B", "a")),
				SqlTestUtil.CreateEq(SqlTestUtil.CreateColRef("B", "b"), SqlTestUtil.CreateColRef("A", "b")));

		NestedLoopJoinOp joinOp = new NestedLoopJoinOp(new JoinOp(joinPred), 0);

		LogicalProperty[] childrenLogicalProperties = new LogicalProperty[] {
				SqlTestUtil.CreateLogProperties("A", "a,b,c"), SqlTestUtil.CreateLogProperties("B", "a,b,c") };
		LogicalProperty joinProp = (new JoinOp(joinPred)).computeLogicalProperties(childrenLogicalProperties);

		PhysicalProperties serialProp = new PhysicalProperties(true);

		childrenReqdProperties =  joinOp.getChildReqdProperties(joinProp, childrenLogicalProperties, serialProp);
		if (joinOp.getChildReqdProperties(joinProp, childrenLogicalProperties, serialProp) == null)
			fail("Join should deliver serial property");

		PhysicalProperties randomPartProp = new PhysicalProperties(true);

		childrenReqdProperties = joinOp.getChildReqdProperties(joinProp, childrenLogicalProperties, randomPartProp) ;
		if (childrenReqdProperties == null)
			fail("Join should deliver random partition property");

		PhysicalProperties hashPartAa = new PhysicalProperties(new int[] { joinProp.getAttributeIndex("a", "A") }, 1,
				PartitionType.HASH);
		childrenExpectedReqdProp[0] = new PhysicalProperties(
				new int[] { childrenLogicalProperties[0].getAttributeIndex("a", "A") }, 1, PartitionType.HASH);
		childrenExpectedReqdProp[1] = new PhysicalProperties(
				new int[] { childrenLogicalProperties[1].getAttributeIndex("a", "B") }, 1, PartitionType.HASH);

		childrenReqdProperties = joinOp.getChildReqdProperties(joinProp, childrenLogicalProperties, hashPartAa);
		if (childrenReqdProperties == null)
			fail("Join should deliver hash partition on equi-join column");

		if (!childrenReqdProperties[0].isEquivalent(childrenExpectedReqdProp[0]))
			fail("Child reqd property is not correct");
		if (!childrenReqdProperties[1].isEquivalent(childrenExpectedReqdProp[1]))
			fail("Child reqd property is not correct");

		PhysicalProperties hashPartAaBbAc = new PhysicalProperties(new int[] { joinProp.getAttributeIndex("a", "A"),
				joinProp.getAttributeIndex("c", "A"), joinProp.getAttributeIndex("b", "B") }, 1,
				PartitionType.HASH, new int[] { joinProp.getAttributeIndex("b", "A") });
		childrenExpectedReqdProp[0] = new PhysicalProperties(new int[] { childrenLogicalProperties[0].getAttributeIndex("a", "A"),
				childrenLogicalProperties[0].getAttributeIndex("b", "A") }, 1, PartitionType.HASH,
				new int[] { childrenLogicalProperties[0].getAttributeIndex("b", "A") });
		childrenExpectedReqdProp[1] = new PhysicalProperties(new int[] { childrenLogicalProperties[1].getAttributeIndex("a", "B"),
				childrenLogicalProperties[1].getAttributeIndex("b", "B") }, 1, PartitionType.HASH);

		childrenReqdProperties = joinOp.getChildReqdProperties(joinProp, childrenLogicalProperties, hashPartAaBbAc);
		if (childrenReqdProperties == null)
			fail("Join should deliver hash partition on super set of equi-join column");

		if (!childrenReqdProperties[0].isEquivalent(childrenExpectedReqdProp[0])) {
			childrenReqdProperties[0].print(childrenLogicalProperties[0]);
			System.out.println();
			childrenExpectedReqdProp[0].print(childrenLogicalProperties[0]);
			fail("Child reqd property is not correct");
		}
		if (!childrenReqdProperties[1].isEquivalent(childrenExpectedReqdProp[1]))
			fail("Child reqd property is not correct");
	}
}
