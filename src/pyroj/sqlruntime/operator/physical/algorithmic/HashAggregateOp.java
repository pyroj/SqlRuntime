package pyroj.sqlruntime.operator.physical.algorithmic;

import java.util.ArrayList;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.AttrMap_t;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class HashAggregateOp extends AggregateImp {

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp,
			PhysicalProperties reqdProperties, PhysicalProperties[] childrenPhysicalProperties,
			MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {
		
		double outRelSize_Blocks = logProp.getNumBlocks();
		double inRelSize_Blocks = childLogProp[0].getNumBlocks();
		double inRelSize_Tuples = childLogProp[0].getNumTuples();

		boolean isPartitioned = !reqdProperties.isSerial();

		if (isPartitioned) {
			inRelSize_Tuples = Math.ceil(inRelSize_Tuples / Config.getDegreeOfParallelism());
			outRelSize_Blocks = Math.ceil(outRelSize_Blocks / Config.getDegreeOfParallelism());
		}

		double ioCost = 0;
		double blocksToProcess = Math.ceil(inRelSize_Tuples * 0.01 + outRelSize_Blocks);

		if (outRelSize_Blocks >= Config.getNumBuffers() * 0.5)
			ioCost = (Config.getReadTime() + Config.getWriteTime()) * (inRelSize_Blocks - 0.5 * Config.getNumBuffers());

		// hybrid hash -- assuming half of mem is in hybrid portion
		diskLatency.add(ioCost);
		processingLatency.add((blocksToProcess) * Config.getProcessingTimePerBlock());
	}

	public HashAggregateOp(AttrMap_t groupByAttrMap, AttrMap_t aggAttrMap) {
		super(groupByAttrMap, aggAttrMap);
	}

	@Override
	public ArrayList<Object[]> getChildReqdPropertiesArray(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties, PhysicalProperties reqdProperties) {
		ArrayList<Object[]> childReqdProperties = SqlRuntimeMemoryManager.getPhysicalPropertiesArrayList();
		ArrayList<PhysicalProperties> childrenReqdProperties = new ArrayList<PhysicalProperties>(); 
		getChildPartitionProp(logicalProperties, childrenLogicalProperties[0], reqdProperties, childrenReqdProperties);

		for (PhysicalProperties prop : childrenReqdProperties) {
			childReqdProperties.add(SqlRuntimeMemoryManager.getPhysicalPropertiesArray(prop));
		}
		return childReqdProperties;
	
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties,
			LogicalProperty[] childrenLogicalProperties, PhysicalProperties reqdProperties) {

		ArrayList<PhysicalProperties> childrenReqdProperties = new ArrayList<PhysicalProperties>(); 
		getChildPartitionProp(logicalProperties, childrenLogicalProperties[0], reqdProperties, childrenReqdProperties);

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( childrenReqdProperties.get(0) );
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(this._id + ": HashAggregateOp ");
		super.print();
	}
}
