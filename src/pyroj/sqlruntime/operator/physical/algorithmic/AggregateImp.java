package pyroj.sqlruntime.operator.physical.algorithmic;

import java.util.ArrayList;
import java.util.BitSet;

import pyroj.sqlruntime.expression.AttrMap_t;
import pyroj.sqlruntime.expression.Expression.ExpressionType;
import pyroj.sqlruntime.expression.Expression;
import pyroj.sqlruntime.expression.RelArgRef_t;
import pyroj.sqlruntime.operator.logical.AggregateOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;
import pyroj.sqlruntime.properties.physical.PhysicalProperties.PartitionType;

public abstract class AggregateImp extends PhysicalOp {

	protected final AttrMap_t _groupByAttrMap;

	protected final AttrMap_t _aggAttrMap;

	protected AggregateImp(AttrMap_t groupByAttrMap, AttrMap_t aggAttrMap) {
		super(1);
		_groupByAttrMap = groupByAttrMap;
		_aggAttrMap = aggAttrMap;
	}

	/**
	 * Input must be partitioned on subset of group by columns (intersection of
	 * group by columns and reqdPartition columns)
	 * 
	 * @param outLP
	 * @param childLP
	 * @param reqdPartition
	 * @return
	 */
	protected static boolean canDeliverSortProperty(AggregateOp aggOp, LogicalProperty outSchema, LogicalProperty childSchema,
			PhysicalProperties reqdProperties) {

		int[] reqdSortProp = reqdProperties.getSortColumns();
		if (reqdSortProp == null)
			return true;

		int numGroupByAttrs = aggOp.getAggAttrMap().NumProj();

		if (reqdSortProp.length > numGroupByAttrs)
			return false;

		BitSet groupByAttrFound = new BitSet(numGroupByAttrs);
		for (int i = 0; i < reqdSortProp.length; i++) {
			Attribute sortAttr = outSchema.getAttribute(reqdSortProp[i]);

			boolean attrFound = false;

			for (int j = 0; !attrFound && j < numGroupByAttrs; j++) {
				RelArgRef_t e = (RelArgRef_t) aggOp.getAggAttrMap().Entry(j).Expr();
				if (sortAttr.isEquivalent(e.AttrName(), e.RelName())) {
					attrFound = true;
					groupByAttrFound.set(j);
				}
			}

			if (!attrFound)
				return false;

		}

		return true;

	}

	/**
	 * Input must be partitioned on subset of group by columns (intersection of
	 * group by columns and reqdPartition columns)
	 * 
	 * @param outLP
	 * @param childLP
	 * @param reqdPartition
	 * @return
	 */
	public static boolean canDeliverPartitionProperty(AggregateOp aggOp, LogicalProperty outSchema, LogicalProperty childSchema,
			PhysicalProperties reqdProperties) {

		if (reqdProperties.isSerial()) {
			return true;
		}
		else if (reqdProperties.getPartitionType() == PartitionType.RANDOM)
			return true;
		else {

			int numGroupByAttrs = aggOp.getAggAttrMap().NumProj();

			if (numGroupByAttrs < reqdProperties.getMinNumPartitionKeys())
				return false;

			int[] partitionKeys = reqdProperties.getPartitionKeys();
			int numPartitionKeys = partitionKeys.length;
			int numKeys = 0;

			for (int i = 0; i < numPartitionKeys; i++) {

				Attribute partitionAttr = outSchema.getAttribute(partitionKeys[i]);

				boolean attrFound = false;

				for (int j = 0; !attrFound && j < numGroupByAttrs; j++) {
					RelArgRef_t e = (RelArgRef_t) aggOp.getAggAttrMap().Entry(j).Expr();
					if (partitionAttr.isEquivalent(e.AttrName(), e.RelName()))
						attrFound = true;
				}

				if (reqdProperties.getPartitionType() == PartitionType.RANGE && !attrFound)
					break;

			}

			if (numKeys < reqdProperties.getMinNumPartitionKeys())
				return false;

			return true;
		}

	}

	/**
	 * Input must be partitioned on subset of group by columns (intersection of
	 * group by columns and reqdPartition columns)
	 * 
	 * @param outLP
	 * @param childLP
	 * @param reqdPartition
	 * @return
	 */
	protected boolean getChildPartitionProp(LogicalProperty outSchema, LogicalProperty childSchema,
			PhysicalProperties reqdProperties, ArrayList<PhysicalProperties> outChildReqdProperties) {

		if (reqdProperties.isSerial()) {
			if (outChildReqdProperties != null)
				outChildReqdProperties.add(new PhysicalProperties(true));
			return true;
		}
		else if (reqdProperties.getPartitionType() == PartitionType.RANDOM) {
			// Must partition on subset of group by attribute

			int numGroupByAttrs = _groupByAttrMap.NumProj();
			int[] partitionKeys = new int[numGroupByAttrs];
			for (int i = 0; i < numGroupByAttrs; i++) {
				RelArgRef_t groupByAttr = (RelArgRef_t) _groupByAttrMap.Entry(i).Expr();
				partitionKeys[i] = childSchema.getAttributeIndex(groupByAttr.AttrName(), groupByAttr.RelName());
			}
			
			if (outChildReqdProperties != null)
				outChildReqdProperties.add(new PhysicalProperties(partitionKeys, 1, PartitionType.HASH));
			return true;
		}
		else {

			int numGroupByAttrs = _groupByAttrMap.NumProj();

			if (numGroupByAttrs < reqdProperties.getMinNumPartitionKeys())
				return false;

			int[] partitionKeys = reqdProperties.getPartitionKeys();
			int numPartitionKeys = partitionKeys.length;
			// sort order for the input
			int[] childOrder = new int[numPartitionKeys];
			int numKeys = 0;

			for (int i = 0; i < numPartitionKeys; i++) {

				Attribute partitionAttr = outSchema.getAttribute(partitionKeys[i]);

				boolean attrFound = false;

				for (int j = 0; !attrFound && j < numGroupByAttrs; j++) {
					RelArgRef_t e = (RelArgRef_t) _groupByAttrMap.Entry(j).Expr();
					if (partitionAttr.isEquivalent(e.AttrName(), e.RelName()))
						attrFound = true;
				}

				if (reqdProperties.getPartitionType() == PartitionType.RANGE && !attrFound)
					break;

				if (attrFound)
					childOrder[numKeys++] = childSchema.getAttributeIndex(partitionAttr);
			}

			if (numKeys < reqdProperties.getMinNumPartitionKeys())
				return false;

			if (outChildReqdProperties != null) {
				
				if(childOrder.length > 1 && reqdProperties.getMinNumPartitionKeys() <= 1) {
					for(int order : childOrder) {
						outChildReqdProperties.add(new PhysicalProperties(new int[] {order}, 1,
								reqdProperties.getPartitionType()));						
					}
					outChildReqdProperties.add(new PhysicalProperties(childOrder, 2, reqdProperties.getPartitionType()));
				}
				else {
					outChildReqdProperties.add(new PhysicalProperties(childOrder, reqdProperties.getMinNumPartitionKeys(),
							reqdProperties.getPartitionType()));
					
				}
			}
			return true;
		}

	}

	/**
	 * Input must be sorted on all group by columns and reqdSortColumns must be
	 * subset of group by columns.
	 * 
	 * @param outLP
	 * @param childLP
	 * @param reqdSortProp
	 * @return
	 */
	protected int[] getChildSortColumns(LogicalProperty outLP, LogicalProperty childLP, int[] reqdSortProp) {

		int numGroupByAttrs = _groupByAttrMap.NumProj();

		if (numGroupByAttrs <= 0)
			return null;

		// sort order for the input
		int[] childOrder = new int[numGroupByAttrs];

		if (reqdSortProp != null) {
			if (reqdSortProp.length > numGroupByAttrs)
				return null;

			BitSet groupByAttrFound = new BitSet(numGroupByAttrs);
			for (int i = 0; i < reqdSortProp.length; i++) {
				Attribute sortAttr = outLP.getAttribute(reqdSortProp[i]);

				boolean attrFound = false;

				for (int j = 0; !attrFound && j < numGroupByAttrs; j++) {
					RelArgRef_t e = (RelArgRef_t) _groupByAttrMap.Entry(j).Expr();
					if (sortAttr.isEquivalent(e.AttrName(), e.RelName())) {
						attrFound = true;
						groupByAttrFound.set(j);
					}
				}

				if (!attrFound)
					return null;

				childOrder[i] = childLP.getAttributeIndex(sortAttr);
			}

			for (int i = reqdSortProp.length; i < numGroupByAttrs; i++) {

				int j = groupByAttrFound.nextClearBit(0);
				RelArgRef_t e = (RelArgRef_t) _groupByAttrMap.Entry(j).Expr();
				childOrder[i] = childLP.getAttributeIndex(e.AttrName(), e.RelName());
			}
		}
		else {
			for (int i = 0; i < numGroupByAttrs; i++) {
				// bind the group-by expression with the input schema
				Expression e = _groupByAttrMap.Entry(i).Expr();

				// FIX: no complex expressions allowed
				assert e.getExprType() == ExpressionType.REL_ARG_REF_T;

				// attribute number in input schema bound to the $i$th group-by
				// attr
				childOrder[i] = ((RelArgRef_t) e).getBinding(childLP);
			}
		}

		return childOrder;
	}

	protected void print() {
		System.out.print("<");
		if (_groupByAttrMap != null)
			_groupByAttrMap.print();
		System.out.print(" | ");
		if (_aggAttrMap != null)
			_aggAttrMap.print();
		System.out.print(">");
	}
}
