package pyroj.sqlruntime.operator.physical.algorithmic;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class IndexSelectOp extends PhysicalOp {

	private final Predicate _predicate;
	private final int[] _indexColumns;
	
	public IndexSelectOp(Predicate predicate, int[] indexColumns) {
		super(1);
		_predicate = predicate;
		_indexColumns = indexColumns;
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		PhysicalProperties phyReqdProperties = reqdProperties;

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray(new PhysicalProperties(phyReqdProperties.isSerial(),
				phyReqdProperties.getPartitionKeys(), phyReqdProperties.getMinNumPartitionKeys(),
				phyReqdProperties.getPartitionType(), phyReqdProperties.getSortColumns(), _indexColumns));
	}

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		LogicalProperty childLP = childLogProp[0];
		LogicalProperty outLP = logProp;

		double inpRelSize_Blocks = childLP.getNumBlocks();
		double indexFanout = Config.getIndexFanout();

		double outRelSize_Blocks = outLP.getNumBlocks();

		double sel = 1.0;
		if (childLP.getNumTuples() > 0)
			sel = outLP.getNumTuples() / childLP.getNumTuples();

		// Note: We are assuming input is pipelined -- a relscan below
		// ensures this if select is on a db relation, and accounts
		// for the read cost

		// assume at least first level of index is in memory

		double inBlocks = Math.ceil(Math.log(inpRelSize_Blocks + 1) / Math.log(indexFanout));

		if (inBlocks < 0)
			inBlocks = 0;

		// If input rel << memory, assume lower levels are also partially cached
		// Not very satisfactory with nested queries, but we don't know how
		// many time select is called in that case, so we live with this.
		// To hack caching effects for nested queries, we assume cost is 0
		// if relation is small enough.

		if (inpRelSize_Blocks < Config.getNumBuffers() * 0.75)
			inBlocks = inBlocks * (inpRelSize_Blocks / Config.getNumBuffers());
		if (inpRelSize_Blocks < Config.getNumBuffers() * 0.25)
			inBlocks = 0; // Assume its entirely memory resident

		boolean isPartitioned = !reqdProperties.isSerial();
		if (isPartitioned) {
			inBlocks = Math.ceil(inBlocks / Config.getDegreeOfParallelism());
			inpRelSize_Blocks = Math.ceil(inpRelSize_Blocks / Config.getDegreeOfParallelism());
			outRelSize_Blocks = Math.ceil(outRelSize_Blocks / Config.getDegreeOfParallelism());
		}

		double iocost = (Config.getSeekTime() + Config.getReadTime()) * inBlocks + Config.getReadTime() * sel
				* inpRelSize_Blocks;

		diskLatency.add(iocost);
		processingLatency.add(outRelSize_Blocks * Config.getProcessingTimePerBlock());
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": IndexSelect <");
		_predicate.print();
		System.out.print(">");
	}

}
