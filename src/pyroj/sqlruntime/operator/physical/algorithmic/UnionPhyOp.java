package pyroj.sqlruntime.operator.physical.algorithmic;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class UnionPhyOp extends PhysicalOp {

	public enum PartProp {
		AllSerial, AllParallel, LeftSerial, RightSerial
	}

	private final PartProp _partProp;

	public UnionPhyOp(int numChildren, PartProp partProp) {
		super(numChildren);
		_partProp = partProp;
	}

	@Override
	public  PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		if (_numChildren == 1) {
			if (_partProp == PartProp.AllSerial) {
				return new PhysicalProperties[] { new PhysicalProperties(true) };
			}
			else if (_partProp == PartProp.AllParallel) {
				return new PhysicalProperties[] { new PhysicalProperties(false) };
			}
			else
				return null;
		}
		else if (_numChildren == 2) {
			if (_partProp == PartProp.AllSerial) {
				return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( new PhysicalProperties(true), new PhysicalProperties(true));
			}
			if (_partProp == PartProp.AllParallel) {
				return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( new PhysicalProperties(false), new PhysicalProperties(false));
			}
			if (_partProp == PartProp.LeftSerial) {
				return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( new PhysicalProperties(true), new PhysicalProperties(false));
			}
			if (_partProp == PartProp.RightSerial) {
				return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( new PhysicalProperties(false), new PhysicalProperties(true));
			}
		}
		return null;
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": UnionPhyOp");
		System.out.print(" <" + _partProp.toString() + ">");
	}

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {
	}

}
