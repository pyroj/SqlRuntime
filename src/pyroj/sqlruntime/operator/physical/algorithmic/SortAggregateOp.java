package pyroj.sqlruntime.operator.physical.algorithmic;

import java.util.ArrayList;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.AttrMap_t;
import pyroj.sqlruntime.operator.logical.AggregateOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class SortAggregateOp extends AggregateImp {

	public static boolean canDeliverProperties(AggregateOp aggOp, Object logicalProperties, Object[] childrenLogicalProperties, Object reqdProperties) {

		PhysicalProperties phyReqdProps = (PhysicalProperties) reqdProperties;
		if (phyReqdProps.getIndexColumns() != null)
			return false;

		LogicalProperty outSchema = (LogicalProperty) logicalProperties;
		LogicalProperty leftSchema = (LogicalProperty) childrenLogicalProperties[0];

		if (!canDeliverPartitionProperty(aggOp, outSchema, leftSchema, phyReqdProps)) {
			return false;
		}

		if (!canDeliverSortProperty(aggOp, outSchema, leftSchema, phyReqdProps)) {
			return false;
		}
		return true;
	}

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties, PhysicalProperties[] childrenPhysicalProperties,
			MutableDouble memoryLatency, MutableDouble processingLatency, MutableDouble diskLatency, MutableDouble networkLatency) {
		double outRelSize_Blocks = logProp.getNumBlocks();

		boolean isPartitioned = !reqdProperties.isSerial();

		if (isPartitioned) {
			outRelSize_Blocks = Math.ceil(outRelSize_Blocks / Config.getDegreeOfParallelism());
		}

		processingLatency.add((outRelSize_Blocks) * Config.getProcessingTimePerBlock());
	}

	public SortAggregateOp(AttrMap_t groupByAttrMap, AttrMap_t aggAttrMap) {
		super(groupByAttrMap, aggAttrMap);
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties, PhysicalProperties reqdProperties) {

		ArrayList<PhysicalProperties> childrenReqdProperties = new ArrayList<PhysicalProperties>(); 
		getChildPartitionProp(logicalProperties, childrenLogicalProperties[0], reqdProperties, childrenReqdProperties);

		int[] childSortProp = getChildSortColumns(logicalProperties, childrenLogicalProperties[0],
				reqdProperties.getSortColumns());

		childrenReqdProperties.get(0).setSortProperty(childSortProp);
		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( childrenReqdProperties.get(0));
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(this._id + ": SortAggregateOp ");
		super.print();
	}
}
