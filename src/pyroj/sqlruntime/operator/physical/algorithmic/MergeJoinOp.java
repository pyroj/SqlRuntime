package pyroj.sqlruntime.operator.physical.algorithmic;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.properties.logical.AttributeProperty;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class MergeJoinOp extends JoinImp {

	public static boolean canDeliverProperties(JoinOp joinOp, Object logicalProperties,
			Object[] childrenLogicalProperties, Object reqdProperties) {

		PhysicalProperties phyReqdProps = (PhysicalProperties) reqdProperties;
		if (phyReqdProps.getIndexColumns() != null)
			return false;

		LogicalProperty outSchema = (LogicalProperty) logicalProperties;
		LogicalProperty leftSchema = (LogicalProperty) childrenLogicalProperties[0];
		LogicalProperty rightSchema = (LogicalProperty) childrenLogicalProperties[1];

		if (joinOp.getLeftEqAttributeBnd() == null) {
			computeEquilityPredicate(joinOp, (LogicalProperty) childrenLogicalProperties[0], (LogicalProperty) childrenLogicalProperties[1]);
		}
		
		if(joinOp.getLeftEqAttributeBnd().length <= 0)
			return false;

		if (!canDeliverPartitionProperty(outSchema, leftSchema, rightSchema, joinOp.getLeftEqAttributeBnd(),
				joinOp.getRightEqAttributeBnd(), phyReqdProps)) {

			return false;
		}

		if (!canDeliverSortProp(outSchema, leftSchema, rightSchema, phyReqdProps.getSortColumns(),
				joinOp.getLeftEqAttributeBnd(), joinOp.getRightEqAttributeBnd())) {

			return false;
		}

		return true;
	}

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		LogicalProperty outSchema = logProp;
		LogicalProperty lchildSchema = childLogProp[0];
		LogicalProperty rchildSchema = childLogProp[1];

		if (_joinOp.getLeftEqAttributeBnd() == null) {
			computeEquilityPredicate(_joinOp, childLogProp[0], childLogProp[1]);
		}

		int[] leftEqPredBnd = _joinOp.getLeftEqAttributeBnd();
		int[] rightEqPredBnd = _joinOp.getRightEqAttributeBnd();

		// Does not support non-equi join
		if (leftEqPredBnd.length == 0) {
			memoryLatency.setValue(Double.MAX_VALUE);
			diskLatency.setValue(Double.MAX_VALUE);
			processingLatency.setValue(Double.MAX_VALUE);
			networkLatency.setValue(Double.MAX_VALUE);
			return;
		}

		PhysicalProperties reqdPhyProp = reqdProperties;
		int[][] childSortProp = getChildSortProp(outSchema, lchildSchema, rchildSchema, reqdPhyProp.getSortColumns(),
				leftEqPredBnd, rightEqPredBnd);

		if (childSortProp == null) {
			memoryLatency.setValue(Double.MAX_VALUE);
			diskLatency.setValue(Double.MAX_VALUE);
			processingLatency.setValue(Double.MAX_VALUE);
			networkLatency.setValue(Double.MAX_VALUE);
			return;
		}

		boolean isPartitioned = !reqdProperties.isSerial();

		for (int i = 0; i < 2; i++) {
			double childSizeBlocks = childLogProp[i].getNumBlocks();

			if (isPartitioned)
				childSizeBlocks = childSizeBlocks / Config.getDegreeOfParallelism();

			double childDistinctValues = 1;
			
			for (int j = 0; j < childSortProp[i].length; j++) {
				AttributeProperty sortAttrProp = childLogProp[i].getAttributeProperties(childSortProp[i][j]);
				childDistinctValues *= sortAttrProp.getNumberOfDistinctValues();
			}

			// Too many duplicates
			if (childSizeBlocks / childDistinctValues > Config.getNumBuffers() / 2) {
				memoryLatency.setValue(Double.MAX_VALUE);
				diskLatency.setValue(Double.MAX_VALUE);
				processingLatency.setValue(Double.MAX_VALUE);
				networkLatency.setValue(Double.MAX_VALUE);
				return;
			}
		}

		double outSize_Blocks = outSchema.getNumBlocks();
		if (isPartitioned)
			outSize_Blocks = outSize_Blocks / Config.getDegreeOfParallelism();

		processingLatency.add((2 * Math.ceil(outSize_Blocks)) * Config.getProcessingTimePerBlock());
		return;
	}

	public MergeJoinOp(JoinOp joinOp) {
		super(joinOp);
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		PhysicalProperties phyReqdProps = reqdProperties;
		if (phyReqdProps.getIndexColumns() != null)
			return null;

		LogicalProperty outSchema = logicalProperties;
		LogicalProperty leftSchema = childrenLogicalProperties[0];
		LogicalProperty rightSchema = childrenLogicalProperties[1];

		if (_joinOp.getLeftEqAttributeBnd() == null) {
			computeEquilityPredicate(_joinOp, childrenLogicalProperties[0], childrenLogicalProperties[1]);
		}

		// Check partition property.
		PhysicalProperties[] reqdChildPartProp = getChildPartitionProp(outSchema, leftSchema, rightSchema,
				_joinOp.getLeftEqAttributeBnd(), _joinOp.getRightEqAttributeBnd(), phyReqdProps);

		int[][] reqdChildSortProp = getChildSortProp(outSchema, leftSchema, rightSchema, phyReqdProps.getSortColumns(),
				_joinOp.getLeftEqAttributeBnd(), _joinOp.getRightEqAttributeBnd());

		reqdChildPartProp[0].setSortProperty(reqdChildSortProp[0]);
		reqdChildPartProp[1].setSortProperty(reqdChildSortProp[1]);

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( reqdChildPartProp[0], reqdChildPartProp[1] );
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": MergeJoinOp ");
		super.print();
	}

}
