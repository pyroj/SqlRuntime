package pyroj.sqlruntime.operator.physical.enforcer;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.AttributeProperty;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class SortOp extends PhysicalOp {

	public static Object getChildReqdProperties(Object reqdProperties) {
		PhysicalProperties reqdProp = (PhysicalProperties) reqdProperties;

		// Does not propagate index property.
		if (reqdProp.getIndexColumns() != null)
			return null;

		if (reqdProp.getSortColumns() != null) {
			return new PhysicalProperties(reqdProp.isSerial(), reqdProp.getPartitionKeys(),
					reqdProp.getMinNumPartitionKeys(), reqdProp.getPartitionType(), null, null);
		}

		return null;

	}

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		boolean isPartitioned = !reqdProperties.isSerial();

		// get the logical property of the input/output
		// schema property of the input/output

		double card = 1;
		for (int i = 0; i < _sortColumnBinding.length; i++) {
			final AttributeProperty p = logProp.getAttributeProperties(_sortColumnBinding[i]);

			card *= p.getNumberOfDistinctValues();
		}

		if (card <= 1) {
			return;
		}

		double relSize_Tuples = logProp.getNumTuples();

		double relSize_Blocks = logProp.getNumBlocks();

		if (isPartitioned) {
			relSize_Tuples = Math.ceil(relSize_Tuples / Config.getDegreeOfParallelism());
			relSize_Blocks = Math.ceil(relSize_Blocks / Config.getDegreeOfParallelism());
		}

		double io_cost = 0;
		double cpu_Blocks = 0;

		if (relSize_Blocks < Config.getNumBuffers()) {
			io_cost = 0;
		}
		else {
			// assuming each merge input has enough buffers so that seek time is
			// dwarfed by write+read time
			io_cost = (Config.getReadTime() + Config.getWriteTime())
					* relSize_Blocks
					* Math.ceil(Math.ceil(Math.log(Math.ceil(relSize_Blocks / Config.getNumBuffers()) + 1)
							/ Math.log(Config.getNumBuffers() - 1)));
		}
		cpu_Blocks = Math.ceil(Math.log(Math.ceil(relSize_Tuples) + 1) / Math.log(2.0)) * relSize_Blocks + 1;

		diskLatency.add(io_cost);
		processingLatency.add((cpu_Blocks) * Config.getProcessingTimePerBlock());
	}

	private final int[] _sortColumnBinding;

	public SortOp(Object reqdProperties) {
		super(1);

		_sortColumnBinding = ((PhysicalProperties) reqdProperties).getSortColumns();
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		PhysicalProperties reqdProp = reqdProperties;

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( new PhysicalProperties(reqdProp.isSerial(), reqdProp.getPartitionKeys(),
				reqdProp.getMinNumPartitionKeys(), reqdProp.getPartitionType(), null, null));
	}

	@Override
	// TODO Print requires schema
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": SortOp <");
		if (_sortColumnBinding != null && schema != null) {
			for (int i = 0; i < _sortColumnBinding.length; i++) {
				if (i > 0)
					System.out.print(", ");
				final Attribute a = schema.getAttribute(_sortColumnBinding[i]);
				System.out.print(a);
			}
		}
		System.out.print(">");
	}
}
