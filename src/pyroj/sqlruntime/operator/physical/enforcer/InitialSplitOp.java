package pyroj.sqlruntime.operator.physical.enforcer;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;
import pyroj.sqlruntime.properties.physical.PhysicalProperties.PartitionType;

public class InitialSplitOp extends PhysicalOp {

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		LogicalProperty inpLP = logProp;
		double relSize_Blocks = inpLP.getNumBlocks();

		int numPartitionsWithData = Config.getDegreeOfParallelism();

		/*
		 * Partition causes each rows with same key to go to same partitions.
		 * Thus number of partitions with data cannot be greater than number of
		 * distinct values of partition key. get the logical property of the
		 * input/output schema property of the input/output
		 */

		// If no key present then it is round robin partitioning which is always
		// balanced.
		if (_partitionKeys != null && _partitionKeys.length > 0) {

			double numDistinctValues = 1;
			for (int i = 0; i < _partitionKeys.length; i++)
				numDistinctValues *= inpLP.getAttributeProperties(_partitionKeys[i]).getNumberOfDistinctValues();

			if (numDistinctValues < numPartitionsWithData)
				numPartitionsWithData = (int) numDistinctValues;
		}

		double outPartSize = Math.ceil(relSize_Blocks / numPartitionsWithData);

		diskLatency.add(Config.getReadTime() * outPartSize);
		processingLatency.add((outPartSize) * Config.getProcessingTimePerBlock());
		networkLatency.add((Config.getDegreeOfParallelism() - 1) * Config.getVertexCreationTime());

		// input is serial and output is parallel
		double inputPartSize = Math.ceil(relSize_Blocks);
		double sendingTransferTime = Math.ceil(inputPartSize / Config.getDegreeOfParallelism())
				* Config.getDegreeOfParallelism() * Config.getTransferTime();
		double receiveTransferTime = Math.ceil(outPartSize / Config.getDegreeOfParallelism())
				* Config.getDegreeOfParallelism() * Config.getTransferTime();
		double transferTime = Math.max(sendingTransferTime, receiveTransferTime);
		networkLatency.add(transferTime);
	}

	private final PartitionType _partitionType;
	private final int[] _partitionKeys;

	public InitialSplitOp(Object reqdProperties) {
		super(1);

		_partitionType = ((PhysicalProperties) reqdProperties).getPartitionType();
		_partitionKeys = ((PhysicalProperties) reqdProperties).getPartitionKeys();
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		PhysicalProperties phyReqdProps = reqdProperties;

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray(new PhysicalProperties(true, phyReqdProps.getSortColumns()));
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": InitialSplit <");
		PhysicalProperties.printPartitionProp(_partitionType, _partitionKeys, schema);
		System.out.print(">");
	}

}
