package pyroj.sqlruntime.operator.physical.enforcer;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class FinalMergeOp extends PhysicalOp {

	public static Object getChildReqdProperties(Object reqdProperties) {
		PhysicalProperties phyReqdProps = (PhysicalProperties) reqdProperties;
		if (phyReqdProps.getIndexColumns() != null)
			return null;

		if (!phyReqdProps.isSerial())
			return null;

		return new PhysicalProperties(false, phyReqdProps.getSortColumns());
	}

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		PhysicalProperties phyReqdProps = reqdProperties;
		int[] sortOrderProp = phyReqdProps.getSortColumns();

		LogicalProperty inpLP = logProp;

		double relSize_Blocks = inpLP.getNumBlocks();
		double io_cost = Config.getReadTime() * relSize_Blocks;
		double blocksProcessed = relSize_Blocks;

		if (sortOrderProp != null) {

			double numDistinctSortKey = 1;
			for (int i = 0; i < sortOrderProp.length; i++) {
				numDistinctSortKey *= inpLP.getAttributeProperties(sortOrderProp[i]).getNumberOfDistinctValues();
			}

			double numDuplicateBlocks = relSize_Blocks / numDistinctSortKey;

			// This means a single value of input of the merge cannot fit in
			// memory
			if (numDuplicateBlocks > Config.getNumBuffers()) {
				memoryLatency.setValue(Double.MAX_VALUE);
				diskLatency.setValue(Double.MAX_VALUE);
				processingLatency.setValue(Double.MAX_VALUE);
				networkLatency.setValue(Double.MAX_VALUE);
				return;
			}
			else
				// 2 is multiplying factor for merging CPU cost.
				blocksProcessed = 2 * relSize_Blocks;

		}

		diskLatency.add(io_cost);
		processingLatency.add((blocksProcessed) * Config.getProcessingTimePerBlock());

		// input is parallel and output is serial
		double inputPartSize = Math.ceil(relSize_Blocks / Config.getDegreeOfParallelism());
		double sendingTransferTime = Math.ceil(inputPartSize / Config.getDegreeOfParallelism())
				* Config.getDegreeOfParallelism() * Config.getTransferTime();
		double receiveTransferTime = Math.ceil(relSize_Blocks / Config.getDegreeOfParallelism())
				* Config.getDegreeOfParallelism() * Config.getTransferTime();
		double transferTime = Math.max(sendingTransferTime, receiveTransferTime);
		networkLatency.add(transferTime);
	}

	public FinalMergeOp() {
		super(1);
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		PhysicalProperties phyReqdProps = reqdProperties;

		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( new PhysicalProperties(false, phyReqdProps.getSortColumns()) );
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": FinalMergeOp");
	}

}
