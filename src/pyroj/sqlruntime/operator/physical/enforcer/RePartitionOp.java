package pyroj.sqlruntime.operator.physical.enforcer;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;
import pyroj.sqlruntime.properties.physical.PhysicalProperties.PartitionType;

public class RePartitionOp extends PhysicalOp {

	@Override
	public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties,
			PhysicalProperties[] childrenPhysicalProperties, MutableDouble memoryLatency, MutableDouble processingLatency,
			MutableDouble diskLatency, MutableDouble networkLatency) {

		PhysicalProperties phyReqdProps = reqdProperties;
		int[] sortOrderProp = phyReqdProps.getSortColumns();

		LogicalProperty inpLP = logProp;

		double relSize_Blocks = inpLP.getNumBlocks();

		double ioCost = 0;
		double blocksProcessed = 0;

		// Compute partition prop. Partitioning cost is partitioning overhead +
		// serial scan of each input partition.
		// Blocks processed on input side.
		double inpPartSize_Blocks = Math.ceil(relSize_Blocks / Config.getDegreeOfParallelism());
		blocksProcessed += inpPartSize_Blocks;

		/*
		 * Partition causes each rows with same key to go to same partitions.
		 * Thus number of partitions with data cannot be greater than number of
		 * distinct values of partition key. get the logical property of the
		 * input/output schema property of the input/output
		 */
		int numOutPartitionsWithData = Config.getDegreeOfParallelism();

		double numDistinctValues = 1;
		if (_partitionKeys != null)
			for (int i = 0; i < _partitionKeys.length; i++)
				numDistinctValues *= inpLP.getAttributeProperties(_partitionKeys[i]).getNumberOfDistinctValues();

		if (numDistinctValues < numOutPartitionsWithData)
			numOutPartitionsWithData = (int) numDistinctValues;

		double outPartSize_Blocks = Math.ceil(relSize_Blocks / numOutPartitionsWithData);

		ioCost += Config.getReadTime() * outPartSize_Blocks;

		// Merging cost
		if (sortOrderProp != null) {

			double numDistinctSortKey = 1;
			for (int i = 0; i < sortOrderProp.length; i++) {
				numDistinctSortKey *= inpLP.getAttributeProperties(sortOrderProp[i]).getNumberOfDistinctValues();
			}

			double numDuplicateBlocks = relSize_Blocks / numDistinctSortKey;

			// This means a single value of input of the merge cannot fit in
			// memory
			if (numDuplicateBlocks > Config.getNumBuffers()) {
				memoryLatency.setValue(Double.MAX_VALUE);
				diskLatency.setValue(Double.MAX_VALUE);
				processingLatency.setValue(Double.MAX_VALUE);
				networkLatency.setValue(Double.MAX_VALUE);
				return;
			}
			else
				// 2 is multiplying factor for merging CPU cost.
				blocksProcessed += 2 * outPartSize_Blocks;

		}
		else {
			blocksProcessed += outPartSize_Blocks;
		}

		diskLatency.add(ioCost);
		processingLatency.add((blocksProcessed) * Config.getProcessingTimePerBlock());
		networkLatency.add((Config.getDegreeOfParallelism() - 1) * Config.getVertexCreationTime());

		// input is serial and output is parallel
		double inputPartSize = Math.ceil(relSize_Blocks / Config.getDegreeOfParallelism());
		double sendingTransferTime = Math.ceil(inputPartSize / Config.getDegreeOfParallelism())
				* Config.getDegreeOfParallelism() * Config.getTransferTime();
		double receiveTransferTime = Math.ceil(outPartSize_Blocks / Config.getDegreeOfParallelism())
				* Config.getDegreeOfParallelism() * Config.getTransferTime();
		double transferTime = Math.max(sendingTransferTime, receiveTransferTime);
		networkLatency.add(transferTime);
	}

	private final PartitionType _partitionType;
	private final int[] _partitionKeys;

	public RePartitionOp(Object reqdProperties) {
		super(1);
		_partitionType = ((PhysicalProperties) reqdProperties).getPartitionType();
		_partitionKeys = ((PhysicalProperties) reqdProperties).getPartitionKeys();
	}

	@Override
	public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties,
			PhysicalProperties reqdProperties) {

		PhysicalProperties phyReqdProps = reqdProperties;

		PhysicalProperties childProp = new PhysicalProperties(false, phyReqdProps.getSortColumns());
		return SqlRuntimeMemoryManager.getPhysicalPropertiesArray( childProp );
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": RePartitionOp <");
		PhysicalProperties.printPartitionProp(_partitionType, _partitionKeys, schema);
		System.out.print(">");
	}

}
