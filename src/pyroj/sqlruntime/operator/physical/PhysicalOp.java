package pyroj.sqlruntime.operator.physical;

import java.util.ArrayList;

import org.apache.commons.lang3.mutable.MutableDouble;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.Operator;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public abstract class PhysicalOp extends Operator {

	public static int NumInstances = 0;
	protected final int _id;

	public PhysicalOp(int numChildren) {
		super(numChildren);
		_id = NumInstances++;
	}

	public ArrayList<Object[]> getChildReqdPropertiesArray(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties, PhysicalProperties reqdProperties) {
		ArrayList<Object[]> childReqdProperties = SqlRuntimeMemoryManager.getPhysicalPropertiesArrayList();
		childReqdProperties.add(getChildReqdProperties(
							logicalProperties, 
							childrenLogicalProperties,
							reqdProperties));
		return childReqdProperties;
	
	}
	/**
	 * Return false if required properties cannot be satisfied.
	 * 
	 * @param logicalProperties
	 * @param childrenLogicalProperties
	 * @param reqdProperties
	 * @return
	 */
	abstract public PhysicalProperties[] getChildReqdProperties(LogicalProperty logicalProperties, LogicalProperty[] childrenLogicalProperties, PhysicalProperties reqdProperties);

	abstract public void computeLocalCost(LogicalProperty logProp, LogicalProperty[] childLogProp, PhysicalProperties reqdProperties, PhysicalProperties[] childrenPhysicalProperties,
			MutableDouble memoryLatency, MutableDouble processingLatency, MutableDouble diskLatency, MutableDouble networkLatency);
}
