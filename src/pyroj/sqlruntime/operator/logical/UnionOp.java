package pyroj.sqlruntime.operator.logical;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.properties.logical.GlobalMembersLogProp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

public class UnionOp extends LogicalOp {

	public final static int OperatorType = LogicalOp.UnionOpValue;

	public UnionOp(int numChildren) {
		super(numChildren);
		// TODO Auto-generated constructor stub
	}

	@Override
	public LogicalProperty computeLogicalProperties(Object[] childLogProp) {
		if (_numChildren == 1) {
			return SqlRuntimeMemoryManager.getIntermediateRelLogProp((LogicalProperty) childLogProp[0]);
		}
		else if (_numChildren == 2) {
			LogicalProperty leftLP = (LogicalProperty) childLogProp[0];
			LogicalProperty rightLP = (LogicalProperty) childLogProp[1];

			double relSize_Tuples = leftLP.getNumTuples() + rightLP.getNumTuples();

			LogicalProperty outLP = GlobalMembersLogProp.MergeLogicalProperties(leftLP, rightLP);

			outLP.setNumTuples(relSize_Tuples);
			return outLP;
		}

		return null;
	}

	@Override
	public int getOperatorType() {
		return OperatorType;
	}

	@Override
	public boolean isEqual(LogicalOp obj) {
		return true;
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": UnionOp");
	}
}
