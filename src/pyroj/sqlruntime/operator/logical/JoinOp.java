package pyroj.sqlruntime.operator.logical;

import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.properties.logical.GlobalMembersLogProp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

/**
 * Logical join node. TODO Currently, it is just a placeholder. Will need to add
 * more fields as required.
 */
public class JoinOp extends LogicalOp {

	public final static int OperatorType = LogicalOp.JoinOpValue;
	private final Predicate _joinPredicate;
	private int[] _leftEqAttributBnd = null;
	private int[] _rightEqAttributBnd = null;
	private boolean _disableTrasformation = false;

	public JoinOp(Predicate predicate) {
		super(2);
		_joinPredicate = predicate;
	}

	@Override
	public LogicalProperty computeLogicalProperties(Object[] childLogProp) {

		LogicalProperty leftLP = (LogicalProperty) childLogProp[0];

		LogicalProperty rightLP = (LogicalProperty) childLogProp[1];

		LogicalProperty outLP = GlobalMembersLogProp.MergeLogicalProperties(leftLP, rightLP);

		if (_joinPredicate != null)
			_joinPredicate.inferProp(outLP);

		return outLP;
	}

	public void disableTransformation() {
		_disableTrasformation = true;
	}

	public Predicate getJoinPredicate() {
		return _joinPredicate;
	}

	public int[] getLeftEqAttributeBnd() {
		return _leftEqAttributBnd;
	}

	@Override
	public int getOperatorType() {
		return OperatorType;
	}

	public int[] getRightEqAttributeBnd() {
		return _rightEqAttributBnd;
	}

	@Override
	public boolean isEqual(LogicalOp obj) {
		JoinOp other = (JoinOp) obj;
		if (!_joinPredicate.isEquivalent(other._joinPredicate))
			return false;
		return true;
	}

	public boolean isTransformationDisabled() {
		return _disableTrasformation;
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": JoinOp <");
		_joinPredicate.print();
		System.out.print(">");

	}

	public void setEqAttributeBnd(int[] leftEqPredBnd, int[] rightEqPredBnd) {
		_leftEqAttributBnd = leftEqPredBnd;
		_rightEqAttributBnd = rightEqPredBnd;
	}
}
