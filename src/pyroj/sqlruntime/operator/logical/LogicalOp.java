package pyroj.sqlruntime.operator.logical;

import pyroj.sqlruntime.operator.Operator;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

public abstract class LogicalOp extends Operator {

	protected static final int GetOpValue = 1;
	protected static final int SelectOpValue = 2;
	protected static final int ProjectOpValue = 3;
	protected static final int JoinOpValue = 4;
	protected static final int AppregateOpValue = 5;
	protected static final int UnionOpValue = 6;

	public static int NumInstances = 0;
	protected final int _id;

	LogicalOp(int numChildren) {
		super(numChildren);
		_id = NumInstances;
		NumInstances++;
	}

	@Override
	public int hashCode() {
		return getOperatorType();
	}

	public abstract LogicalProperty computeLogicalProperties(Object[] childLogProp);

	@Override
	public boolean equals(Object other) {

		if (!(other instanceof LogicalOp))
			return false;

		LogicalOp otherOp = (LogicalOp) other;

		if (_numChildren != otherOp._numChildren)
			return false;

		if (getOperatorType() != otherOp.getOperatorType())
			return false;

		if (!isEqual(otherOp))
			return false;
		return true;
	}

	public abstract int getOperatorType();

	abstract public boolean isEqual(LogicalOp obj);
}
