package pyroj.sqlruntime.operator.logical;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.AggExpr_t;
import pyroj.sqlruntime.expression.AttrMap_t;
import pyroj.sqlruntime.expression.Expression.ExpressionType;
import pyroj.sqlruntime.expression.Expression;
import pyroj.sqlruntime.expression.RelArgRef_t;
import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.AttributeProperty;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

/**
 * Logical aggregate node. TODO Currently, it is just a placeholder. Will need
 * to add more fields as required.
 */
public class AggregateOp extends LogicalOp {

	public final static int OperatorType = LogicalOp.AppregateOpValue;
	private AttrMap_t _groupByAttrMap;
	private AttrMap_t _aggAttrMap;

	public AggregateOp(AttrMap_t groupByAttrMap, AttrMap_t aggAttrMap) {
		super(1);
		_groupByAttrMap = groupByAttrMap;
		_aggAttrMap = aggAttrMap;
	}

	AggregateOp() {
		super(1);
	}

	@Override
	public LogicalProperty computeLogicalProperties(Object[] childLogProp) {

		LogicalProperty inpLP = (LogicalProperty) childLogProp[0];

		int numGroupByAttrs = _groupByAttrMap.NumProj();
		int numAggAttrs = _aggAttrMap.NumProj();

		int numAttrs = numGroupByAttrs + numAggAttrs;

		Attribute[] schema = new Attribute[numAttrs];

		int colIndex = 0;

		// group-by attributes
		for (int i = 0; i < numGroupByAttrs; i++) {
			// bind the project expression with the input schema
			Expression e = _groupByAttrMap.Entry(i).Expr();

			// FIX: no complex expressions allowed
			assert e.getExprType() == ExpressionType.REL_ARG_REF_T;

			// attribute number in input schema bound to the $i$th projection
			int bnd = ((RelArgRef_t) e).getBinding(inpLP);

			// set the schema attribute
			schema[colIndex] = inpLP.getAttribute(bnd).clone();
			colIndex++;
		}

		for (int i = 0; i < numAggAttrs; i++) {
			// bind the project expression with the input schema
			Expression e = _aggAttrMap.Entry(i).Expr();
			RelArgRef_t rInp;
			if (e instanceof RelArgRef_t)
				rInp = (RelArgRef_t) e;
			else {
				AggExpr_t aggExpr = (AggExpr_t) e;

				Expression eInp = aggExpr.Input();
				rInp = (RelArgRef_t) eInp;
			}

			// attribute number in input schema bound to the $i$th projection
			int bnd = rInp.getBinding(inpLP);

			// TODO :DIRTY FIX: we keep the name of the aggregate attribute as
			// the name of the aggregated attribute --- simplifies matters
			// for cascaded attributes. We assume only max/min type of
			// aggregations so that the maxval/minval/numDistinct properties
			// of the attribute remain the same after aggregation
			// Fixing this requires some thought --- will do it sometime

			// set the aggregated schema attribute
			// set the schema attribute
			schema[colIndex] = inpLP.getAttribute(bnd).clone();
			colIndex++;
		}

		// max rel size is the product of the distinct values of the
		// groupBy attributes
		double inpRelSize_Tuples = inpLP.getNumTuples();
		double relSize_Tuples = 1;
		for (colIndex = 0; colIndex < numGroupByAttrs; colIndex++) {
			AttributeProperty aProp = schema[colIndex].getProp();
			double aDistinct = aProp.getNumberOfDistinctValues();
			relSize_Tuples *= aDistinct;

			// output size should be at most input size
			if (relSize_Tuples > inpRelSize_Tuples) {
				relSize_Tuples = inpRelSize_Tuples;
				break;
			}
		}

		LogicalProperty outLP = SqlRuntimeMemoryManager.getIntermediateRelLogProp(schema, relSize_Tuples);
		outLP.setNumTuples(relSize_Tuples);

		return outLP;
	}

	public AttrMap_t getAggAttrMap() {
		return _aggAttrMap;
	}

	public AttrMap_t getGroupByAttr() {
		return _groupByAttrMap;
	}

	@Override
	public int getOperatorType() {
		return OperatorType;
	}

	@Override
	public boolean isEqual(LogicalOp obj) {
		AggregateOp other = (AggregateOp) obj;
		if (!_aggAttrMap.isEqualAsSet(other._aggAttrMap))
			return false;
		if (!_groupByAttrMap.isEqualAsSet(other._groupByAttrMap))
			return false;
		return true;
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": AggregateOp <");
		if (_groupByAttrMap != null)
			_groupByAttrMap.print();
		System.out.print(" | ");
		if (_aggAttrMap != null)
			_aggAttrMap.print();
		System.out.print(">");

	}

}
