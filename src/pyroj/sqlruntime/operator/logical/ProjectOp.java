package pyroj.sqlruntime.operator.logical;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.AttrMap_t;
import pyroj.sqlruntime.expression.Expression;
import pyroj.sqlruntime.expression.RelArgRef_t;
import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

/**
 * Logical aggregate node. TODO Currently, it is just a placeholder. Will need
 * to add more fields as required.
 */
public class ProjectOp extends LogicalOp {

	public final static int OperatorType = LogicalOp.ProjectOpValue;
	AttrMap_t _projMap;

	public ProjectOp(AttrMap_t projMap) {
		super(1);
		_projMap = projMap;
	}

	ProjectOp() {
		super(1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public LogicalProperty computeLogicalProperties(Object[] childLogProp) {

		LogicalProperty inpLP = (LogicalProperty) childLogProp[0];

		int numProj = _projMap.NumProj();

		Attribute[] schema = new Attribute[numProj];

		for (int i = 0; i < numProj; i++) {
			// bind the project expression with the input schema
			Expression e = _projMap.Entry(i).Expr();

			// FIX: only simple projection supported for now
			RelArgRef_t r = (RelArgRef_t) e;

			// attribute number in input schema bound to the $i$th projection
			int bnd = r.getBinding(inpLP);
			assert bnd != -1;

			// set the schema attribute
			Attribute a = inpLP.getAttribute(bnd).clone();
			schema[i] = a;
		}

		// rel size remains same
		double relSize_Tuples = ((LogicalProperty) childLogProp[0]).getNumTuples();
		LogicalProperty lp = SqlRuntimeMemoryManager.getIntermediateRelLogProp(schema, relSize_Tuples);

		return lp;
	}

	@Override
	public int getOperatorType() {
		return OperatorType;
	}

	public AttrMap_t getProjectMap() {
		return _projMap;
	}

	@Override
	public boolean isEqual(LogicalOp obj) {
		ProjectOp other = (ProjectOp) obj;
		if (!_projMap.isEqualAsSet(other._projMap))
			return false;
		return true;
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": ProjectOp <");
		_projMap.print();
		System.out.print(">");

	}
}
