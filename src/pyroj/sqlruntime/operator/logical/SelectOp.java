package pyroj.sqlruntime.operator.logical;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

/**
 * Logical aggregate node. TODO Currently, it is just a placeholder. Will need
 * to add more fields as required.
 */
public class SelectOp extends LogicalOp {
	private final Predicate _predicate;
	public final static int OperatorType = LogicalOp.SelectOpValue;

	public SelectOp(Predicate pred) {
		super(1);
		_predicate = pred;
	}

	@Override
	public LogicalProperty computeLogicalProperties(Object[] childLogProp) {

		LogicalProperty inpLP = (LogicalProperty) childLogProp[0];

		LogicalProperty outLP = SqlRuntimeMemoryManager.getIntermediateRelLogProp(inpLP);
		_predicate.inferProp(outLP);

		return outLP;
	}

	@Override
	public int getOperatorType() {
		return OperatorType;
	}

	public Predicate getPredicate() {
		return _predicate;
	}

	@Override
	public boolean isEqual(LogicalOp obj) {
		SelectOp other = (SelectOp) obj;
		if (!_predicate.isEquivalent(other._predicate))
			return false;
		return true;
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(_id + ": SelectOp <");
		_predicate.print();
		System.out.print(">");

	}
}
