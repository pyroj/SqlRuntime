package pyroj.sqlruntime.operator.logical;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

/**
 * Logical get node. TODO Currently, it is just a placeholder. Will need to add
 * more fields as required.
 */
public class GetOp extends LogicalOp {

	public final static int OperatorType = LogicalOp.GetOpValue;
	private final String _relationName;

	public GetOp(String relationName) {
		super(0);
		this._relationName = relationName;
	}

	@Override
	public LogicalProperty computeLogicalProperties(Object[] childLogProp) {
		return Config.getCatalog().GetRelLogProp(this._relationName);
	}

	@Override
	public int getOperatorType() {
		return OperatorType;
	}

	public String getRelationName() {
		return this._relationName;
	}

	@Override
	public boolean isEqual(LogicalOp obj) {
		GetOp other = (GetOp) obj;
		if (!this._relationName.equalsIgnoreCase(other._relationName))
			return false;
		return true;
	}

	@Override
	public void print(LogicalProperty schema) {
		System.out.print(this._id + ": GetOp <" + this._relationName + ">");
	}
}
