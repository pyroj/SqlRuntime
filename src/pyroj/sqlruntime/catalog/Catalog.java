package pyroj.sqlruntime.catalog;

import org.apache.commons.lang3.mutable.Mutable;

import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class Catalog {

	private Attribute_Catalog GetAttr(String relname, String attrname) {

		if (relname == null || attrname == null) {
			return null;
		}

		Schema_Def ptrSchema = Rel_Schemas;

		while (ptrSchema != null && !(ptrSchema.RelName.equals(relname)))
			ptrSchema = ptrSchema.NextRel;

		if (ptrSchema == null) {
			// added by prasan -- quick fix
			AddAttribute(attrname, relname);
			ptrSchema = Rel_Schemas;
			while (ptrSchema != null && !(ptrSchema.RelName.equals(relname)))
				ptrSchema = ptrSchema.NextRel;
		}

		Attribute_Catalog ptrAttribute = ptrSchema.Attributes;

		while (ptrAttribute != null
				&& !(ptrAttribute._attribute.isEquivalent(attrname, relname)))
			ptrAttribute = ptrAttribute.Next;
		if (ptrAttribute == null) {
			// added by prasan -- quick fix
			AddAttribute(attrname, relname);
			ptrAttribute = ptrSchema.Attributes;
			while (ptrAttribute != null
					&& !(ptrAttribute._attribute.isEquivalent(attrname, relname)))
				ptrAttribute = ptrAttribute.Next;
		}
		return ptrAttribute;
	}

	public Schema_Def Rel_Schemas = null;
	public CatalogInfoForRel Rel_Sizes = null;

	public Catalog() {
	}

	public void dispose() {
	}

	public int AddSchema(Schema_Def schema) {

		if (schema == null) {
			System.err.println("\nError: error in AddToCatalog"
					+ " input schema is NULL");
			return 0;
		}
		schema.NextRel = Rel_Schemas;
		Rel_Schemas = schema;
		return 1;
	}

	public Attribute_Catalog AddAttribute(String attrname, String relname) {
		return AddAttribute(attrname, relname, OAttrType.O_UNKNOWN_TYPE, 0);
	}

	public Attribute_Catalog AddAttribute(String attrname, String relname,
			OAttrType Type_a, int len_a) {

		if (relname == null)
			relname = "";

		Schema_Def PtrSchema = Rel_Schemas;

		while (PtrSchema != null && !(PtrSchema.RelName.equals(relname)))
			PtrSchema = PtrSchema.NextRel;

		if (PtrSchema != null) {
			Attribute_Catalog ptrAttributes = PtrSchema.Attributes;
			while (ptrAttributes != null) {
				if (ptrAttributes._attribute.isEquivalent(attrname, relname))
					return ptrAttributes;
				else
					ptrAttributes = ptrAttributes.Next;
			}
		}

		Attribute_Catalog newAttr = new Attribute_Catalog(Type_a, relname, attrname, len_a);

		if (PtrSchema != null) {
			newAttr.Next = PtrSchema.Attributes;
			PtrSchema.Attributes = newAttr;
		} else {
			Schema_Def newschema = new Schema_Def();
			newschema.RelName = relname;
			newschema.Attributes = newAttr;
			AddSchema(newschema);
		}
		return newAttr;
	}

	public Schema_Def GetRelSchema(Mutable<String> relname) {

		if (relname.getValue() == null)
			return null;

		if (relname.getValue() == null)
			return null;

		Schema_Def ptrSchema = Rel_Schemas;
		while (ptrSchema != null) {
			if (ptrSchema.RelName.equals(relname)) {
				return ptrSchema;
			}
			ptrSchema = ptrSchema.NextRel;
		}
		return null;
	}

	public int AddAttrSize(String relname, String attrname, int size) {
		assert size > 0;

		if (relname == null || attrname == null) {
			return (0);
		}

		Attribute_Catalog ptrAttribute = GetAttr(relname, attrname);

		if (ptrAttribute == null) {
			System.err
					.print("\nError: error in Catalog::AddAttrMinValue attribute with name "
							+ attrname
							+ "does not exist in the "
							+ " relation " + relname);
			return 0;
		}

		if (ptrAttribute.catalogInfo == null)
			ptrAttribute.catalogInfo = new CatalogInfoForAttr();

		ptrAttribute.catalogInfo.size = size;

		return 1;
	}

	public int AddAttrMinValue(String relname, String attrname, double minval) {

		if (relname == null || attrname == null) {
			return (0);
		}

		Attribute_Catalog ptrAttribute = GetAttr(relname, attrname);

		if (ptrAttribute == null) {
			System.err
					.print("\nError: error in Catalog::AddAttrMinValue attribute with name "
							+ attrname
							+ "does not exist in the "
							+ " relation " + relname);
			return 0;
		}

		if (ptrAttribute.catalogInfo == null)
			ptrAttribute.catalogInfo = new CatalogInfoForAttr();

		ptrAttribute.catalogInfo.minValue = minval;
		ptrAttribute.catalogInfo.minValueValid = 1;

		return 1;
	}

	public int AddAttrMaxValue(String relname, String attrname, double maxval) {

		if (relname == null || attrname == null) {
			return (0);
		}

		Attribute_Catalog ptrAttribute = GetAttr(relname, attrname);

		if (ptrAttribute == null) {
			System.err
					.print("\nError: error in Catalog::AddAttrMinValue attribute with name "
							+ attrname
							+ "does not exist in the "
							+ " relation " + relname);
			return 0;
		}

		if (ptrAttribute.catalogInfo == null)
			ptrAttribute.catalogInfo = new CatalogInfoForAttr();

		ptrAttribute.catalogInfo.maxValue = maxval;
		ptrAttribute.catalogInfo.maxValueValid = 1;

		return 1;
	}

	public int AddAttrHistBounds(String relname, String attrname,
			double[] histbounds) {

		if (relname == null || attrname == null) {
			return (0);
		}

		Attribute_Catalog ptrAttribute = GetAttr(relname, attrname);

		if (ptrAttribute == null) {
			System.err
					.print("\nError: error in Catalog::AddAttrHistBounds attribute with name "
							+ attrname
							+ "does not exist in the "
							+ " relation " + relname);
			return 0;
		}

		if (ptrAttribute.catalogInfo == null)
			ptrAttribute.catalogInfo = new CatalogInfoForAttr();

		ptrAttribute.catalogInfo.histBounds = histbounds;
		ptrAttribute.catalogInfo.histBoundsValid = 1;

		return 1;
	}

	public int AddAttrCommonVals(String relname, String attrname,
			double[] commonvals) {

		if (relname == null || attrname == null) {
			return (0);
		}

		Attribute_Catalog ptrAttribute = GetAttr(relname, attrname);

		if (ptrAttribute == null) {
			System.err
					.print("\nError: error in Catalog::AddAttrCommonVals attribute with name "
							+ attrname
							+ "does not exist in the "
							+ " relation " + relname);
			return 0;
		}

		if (ptrAttribute.catalogInfo == null)
			ptrAttribute.catalogInfo = new CatalogInfoForAttr();

		ptrAttribute.catalogInfo.commonVals = commonvals;
		ptrAttribute.catalogInfo.commonValsValid = 1;

		return 1;
	}

	public int AddAttrCommonFreqs(String relname, String attrname,
			double[] commonfreqs) {

		if (relname == null || attrname == null) {
			return (0);
		}

		Attribute_Catalog ptrAttribute = GetAttr(relname, attrname);

		if (ptrAttribute == null) {
			System.err
					.print("\nError: error in Catalog::AddAttrMinValue attribute with name "
							+ attrname
							+ "does not exist in the "
							+ " relation " + relname);
			return 0;
		}

		if (ptrAttribute.catalogInfo == null)
			ptrAttribute.catalogInfo = new CatalogInfoForAttr();

		ptrAttribute.catalogInfo.commonFreqs = commonfreqs;
		ptrAttribute.catalogInfo.commonFreqsValid = 1;

		return 1;
	}

	public int AddAttrDistinct(String relname, String attrname, int noofdistinct) {

		if (relname == null || attrname == null) {
			return (0);
		}

		Attribute_Catalog ptrAttribute = GetAttr(relname, attrname);

		if (ptrAttribute == null) {
			System.err
					.print("\nError: error in Catalog::AddAttrMinValue attribute with name "
							+ attrname
							+ "does not exist in the "
							+ " relation " + relname);
			return 0;
		}

		if (ptrAttribute.catalogInfo == null)
			ptrAttribute.catalogInfo = new CatalogInfoForAttr();

		ptrAttribute.catalogInfo.noOfDistinct = noofdistinct;
		ptrAttribute.catalogInfo.noOfDistinctValid = 1;

		return 1;
	}

	public int AddRelSize(String relname, int nooftuples) {

		if (relname.isEmpty()) {
			return (0);
		}

		CatalogInfoForRel ptrRel = Rel_Sizes;

		while (ptrRel != null && !ptrRel.RelName.equals(relname))
			ptrRel = ptrRel.Next;

		if (ptrRel == null) {
			ptrRel = new CatalogInfoForRel();

			ptrRel.RelName = relname;

			ptrRel.RelSize = new Size_t();
			ptrRel.RelSize.numTuples = nooftuples;

			ptrRel.Next = Rel_Sizes;
			Rel_Sizes = ptrRel;

			return 1;
		}

		if (ptrRel.RelSize == null)
			ptrRel.RelSize = new Size_t();
		ptrRel.RelSize.numTuples = nooftuples;

		return 1;
	}

	public int AddNumOfBlocks(String relname, int num_of_blocks) {

		if (relname == null) {
			return (0);
		}

		CatalogInfoForRel ptrRel = Rel_Sizes;

		while (ptrRel != null && !ptrRel.RelName.equals(relname))
			ptrRel = ptrRel.Next;

		if (ptrRel == null) {
			ptrRel = new CatalogInfoForRel();
			ptrRel.RelSize = new Size_t();

			ptrRel.RelName = relname;
			ptrRel.RelSize.noOfBlocks = num_of_blocks;

			ptrRel.Next = Rel_Sizes;
			Rel_Sizes = ptrRel;

			return 1;
		}

		if (ptrRel.RelSize == null)
			ptrRel.RelSize = new Size_t();
		ptrRel.RelSize.noOfBlocks = num_of_blocks;

		return 1;
	}

	public int AddTuplesPerBlock(String relname, int tuples_per_block) {

		if (relname == null) {
			return (0);
		}

		CatalogInfoForRel ptrRel = Rel_Sizes;

		while (ptrRel != null && !ptrRel.RelName.equals(relname))
			ptrRel = ptrRel.Next;

		if (ptrRel == null) {
			ptrRel = new CatalogInfoForRel();
			ptrRel.RelSize = new Size_t();

			ptrRel.RelName = relname;
			ptrRel.RelSize.tuplesPerBlock = tuples_per_block;

			ptrRel.Next = Rel_Sizes;
			Rel_Sizes = ptrRel;

			return 1;
		}

		if (ptrRel.RelSize == null)
			ptrRel.RelSize = new Size_t();
		ptrRel.RelSize.tuplesPerBlock = tuples_per_block;

		return 1;
	}

	int ComputeInfoFromExisting() {
		return 0;

	}

	public Attribute_Catalog GetAttrValInfo(String attrname) {

		Schema_Def ptr_schema = Rel_Schemas;

		while (ptr_schema != null) {

			if (!(ptr_schema.RelName.equals(""))) {

				Attribute_Catalog ptr_attr = ptr_schema.Attributes;

				while (ptr_attr != null
						&& !ptr_attr._attribute.isEquivalent(attrname, ptr_schema.RelName))
					ptr_attr = ptr_attr.Next;

				if (ptr_attr != null)
					return ptr_attr;
			}

			ptr_schema = ptr_schema.NextRel;
		}

		return null;
	}

	public int AddSizeInfoForRel(String relname, Size_t relSize) {

		CatalogInfoForRel relSizeInfo = new CatalogInfoForRel();
		relSizeInfo.RelName = relname;

		relSizeInfo.RelSize = relSize;

		relSizeInfo.Next = Rel_Sizes;
		Rel_Sizes = relSizeInfo;

		return 1;
	}

	public int AddIndexInfo(String relname, int listSize, String[] indexAttrList) {

		if (relname == null || indexAttrList == null || listSize <= 0) {
			return (0);
		}

		CatalogInfoForRel ptrRel = Rel_Sizes;

		while (ptrRel != null && !(ptrRel.RelName.equals(relname)))
			ptrRel = ptrRel.Next;

		if (ptrRel == null) {
			ptrRel = new CatalogInfoForRel();
			ptrRel.RelName = relname;
			for (int i = 0; i < GlobalMembersCatalog.MAXINDEXES; i++) {
				ptrRel.IndexAttrList[i] = null;
			}
			ptrRel.IndexAttrList[0] = indexAttrList;
			ptrRel.NumIndexes = 1;

			ptrRel.Next = Rel_Sizes;

			Rel_Sizes = ptrRel;

			return 1;
		}
		ptrRel.IndexAttrList[ptrRel.NumIndexes] = indexAttrList;
		ptrRel.NumIndexes++;

		return 1;
	}

	public int AddPartitionInfo(String relname,
			PhysicalProperties.PartitionType partitionType, int numattr,
			String[] attr) {

		if (relname == null) {
			return (0);
		}

		CatalogInfoForRel ptrRel = Rel_Sizes;

		while (ptrRel != null && !(ptrRel.RelName.equals(relname)))
			ptrRel = ptrRel.Next;

		if (ptrRel == null) {
			ptrRel = new CatalogInfoForRel();
			ptrRel.RelName = relname;

			ptrRel.Next = Rel_Sizes;

			Rel_Sizes = ptrRel;

			return 1;
		}

		if (ptrRel.RelSize == null)
			ptrRel.RelSize = new Size_t();

		ptrRel.Partitioned = true;
		ptrRel.PartitionType = partitionType;
		ptrRel.PartitionAttr = attr;

		return 1;
	}
}
