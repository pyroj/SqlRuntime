package pyroj.sqlruntime.catalog;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.AttributeProperty;
import pyroj.sqlruntime.properties.logical.BaseRelLogProp;
import pyroj.sqlruntime.properties.logical.IntegerProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

/**
 * caches the log props in the list instead of recomputing
 * 
 */
public class Catalog_t {
	private enum InputInfoType {
		NOTKNOWN, DISTINCT, SIZE_OF_REL, SIZE_OF_TUPLE, ATTR_MAX_VALUE, ATTR_MIN_VALUE, ATTR_HIST_BOUNDS, ATTR_COMMON_VALS, ATTR_COMMON_FREQS, NUM_OF_BLOCKS, TUPLES_PER_BLOCK, ATTR_SIZE_BYTES, INDEXINFO, PARTITIONED, MEM_BLOCKS, PARTITION_ENABLED;
	}

	private int _numBuffers = 0;
	Catalog _catalog;

	private final List<BaseRelLogProp> relLogPropList = new ArrayList<BaseRelLogProp>();

	private final String _catalogFileName;

	/**
	 * load the catalog info
	 */
	public Catalog_t(String catalogFileName) {
		_catalogFileName = catalogFileName;
		_catalog = new Catalog();
		try {
			LoadCatalogInfo(_catalogFileName);
		}
		catch (IOException e) {
			System.out.println("File not found " + _catalogFileName);
		}
	}

	/**
	 * get the logical property information from earlier structures, and return
	 * it packaged in the new structures
	 */
	public final BaseRelLogProp GetRelLogProp(String relName) {
		assert null != relName;
		BaseRelLogProp relLogProp = null;

		// first search in cached schemas
		for (BaseRelLogProp logProp : relLogPropList) {
			String name = logProp.Name();

			assert null != name;
			if (name.equals(relName)) {
				// names match
				relLogProp = logProp;
				break;
			}
		}

		if (relLogProp != null) {
			assert null != relLogProp;
			// logical property was cached in relLogPropList
			return relLogProp;
		}

		// logical property not cached in relLogPropList. So compute it and
		// cache

		// get the schema entry
		Schema_Def oldSchema = _catalog.Rel_Schemas;

		while (oldSchema != null) {
			if (oldSchema.RelName.equals(relName))
				break;
			oldSchema = oldSchema.NextRel;
		}

		// if schema not found, give up
		if (oldSchema == null) {
			return null;
		}

		// find the number of attributes
		int numAttr = 0;
		Attribute_Catalog p = oldSchema.Attributes;
		while (p != null) {
			numAttr++;
			p = p.Next;
		}

		// construct the schema in new format
		Attribute[] schema = new Attribute[numAttr];

		int i = 0;
		p = oldSchema.Attributes;
		while (p != null) {
			CatalogInfoForAttr c = p.catalogInfo;
			p._attribute.setSize(c.size);
			schema[i] = p._attribute;
			AttributeProperty attrProp = null;

			// default type taken as integer
			switch (p.Type) {
			case O_INTEGER:
			default:
				attrProp = new IntegerProperty((int) c.minValue, (int) c.maxValue, c.noOfDistinct);
				break;
			}
			schema[i].setProp(attrProp);
			i++;
			p = p.Next;
		}

		// get the size-info entry
		CatalogInfoForRel relInfo = _catalog.Rel_Sizes;

		while (relInfo != null) {
			if (relInfo.RelName.equals(relName))
				break;
			relInfo = relInfo.Next;
		}
		assert null != relInfo;

		Size_t oldSize = relInfo.RelSize;
		assert null != oldSize;

		boolean isSerial = true;
		int[] partitionKeys = null;
		int minNumPartitionKeys = 0;

		if (relInfo.Partitioned) {
			isSerial = false;

			if (relInfo.PartitionAttr != null && relInfo.PartitionAttr.length > 0) {
				partitionKeys = new int[relInfo.PartitionAttr.length];
				minNumPartitionKeys = partitionKeys.length;
				for (int k = 0; k < relInfo.PartitionAttr.length; k++) {
					assert null != relInfo.PartitionAttr[k];

					// find the attribute number in the schema
					partitionKeys[k] = -1;
					for (int l = 0; l < numAttr; l++) {
						Attribute a = schema[l];
						assert null != a;

						if (a.isEquivalent(relInfo.PartitionAttr[k], relInfo.RelName)) {
							partitionKeys[k] = l;
							break;
						}
					}
					if (partitionKeys[k] == -1)
						System.err.print("NOT FOUND: " + relName + ":" + relInfo.PartitionAttr[k] + "\n");
					assert partitionKeys[k] != -1;
				}
			}
		}

		// construct the logical property
		relLogProp = new BaseRelLogProp(relName, schema, oldSize.numTuples, oldSize.tuplesPerBlock);

		// add the built-in index info
		for (int j = 0; j < relInfo.NumIndexes; j++) {
			// add the j-th index
			// create the order

			int size = relInfo.IndexAttrList[j].length;
			int[] order = new int[size];
			String[] attrList = relInfo.IndexAttrList[j];
			assert null != attrList;

			for (int k = 0; k < size; k++) {
				String s = attrList[k];
				assert null != s;

				// find the attrnum for s in the schema
				order[k] = -1;
				for (int l = 0; l < numAttr; l++) {
					Attribute a = schema[l];
					assert null != a;

					if (a.isEquivalent(s, relInfo.RelName)) {
						order[k] = l;
						break;
					}
				}
				if (order[k] == -1)
					System.out.print("NOT FOUND: " + relName + ":" + s + "\n");
				assert order[k] != -1;
			}

			PhysicalProperties property = new PhysicalProperties(isSerial, partitionKeys, minNumPartitionKeys,
					relInfo.PartitionType, null, order);

			relLogProp.addPhysicalProperty(property);
		}
		if (relInfo.NumIndexes == 0) {
			PhysicalProperties property = new PhysicalProperties(isSerial, partitionKeys, minNumPartitionKeys,
					relInfo.PartitionType, null, null);
			relLogProp.addPhysicalProperty(property);
		}

		// insert in the list
		relLogPropList.add(0, relLogProp);

		return relLogProp;
	}

	public int getNumBuffers() {
		return _numBuffers;
	}

	private void AddAttrDistinct(String inputLine) {
		int index = inputLine.indexOf("DistinctAtrr ") + "DistinctAtrr ".length();
		inputLine = inputLine.substring(index + 1);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		String attrname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		int noofdistinct = GetNextInt(inputLine);
		_catalog.AddAttrDistinct(relname, attrname, noofdistinct);
	}

	private int AddAttrMaxValue(String inputLine) {
		int index = inputLine.indexOf("AttrMaxValue ") + "AttrMaxValue ".length();
		inputLine = inputLine.substring(index);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		String attrname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		double maxval = GetNextDouble(inputLine);
		_catalog.AddAttrMaxValue(relname, attrname, maxval);
		return 1;
	}

	private void AddAttrMinValue(String inputLine) {
		int index = inputLine.indexOf("AttrMinValue ") + "AttrMinValue".length();
		inputLine = inputLine.substring(index + 1);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		String attrname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		double minval = GetNextDouble(inputLine);
		_catalog.AddAttrMinValue(relname, attrname, minval);
	}

	private void AddAttrSize(String inputLine) {
		int index = inputLine.indexOf("AttrSize ") + "AttrSize".length();
		inputLine = inputLine.substring(index + 1);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		String attrname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		int attrsize = GetNextInt(inputLine);
		_catalog.AddAttrSize(relname, attrname, attrsize);
	}

	private void AddAttrHistBounds(String inputLine) {
		int index = inputLine.indexOf("AttrHistBounds ") + "AttrHistBounds".length();
		inputLine = inputLine.substring(index + 1);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		String attrname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		double[] histbounds = GetNextDoubleList(inputLine);
		_catalog.AddAttrHistBounds(relname, attrname, histbounds);
	}

	private void AddAttrCommonVals(String inputLine) {
		int index = inputLine.indexOf("AttrCommonVals ") + "AttrCommonVals".length();
		inputLine = inputLine.substring(index + 1);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		String attrname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		double[] commonvals = GetNextDoubleList(inputLine);
		_catalog.AddAttrCommonVals(relname, attrname, commonvals);
	}

	private void AddAttrCommonFreqs(String inputLine) {
		int index = inputLine.indexOf("AttrCommonFreqs ") + "AttrCommonFreqs".length();
		inputLine = inputLine.substring(index + 1);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		String attrname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		double[] commonfreqs = GetNextDoubleList(inputLine);
		_catalog.AddAttrCommonFreqs(relname, attrname, commonfreqs);
	}

	private void AddIndexInfo(String inputLine) {
		int index = inputLine.indexOf("Index ") + "Index ".length();
		inputLine = inputLine.substring(index);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		int indexListSize = GetNextInt(inputLine);
		assert indexListSize > 0;
		int i = 0;
		String[] indexAttrList = new String[indexListSize];
		for (i = 0; i < indexListSize; i++) {
			inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
			indexAttrList[i] = GetNextStr(inputLine);
		}
		_catalog.AddIndexInfo(relname, indexListSize, indexAttrList);
	}

	private void AddMemBlocksInfo(String inputLine) {
		int index = inputLine.indexOf("MemBlocks ") + "MemBlocks ".length();
		inputLine = inputLine.substring(index);
		_numBuffers = GetNextInt(inputLine);
	}

	private int AddNumOfBlocks(String inputLine) {
		int index = inputLine.indexOf("NumOfBlocks ") + "NumOfBlocks ".length();
		inputLine = inputLine.substring(index);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		int no_of_blocks = GetNextInt(inputLine);
		_catalog.AddNumOfBlocks(relname, no_of_blocks);
		return 1;
	}

	private int AddPartitionInfo(String inputLine) {
		if (!Config.IsPartitioningEnabled)
			return 1;

		int index = inputLine.indexOf("Partitioned ") + "Partitioned ".length();
		inputLine = inputLine.substring(index);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		String partitionTypeStr = GetNextStr(inputLine);
		PhysicalProperties.PartitionType partitionType = null;
		if (partitionTypeStr.equals("random"))
			partitionType = PhysicalProperties.PartitionType.RANDOM;
		else if (partitionTypeStr.equals("hash"))
			partitionType = PhysicalProperties.PartitionType.HASH;
		else {
			System.err.print("Invalid partition type " + partitionTypeStr + "\n");
		}
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		int numattr = GetNextInt(inputLine);
		assert partitionType != PhysicalProperties.PartitionType.HASH || numattr > 0;
		String[] attr = new String[numattr];
		;
		if (numattr > 0) {
			int i = 0;
			for (i = 0; i < numattr; i++) {
				inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
				attr[i] = GetNextStr(inputLine);
			}
		}

		_catalog.AddPartitionInfo(relname, partitionType, numattr, attr);
		return 1;
	}

	private void AddSizeOfRel(String input_line) {
		int index = input_line.indexOf("NumOfTuples ") + "NumOfTuples ".length();
		input_line = input_line.substring(index);
		String relname = GetNextStr(input_line);
		int nooftuples = GetNextInt(input_line.substring(input_line.indexOf(' ') + 1));
		_catalog.AddRelSize(relname, nooftuples);
	}

	private void AddTuplesPerBlock(String inputLine) {
		int index = inputLine.indexOf("TuplesPerBlock ") + "TuplesPerBlock ".length();
		inputLine = inputLine.substring(index);
		String relname = GetNextStr(inputLine);
		inputLine = inputLine.substring(inputLine.indexOf(' ') + 1);
		int tuples_per_block = GetNextInt(inputLine);
		_catalog.AddTuplesPerBlock(relname, tuples_per_block);
	}

	private Catalog_t.InputInfoType FindLineInfoType(String input_line) {
		if (input_line == null)
			return InputInfoType.NOTKNOWN;
		if (input_line.contains("DistinctAttr"))
			return InputInfoType.DISTINCT;
		if (input_line.contains("NumOfTuples"))
			return InputInfoType.SIZE_OF_REL;
		if (input_line.contains("SizeOfTuple"))
			return InputInfoType.SIZE_OF_TUPLE;
		if (input_line.contains("AttrMaxValue"))
			return InputInfoType.ATTR_MAX_VALUE;
		if (input_line.contains("AttrMinValue"))
			return InputInfoType.ATTR_MIN_VALUE;
		if (input_line.contains("AttrHistBounds"))
			return InputInfoType.ATTR_HIST_BOUNDS;
		if (input_line.contains("AttrCommonVals"))
			return InputInfoType.ATTR_COMMON_VALS;
		if (input_line.contains("AttrCommonFreqs"))
			return InputInfoType.ATTR_COMMON_FREQS;
		if (input_line.contains("NumOfBlocks"))
			return InputInfoType.NUM_OF_BLOCKS;
		if (input_line.contains("TuplesPerBlock"))
			return InputInfoType.TUPLES_PER_BLOCK;
		if (input_line.contains("AttrSize"))
			return InputInfoType.ATTR_SIZE_BYTES;
		if (input_line.contains("Index"))
			return InputInfoType.INDEXINFO;
		if (input_line.contains("Partitioned"))
			return InputInfoType.PARTITIONED;
		if (input_line.contains("MemBlocks"))
			return InputInfoType.MEM_BLOCKS;
		if (input_line.contains("PartitionEnabled"))
			return InputInfoType.PARTITION_ENABLED;
		return InputInfoType.NOTKNOWN;
	}

	private double GetNextDouble(String substring) {
		return Double.parseDouble(substring.substring(0, (substring.indexOf(' ') > -1 ? substring.indexOf(' ')
				: substring.length())));
	}

	private int GetNextInt(String substring) {
		return Integer.parseInt(substring.substring(0, (substring.indexOf(' ') > -1 ? substring.indexOf(' ')
				: substring.length())));
	}

	private double[] GetNextDoubleList(String substring) {
		List<Double> doubleList = new ArrayList<Double>();
		int index = 0;
		while (substring.length() > 0) {
			substring = substring.trim();
			index = substring.indexOf(' ');
			if (index < 0)
				index = substring.length();
			doubleList.add(Double.parseDouble(substring.substring(0, index)));
			if (index >= substring.length())
				break;
			substring = substring.substring(index);
		}
		double[] doubleArray = new double[doubleList.size()];
		for (int i = 0; i < doubleList.size(); i++)
			doubleArray[i] = doubleList.get(i);

		return doubleArray;
	}

	/*** old code below ***/
	private String GetNextStr(String substring) {
		return (substring.substring(0, (substring.indexOf(' ') > -1 ? substring.indexOf(' ') : substring.length()))
				.toLowerCase());
	}

	private int LoadCatalogInfo(String catalogFileName) throws IOException {
		String input_line = "";
		int end_of_input = 0;
		FileInputStream fstream = new FileInputStream(catalogFileName);
		assert fstream != null;
		// Get the object of DataInputStream
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		// Read File Line By Line
		while ((input_line = br.readLine()) != null) {
			if ((input_line.equals("\n") || input_line.isEmpty() || (input_line.charAt(0) == '#'))) {
			}
			else {
				input_line = input_line.replace(',', ' ');
				input_line = input_line.replace('(', ' ');
				input_line = input_line.replace(')', ' ');
				input_line = input_line.replace('=', ' ');
				input_line = input_line.replaceAll("\\s+", " ");
				switch (FindLineInfoType(input_line)) {
				case SIZE_OF_REL:
					AddSizeOfRel(input_line);
					break;
				case SIZE_OF_TUPLE:
					// assert false;
					// removed -- prasan
					// inserted = AddSizeOfTuple(input_line);
					break;
				case DISTINCT:
					AddAttrDistinct(input_line);
					break;
				case ATTR_MAX_VALUE:
					AddAttrMaxValue(input_line);
					break;
				case ATTR_MIN_VALUE:
					AddAttrMinValue(input_line);
					break;
				case ATTR_HIST_BOUNDS:
					AddAttrHistBounds(input_line);
					break;
				case ATTR_COMMON_VALS:
					AddAttrCommonVals(input_line);
					break;
				case ATTR_COMMON_FREQS:
					AddAttrCommonFreqs(input_line);
					break;
				case TUPLES_PER_BLOCK:
					AddTuplesPerBlock(input_line);
					break;
				case NUM_OF_BLOCKS:
					AddNumOfBlocks(input_line);
					break;
				case ATTR_SIZE_BYTES:
					AddAttrSize(input_line);
					break;
				case INDEXINFO:
					AddIndexInfo(input_line);
					break;
				case PARTITIONED:
					AddPartitionInfo(input_line);
					break;
				case MEM_BLOCKS:
					AddMemBlocksInfo(input_line);
					break;
				default:
					break;
				}
			}
		}
		// while (1)
		br.close();
		return end_of_input;
	}
}
