package pyroj.sqlruntime.catalog;

/*** stuff below is from the original code ***/

public enum OAttrType {
	O_UNKNOWN_TYPE(-1), O_INTEGER(0), O_STRING(1), O_FLOAT(2), O_DOUBLE(3), O_VARSTRING(
			4), O_VARBINARY(5), O_DATE(6), O_TIME(7), O_BINARY(8);

	private int intValue;
	private static java.util.HashMap<Integer, OAttrType> mappings;

	private static java.util.HashMap<Integer, OAttrType> getMappings() {
		if (mappings == null) {
			synchronized (OAttrType.class) {
				if (mappings == null) {
					mappings = new java.util.HashMap<Integer, OAttrType>();
				}
			}
		}
		return mappings;
	}

	private OAttrType(int value) {
		intValue = value;
		OAttrType.getMappings().put(value, this);
	}

	public int getValue() {
		return intValue;
	}

	public static OAttrType forValue(int value) {
		return getMappings().get(value);
	}
}
