package pyroj.sqlruntime.catalog;



public class Schema_Def {

	/**
	 * for storing the name of a relation the default value is ""
	 */
	public String RelName;

	/**
	 * for storing the attributes of a relation named 'RelName", which belongs
	 * to the schema
	 */
	public Attribute_Catalog Attributes;

	/**
	 * this used for maintaining a linked list of Schema_Def's as mentioned
	 * above
	 */
	public Schema_Def NextRel;

	public Schema_Def() {
		this.RelName = null;
		this.Attributes = null;
		this.NextRel = null;
	}
}
