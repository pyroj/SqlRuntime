package pyroj.sqlruntime.catalog;

import pyroj.sqlruntime.properties.logical.Attribute;

/**
 * this class is used for representing the attributes of a relation
 * 
 */
public class Attribute_Catalog {

	public OAttrType Type; // gives the type of attribute type
	
	Attribute _attribute;

	// the above is used for getting information about this attribute
	public CatalogInfoForAttr catalogInfo;

	public Attribute_Catalog Next;

	public Attribute_Catalog(OAttrType type, String relName, String attrName, int len) {
		Type = type;
		this.Next = null;
		_attribute = new Attribute(relName, attrName, len, null);
	}
}
