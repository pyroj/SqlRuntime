package pyroj.sqlruntime.catalog;

import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class CatalogInfoForRel {
	public String RelName;
	public Size_t RelSize;
	public CatalogInfoForRel Next;

	public int NumIndexes;
	public String[][] IndexAttrList = new String[GlobalMembersCatalog.MAXINDEXES][];

	public boolean Partitioned;
	public PhysicalProperties.PartitionType PartitionType;
	public String[] PartitionAttr;

	public CatalogInfoForRel() {

		RelName = null;
		RelSize = null;
		Next = null;
		Partitioned = false;

		NumIndexes = 0;
		for (int i = 0; i < GlobalMembersCatalog.MAXINDEXES; i++) {
			IndexAttrList[i] = null;
		}
	}
}
