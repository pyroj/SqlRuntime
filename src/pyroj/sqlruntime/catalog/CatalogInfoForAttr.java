package pyroj.sqlruntime.catalog;

public class CatalogInfoForAttr {
	public double maxValue;
	public double minValue;
	public int noOfDistinct;
	public double[] histBounds;
	public double[] commonVals;
	public double[] commonFreqs;

	public int minValueValid;
	public int maxValueValid;
	public int noOfDistinctValid;
	public int histBoundsValid;
	public int commonValsValid;
	public int commonFreqsValid;
	public int size;

	public CatalogInfoForAttr() {
		minValue = 0;
		maxValue = 0;
		noOfDistinct = 0;
		histBounds = null;
		commonVals = null;
		commonFreqs = null;
		size = 0;

		minValueValid = 0;
		maxValueValid = 0;
		noOfDistinctValid = 0;
		histBoundsValid = 0;
		commonValsValid = 0;
		commonFreqsValid = 0;
	}

}
