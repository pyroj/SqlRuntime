package pyroj.sqlruntime.properties.physical;

import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

public class PhysicalProperties {

	public static int NumInstances = 0;

	public enum PartitionType {
		RANDOM, HASH, RANGE
	};

	public static void printColumns(int[] columns, LogicalProperty schema, String name) {
		if (columns == null)
			return;

		System.out.print(name + ": ");
		for (int i = 0; i < columns.length; i++) {
			if (i > 0)
				System.out.print(",");
			if(schema == null)
				System.out.print(columns[i]);
			else {
				Attribute attr = schema.getAttribute(columns[i]);
				System.out.print(attr);
			}
		}

	}

	public static void printPartitionProp(PartitionType partitionType, int[] partitionKeys, LogicalProperty schema) {

		System.out.print(partitionType.toString().toLowerCase());

		printColumns(partitionKeys, schema, "");

	}

	private static boolean isPrefixOf(int[] array1, int[] array2) {
		// null if prefix of everything.
		if (array1 == null)
			return true;

		// Nothing can be prefix of an empty array
		if (array2 == null)
			return false;

		if (array1.length > array2.length)
			return false;

		for (int i = 0; i < array1.length; i++)
			if (array1[i] != array2[i])
				return false;

		return true;
	}

	private static boolean isSubsetOf(int[] array1, int[] array2) {
		// null if prefix of everything.
		if (array1 == null)
			return true;

		// Nothing can be prefix of an empty array
		if (array2 == null)
			return false;

		if (array1.length > array2.length)
			return false;

		for (int i = 0; i < array1.length; i++) {
			boolean found = false;

			for (int j = 0; !found && j < array2.length; j++)
				if (array1[i] == array2[j])
					found = true;

			if (!found)
				return false;
		}

		return true;

	}

	boolean _isSerial = true;
	int _minNumPartitionKeys = 0;
	PartitionType _partitionType = PartitionType.RANDOM;

	int[] _partitionKeys = null;

	int[] _sortKeys = null;

	int[] _indexKeys = null;

	public PhysicalProperties(boolean isSerial) {
		this(isSerial, null);

	}

	public PhysicalProperties(boolean isSerial, int[] sortKeys) {
		this(isSerial, null, 0, PartitionType.RANDOM, sortKeys, null);
	}

	public PhysicalProperties(boolean isSerial, int[] sortKeys, int[] indexKeys) {
		this(isSerial, null, 0, PartitionType.RANDOM, sortKeys, indexKeys);
	}

	public PhysicalProperties(int[] partitionKeys, int minNumKeys, PartitionType partitionType) {
		this(false, partitionKeys, minNumKeys, partitionType, null, null);
	}

	public PhysicalProperties(int[] partitionKeys, int minNumKeys, PartitionType partitionType, int[] sortKeys) {
		this(false, partitionKeys, minNumKeys, partitionType, sortKeys, null);
	}

	public PhysicalProperties(boolean isSerial, int[] partitionKeys, int minNumKeys, PartitionType partitionType,
			int[] sortKeys, int[] indexKeys) {
		NumInstances++;
		if (isSerial) {
			_isSerial = true;
		}
		else {
			_isSerial = false;
			_minNumPartitionKeys = minNumKeys;
			_partitionKeys = partitionKeys;
			_partitionType = partitionType;
		}

		_sortKeys = sortKeys;
		_indexKeys = indexKeys;
	}

	/**
	 * Check if this covers other
	 */
	public final boolean covers(PhysicalProperties other) {

		// Sort of A, B covers sort on A
		if (!isPrefixOf(other._sortKeys, _sortKeys) && !isPrefixOf(other._sortKeys, _indexKeys))
			return false;

		// Index on A, B covers index on A
		if (!isPrefixOf(other._indexKeys, _indexKeys))
			return false;

		if (other._isSerial != _isSerial)
			return false;

		if (other._isSerial || other._partitionType == PartitionType.RANDOM)
			return true;

		if (other._partitionType != _partitionType)
			return false;

		// Hash partition on A covers hash partition on A, B
		if (other._partitionType == PartitionType.HASH && !isSubsetOf(_partitionKeys, other._partitionKeys))
			return false;

		else if (other._partitionType == PartitionType.RANGE && !isPrefixOf(_partitionKeys, other._partitionKeys))
			return false;

		return true;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(getClass().isInstance(obj)))
			return false;

		PhysicalProperties other = (PhysicalProperties) obj;
		if (!isEquivalent(other))
			return false;

		return true;
	}

	public final int[] getIndexColumns() {
		return _indexKeys;
	}

	public int getMinNumPartitionKeys() {
		return _minNumPartitionKeys;
	}

	public int[] getPartitionKeys() {
		return _partitionKeys;
	}

	public PartitionType getPartitionType() {
		return _partitionType;
	}

	public int[] getSortColumns() {
		return _sortKeys;
	}

	// check if some property is covered by p

	@Override
	public int hashCode() {
		int hashCode = 0;
		if (_sortKeys != null) {
			for (int i = 0; i < _sortKeys.length; i++)
				hashCode += _sortKeys[i];
		}
		if (_indexKeys != null) {
			for (int i = 0; i < _indexKeys.length; i++)
				hashCode += _indexKeys[i];
		}
		if (!_isSerial) {
			if (_partitionType == PartitionType.RANDOM)
				hashCode += 1;
			else if (_partitionType == PartitionType.HASH)
				hashCode += 2;
			else if (_partitionType == PartitionType.RANGE)
				hashCode += 3;
			hashCode += _minNumPartitionKeys;

			if (_partitionKeys != null)
				for (int i = 0; i < _partitionKeys.length; i++) {
					hashCode += _partitionKeys[i];
				}
		}
		return hashCode;
	}

	/**
	 * Check if pg covers this.
	 */
	public final boolean isCoveredBy(PhysicalProperties other) {

		if (other == null)
			return false;

		return other.covers(this);
	}

	// check if two physical prop groups are equal
	public final boolean isEquivalent(PhysicalProperties other) {

		if (other == null)
			return false;

		if (!covers(other))
			return false;

		if (!other.covers(this))
			return false;

		return true;

	}

	public boolean isSerial() {
		return _isSerial;
	}

	// print the physical properties in the group
	public final void print(LogicalProperty schema) {
		if (_isSerial)
			System.out.print("Serial");
		else
			printPartitionProp(_partitionType, _partitionKeys, schema);
		printColumns(_sortKeys, schema, " Sort");
		printColumns(_indexKeys, schema, " Index");
	}

	public void setSortProperty(int[] sortcolumns) {
		_sortKeys = sortcolumns;
	}

	public void setIndexProperty(int[] indexColumns) {
		_indexKeys = indexColumns;
	}
}
