package pyroj.sqlruntime.properties.logical;

import pyroj.sqlruntime.expression.Expression;
import pyroj.sqlruntime.expression.RelArgRef_t;
import pyroj.sqlruntime.expression.Value_t;
import pyroj.sqlruntime.expression.Expression.ExpressionType;

// contains all the logical properties of the relation to be stored in
// the catalog
public abstract class LogicalProperty {

	private Attribute[] _attributes; // attribute array
	private double _relSizeTuples; // size in number of tuples
	
	public static int NumInstances = 0;

	public LogicalProperty(Attribute[] schema, double relSizeTuples) {
		_attributes = new Attribute[schema.length];
		for(int i = 0; i < schema.length; i++) {
			_attributes[i] = schema[i];
		}
		_relSizeTuples = relSizeTuples;
		NumInstances++;
	}
	
	public LogicalProperty(LogicalProperty other) {
		_attributes = new Attribute[other._attributes.length];
		for(int i = 0; i < other._attributes.length; i++) {
			_attributes[i] = other._attributes[i].clone();
		}
		_relSizeTuples = other._relSizeTuples;
		NumInstances++;
	}
	
	public void reset (Attribute[] schema, double relSizeTuples) {
		if(schema == null)
			_attributes = null;
		else {
			_attributes = new Attribute[schema.length];
			for(int i = 0; i < schema.length; i++) {
				_attributes[i] = schema[i];
			}
		}
		_relSizeTuples = relSizeTuples;		
	}
	
	public void reset (LogicalProperty other) {
		if(other._attributes == null)
			_attributes = null;
		else {
			_attributes = new Attribute[other._attributes.length];
			for(int i = 0; i < other._attributes.length; i++) {
				_attributes[i] = other._attributes[i].clone();
			}
		}
		_relSizeTuples = other._relSizeTuples;
	}
		
	public final double getNumBlocks() {
		return Math.ceil(_relSizeTuples / getTuplesPerBlock());
	}

	public final double getNumTuples() {
		return _relSizeTuples;
	}

	public final void setNumTuples(double relSizeTuples) {
		_relSizeTuples = relSizeTuples;

		for (int i = 0; i < _attributes.length; i++) {
			AttributeProperty a = _attributes[i].getProp();
			if (a.getNumberOfDistinctValues() > relSizeTuples) {
				a.setNumDistinctValues(relSizeTuples);
			}
		}
	}

	public final AttributeProperty getAttributeProperties(int index) {
		return _attributes[index].getProp();
	}
	
	public abstract double getTuplesPerBlock();

	public void mergeProperty(LogicalProperty other) {

		_relSizeTuples = Math.min(_relSizeTuples, other._relSizeTuples);

		for (int i = 0; i < _attributes.length; i++) {
			AttributeProperty attrProp = _attributes[i].getProp();

			int j = other.getAttributeIndex(this.getAttribute(i));

			AttributeProperty otherAttrProp = other._attributes[j].getProp();

			attrProp.mergeProperty(otherAttrProp);
		}
	}

	public void print(boolean dontPrintSchema) {
		System.out.print("<tuples = " + getNumTuples() + ", blocks = " + getNumBlocks());
		if (!dontPrintSchema) {
			System.out.print(", schema = ");
			for(int i = 0; i < _attributes.length; i++) {
				if (i > 0)
					System.out.print(", ");
				System.out.print(_attributes[i]);
			}
		}
		System.out.print(">");
	}

	// fix attribute value to the given
	public final double fixAttributeValue(int bnd, Value_t val, boolean updateAttribute) {

		double oldNumDistinct = _attributes[bnd].getProp().getNumberOfDistinctValues();
		AttributeProperty newProp = _attributes[bnd].getProp().fixAttributeValue(val);
		if(updateAttribute)
			_attributes[bnd].setProp(newProp);
		return newProp.getNumberOfDistinctValues() / oldNumDistinct;
	}

	public final double equateAttributes(int leftbnd, int rightbnd, boolean updateAttribute) {

		if (leftbnd == rightbnd || _attributes[leftbnd].getProp() == _attributes[rightbnd].getProp())
			// already equal
			return 1.0;

		double oldNumDistinct = _attributes[rightbnd].getProp().getNumberOfDistinctValues()
				* _attributes[leftbnd].getProp().getNumberOfDistinctValues();
		AttributeProperty newProp = _attributes[leftbnd].getProp().equateAttributes(_attributes[rightbnd].getProp());
		
		if(updateAttribute) {
			_attributes[leftbnd].setProp(newProp);
			_attributes[rightbnd].setProp(newProp);
		}
		return newProp.getNumberOfDistinctValues() / oldNumDistinct;
	}

	// constrain attr[lbnd] < attr[ubnd]
	public final double boundAttributes(int lbnd, int ubnd, boolean updateAttribute) {

		Double oldNumDistinctValues = _attributes[ubnd].getProp().getNumberOfDistinctValues();

		AttributeProperty newProp1 = _attributes[lbnd].getProp().setUpperBound(_attributes[ubnd].getProp());
		AttributeProperty newProp2 = _attributes[ubnd].getProp().setLowerBound(newProp1);

		if(updateAttribute) {
			_attributes[lbnd].setProp(newProp1);
			_attributes[ubnd].setProp(newProp2);
		}
		return newProp2.getNumberOfDistinctValues() / oldNumDistinctValues; // naive
	}

	// fix upper bound for attribute
	public final double setUpperBoundAttributeValue(int bnd, Value_t val, boolean updateAttribute) {

		double oldNumDistinct = _attributes[bnd].getProp().getNumberOfDistinctValues();
		AttributeProperty newProp = _attributes[bnd].getProp().setUpperBound(val);
		if(updateAttribute)
			_attributes[bnd].setProp(newProp);
		return newProp.getNumberOfDistinctValues() / oldNumDistinct;
	}

	// fix lower bound for attribute
	public final double setLowerBoundAttributeValue(int bnd, Value_t val, boolean updateAttribute) {

		double oldNumDistinct = _attributes[bnd].getProp().getNumberOfDistinctValues();
		AttributeProperty newProp = _attributes[bnd].getProp().setLowerBound(val);
		if(updateAttribute)
			_attributes[bnd].setProp(newProp);
		return newProp.getNumberOfDistinctValues() / oldNumDistinct;
	}
	
	// get the $i$th attribute
	public final Attribute getAttribute(int i) {
		return _attributes[i];
	}

	public int getAttributeIndex(Attribute refAttr) {
		for (int i = 0; i < _attributes.length; i++) {
			if (_attributes[i].isEquivalent(refAttr))
				return i;
		}

		return -1;
	}

	public int getAttributeIndex(String name, String relationName) {
		for (int i = 0; i < _attributes.length; i++) {
			if (_attributes[i].isEquivalent(name, relationName))
				return i;
		}

		return -1;
	}

	// number of attributes
	public final int NumAttr() {
		return _attributes.length;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < _attributes.length; i++) {
			if (i > 0)
				sb.append(", ");
			sb.append(_attributes[i]);
		}		
		
		return sb.toString();
	}
	
	public int[] translateBinding(int[] binding, LogicalProperty otherSchema) {
		if (binding == null)
			return null;

		int[] otherBnd = new int[binding.length];
		for (int i = 0; i < binding.length; i++) {
			Attribute attribute = this.getAttribute(binding[i]);
			otherBnd[i] = otherSchema.getAttributeIndex(attribute);
			if (otherBnd[i] < 0)
				return null;
		}

		return otherBnd;
	}
	
	// size of the tuple in bytes
	public final int TupleSize_Bytes() {
		int tupleSize_Bytes = 0;
		
		for(Attribute attribute : _attributes)
			tupleSize_Bytes += attribute.getSize();
		
		return tupleSize_Bytes;
	}
	
	public boolean isPresent(Expression expr) {
		ExpressionType exprType = expr.getExprType();

		if (exprType == ExpressionType.VALUE_T)
			return true;
		else if (exprType == ExpressionType.REL_ARG_REF_T) {
			RelArgRef_t argRef = (RelArgRef_t) expr;

			// if invalid in child and parent, must be a correlation var
			// if it is present in child
			if (argRef.isPresent(this))
				return true;
		}
		return false;		
	}
}
