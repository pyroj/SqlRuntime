package pyroj.sqlruntime.properties.logical;

import pyroj.sqlruntime.expression.Int_t;
import pyroj.sqlruntime.expression.Value_t;

// integer attribute property
public class IntegerProperty extends AttributeProperty {
	private final int _maxValue;
	private final int _minValue;

	public IntegerProperty(int minValue, int maxValue, double numDistinctValues) {
		super(AttrType_t.INTATTR_T, numDistinctValues);

		_minValue = minValue;

		// TODO: Hack to handle missing min and max value.
		if (maxValue - minValue < numDistinctValues) {
			// System.err.println("Inconsistent values");
			_maxValue = minValue + (int) numDistinctValues;
		}
		else
			_maxValue = maxValue;

	}

	@Override
	public final AttributeProperty clone() {
		return new IntegerProperty(_minValue, _maxValue, getNumberOfDistinctValues());
	}

	@Override
	public void dispose() {
	}

	@Override
	public final boolean isEquivalent(AttributeProperty prop) {
		if (!super.isEquivalent(prop))
			return false;
		final IntegerProperty ip = (IntegerProperty) prop;
		return ((_maxValue == ip._maxValue) && (_minValue == ip._minValue));
	}

	@Override
	public final IntegerProperty equateAttributes(AttributeProperty prop) {
		assert prop != null;
		assert prop.getType() == AttrType_t.INTATTR_T;
		IntegerProperty ip = (IntegerProperty) prop;

		int newmaxval = Math.min(_maxValue, ip._maxValue);
		int newminval = Math.max(_minValue, ip._minValue);

		double newNumDistinct = Math.min(_numDistinctValues, ip._numDistinctValues);
		newNumDistinct = Math.min(newNumDistinct, newmaxval - newminval + 1);

		return new IntegerProperty(newminval, newmaxval, newNumDistinct);
	}

	// TODO -- for all integer UBound/LBound, split "<" case and a <= case
	@Override
	public IntegerProperty fixAttributeValue(Value_t val) {
		int v = ((Int_t) val).Value();
		return new IntegerProperty(v, v, 1);
	}

	@Override
	public AttributeProperty setLowerBound(AttributeProperty prop) {
		IntegerProperty ip = (IntegerProperty) prop;

		return setLowerBound(ip._minValue);
	}

	@Override
	public AttributeProperty setLowerBound(Value_t val) {
		Int_t intval = (Int_t) val;
		int v = intval.Value();

		return setLowerBound(v);
	}

	private AttributeProperty setLowerBound(int minValue) {
		int newMinValue = Math.max(_minValue, minValue);

		double newNumDistinctValues = Math.ceil(_numDistinctValues * (_maxValue - newMinValue)
				/ (_maxValue - _minValue));

		newNumDistinctValues = Math.min(newNumDistinctValues, _maxValue - newMinValue + 1);
		if (newNumDistinctValues < 0)
			newNumDistinctValues = 0;

		return new IntegerProperty(newMinValue, _maxValue, newNumDistinctValues);
	}

	@Override
	public AttributeProperty setUpperBound(AttributeProperty prop) {
		IntegerProperty ip = (IntegerProperty) prop;
		return setUpperBound(ip._maxValue);
	}

	@Override
	public AttributeProperty setUpperBound(Value_t val) {
		Int_t intval = (Int_t) val;
		int v = intval.Value();
		return setUpperBound(v);
	}

	private AttributeProperty setUpperBound(int maxValue) {
		int newMaxValue = Math.min(_maxValue, maxValue);

		double newNumDistinctValues = Math.ceil(_numDistinctValues * (newMaxValue - _minValue)
				/ (_maxValue - _minValue));

		newNumDistinctValues = Math.min(newNumDistinctValues, newMaxValue - _minValue + 1);
		if (newNumDistinctValues < 0)
			newNumDistinctValues = 0;

		return new IntegerProperty(_minValue, newMaxValue, newNumDistinctValues);
	}

	@Override
	public final boolean subsumes(AttributeProperty prop) {
		if (!super.isEquivalent(prop))
			return false;
		final IntegerProperty ip = (IntegerProperty) prop;
		return ((_maxValue >= ip._maxValue) && (_minValue <= ip._minValue));
	}

	// returns the union of the $this$ annd $prop$
	// that is, the strongest property that implies both
	@Override
	public final AttributeProperty unionProp(AttributeProperty prop) {
		assert prop != null;
		assert getType() == prop.getType();

		if (subsumes(prop))
			return this;

		if (prop.subsumes(this))
			return prop;

		final IntegerProperty ip = (IntegerProperty) prop;

		int newMinVal = Math.min(_minValue, ip._minValue);
		int newMaxVal = Math.max(_maxValue, ip._maxValue);

		double newNumDistinct = _numDistinctValues + ip._numDistinctValues;

		return new IntegerProperty(newMinVal, newMaxVal, newNumDistinct);
	}

}
