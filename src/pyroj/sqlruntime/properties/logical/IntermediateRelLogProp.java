package pyroj.sqlruntime.properties.logical;

import pyroj.sqlruntime.Config;

// intermediate relation logical property --- blocks are assumed to be
// fully packed
public class IntermediateRelLogProp extends LogicalProperty {

	public IntermediateRelLogProp(Attribute[] schema_a, double relSize_tuples_a) {
		super(schema_a, relSize_tuples_a);
	}
	
	public IntermediateRelLogProp(LogicalProperty other) {
		super(other);
	}

	@Override
	public void reset (Attribute[] schema, double relSizeTuples) {
		super.reset(schema, relSizeTuples);
	}

	@Override
	public void reset (LogicalProperty other) {
		super.reset(other);
	}
	@Override
	public final double getTuplesPerBlock() {
		int blockSize_Bytes = Config.getBlockSize_Bytes();
		int tupleSize_Bytes = TupleSize_Bytes();

		return Math.ceil((double) blockSize_Bytes / tupleSize_Bytes);
	}
}
