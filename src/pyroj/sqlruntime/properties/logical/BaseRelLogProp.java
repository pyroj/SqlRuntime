package pyroj.sqlruntime.properties.logical;

import java.util.ArrayList;

import pyroj.sqlruntime.properties.physical.PhysicalProperties;

// base relation logical property --- blocks are assumed to be
// arbitrarily packed
public class BaseRelLogProp extends LogicalProperty {
	private String _name; // name of the relation
	private double _tuplesPerBlock; // average number of tuples per block
	private ArrayList<PhysicalProperties> _buildInProperties = new ArrayList<PhysicalProperties>();

	public BaseRelLogProp(String name_a, Attribute[] schema_a, double relSize_tuples_a, double tuplesPerBlock_a) {
		super(schema_a, relSize_tuples_a);
		_name = name_a;
		_tuplesPerBlock = tuplesPerBlock_a;
	}

	public final String Name() {
		return _name;
	}

	public final void addPhysicalProperty(PhysicalProperties phyProp) {
		_buildInProperties.add(phyProp);
	}

	public ArrayList<PhysicalProperties> getPhysicalProperties() {
		return _buildInProperties;
	}

	@Override
	public final double getTuplesPerBlock() {
		return _tuplesPerBlock;
	}
}
