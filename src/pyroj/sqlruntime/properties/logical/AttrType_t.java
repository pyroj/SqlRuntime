package pyroj.sqlruntime.properties.logical;

// $Id: LogProp.c,v 2.7 1999/03/08 09:11:41 sigdb Exp $
// logical property manipulation routines

// $Id: LogProp.h,v 2.6 1999/03/08 09:11:41 sigdb Exp $
// logical properties

//C++ TO JAVA CONVERTER TODO TASK: Extern blocks are not supported in Java.
//SAHA: removing extern C as its body was empty
//extern "C"
//{
///#include <ieeefp.h>
//}

// physical props
//C++ TO JAVA CONVERTER NOTE: Java has no need of forward class declarations:
//class PhyProp_t;
//C++ TO JAVA CONVERTER NOTE: Java has no need of forward class declarations:
//class ClusteredIndexOrder_t;
//C++ TO JAVA CONVERTER NOTE: Java has no need of forward class declarations:
//class PartitionProp_t;

// attribute types
public enum AttrType_t {
	INTATTR_T, FLOATATTR_T, DOUBLEATTR_T, STRINGATTR_T, VARSTRINGATTR_T, VARBINARYATTR_T, DATEATTR_T, TIMEATTR_T, BINARYATTR_T;

	public int getValue() {
		return this.ordinal();
	}

	public static AttrType_t forValue(int value) {
		return values()[value];
	}
}
