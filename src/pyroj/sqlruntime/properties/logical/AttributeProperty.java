package pyroj.sqlruntime.properties.logical;

import pyroj.sqlruntime.expression.Value_t;

public abstract class AttributeProperty {
	private final AttrType_t type;

	protected double _numDistinctValues;

	public AttributeProperty(AttrType_t type_a, double numDistinct_a) {
		type = type_a;
		_numDistinctValues = numDistinct_a;
		assert _numDistinctValues > 0;
	}

	@Override
	public abstract AttributeProperty clone();

	public void dispose() {
	}

	public abstract AttributeProperty equateAttributes(AttributeProperty p);

	public abstract AttributeProperty fixAttributeValue(Value_t val);

	public boolean isEquivalent(AttributeProperty prop) {
		return ((type == prop.getType()) && (_numDistinctValues == prop.getNumberOfDistinctValues()));
	}

	public abstract AttributeProperty setLowerBound(AttributeProperty p);

	public abstract AttributeProperty setLowerBound(Value_t val);

	public abstract AttributeProperty setUpperBound(AttributeProperty p);

	public abstract AttributeProperty setUpperBound(Value_t val);

	public void mergeProperty(AttributeProperty other) {
		_numDistinctValues = Math.min(_numDistinctValues, other._numDistinctValues);
	}

	public final double getNumberOfDistinctValues() {
		return _numDistinctValues;
	}

	public final void setNumDistinctValues(double n) {
		_numDistinctValues = n;
	}

	public abstract boolean subsumes(AttributeProperty prop);

	public final AttrType_t getType() {
		return type;
	}

	public abstract AttributeProperty unionProp(AttributeProperty prop);
}
