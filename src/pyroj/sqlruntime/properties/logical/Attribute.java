package pyroj.sqlruntime.properties.logical;

import java.util.HashMap;

// attribute information
public class Attribute {
	
	public static HashMap <Integer, String> AttributeIdToNameMapping = new HashMap <Integer, String>();
	public static HashMap <String, Integer> AttributeNameToIdMapping = new HashMap <String, Integer>();
	
	public static int NumInstances = 0;
	
	private int _id;
	private int _size; // size of the attribute
	private AttributeProperty _attrProp;

	public Attribute(String relName, String attrName, int size, AttributeProperty attrProp) {
		
		String key = getKey (relName, attrName);
		Integer id = AttributeNameToIdMapping.get(key);
		if(id == null) {
			_id = NumInstances++;
			AttributeIdToNameMapping.put(_id, key);
			AttributeNameToIdMapping.put(key, _id);
		}
		else {
			_id = id.intValue();
		}

		_size = size;
		_attrProp = attrProp; 
	}

	private Attribute(int id, int size, AttributeProperty attrProp) {
		
		_id = id;
		_size = size;
		_attrProp = attrProp; 
	}
	
	@Override
	public Attribute clone () {
		return new Attribute (_id, _size, _attrProp.clone());
	}
	
	public boolean isEquivalent(String name, String relName) {
		Integer id = AttributeNameToIdMapping.get(getKey (relName, name));
		if(id == null)
			return false;
		return _id == id;
	}

	public final boolean isEquivalent(Attribute other) {
		return _id == other._id;
	}

	public final int getId () {
		return this._id;
	}
	
	public final int getSize() {
		return this._size;
	}

	public void setSize (int size) {
		_size = size;
	}
	
	public AttributeProperty getProp () {
		return _attrProp;
	}
	
	public void setProp (AttributeProperty prop) {
		_attrProp = prop;
	}
	
	@Override
	public String toString() {
		return AttributeIdToNameMapping.get(_id);
	}

	public int compareTo(Attribute other) {
		return _id - other._id;
	}
	
	private String getKey(String relName, String attrName) {
		return relName + "." + attrName;
	}
}
