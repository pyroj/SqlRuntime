package pyroj.sqlruntime.properties.logical;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;

public class GlobalMembersLogProp {

	public static LogicalProperty MergeLogicalProperties(LogicalProperty leftLP, LogicalProperty rightLP) {

		int leftNumAttr = leftLP.NumAttr();
		int rightNumAttr = rightLP.NumAttr();
		Attribute[] schema = new Attribute[leftNumAttr + rightNumAttr];

		int leftIndex = 0;
		int rightIndex = 0;
		int index = 0;

		while (leftIndex < leftNumAttr && rightIndex < rightNumAttr) {
			int cmp = leftLP.getAttribute(leftIndex).compareTo(rightLP.getAttribute(rightIndex));
			if (cmp < 0) {
				schema[index] = leftLP.getAttribute(leftIndex).clone();
				index++;
				leftIndex++;
			}
			else {
				schema[index] = rightLP.getAttribute(rightIndex).clone();
				index++;
				rightIndex++;
			}
		}

		while (leftIndex < leftNumAttr) {
			schema[index] = leftLP.getAttribute(leftIndex).clone();
			index++;
			leftIndex++;
		}

		while (rightIndex < rightNumAttr) {
			schema[index] = rightLP.getAttribute(rightIndex).clone();
			index++;
			rightIndex++;
		}

		double relSizeTuples = leftLP.getNumTuples() * rightLP.getNumTuples();

		return SqlRuntimeMemoryManager.getIntermediateRelLogProp(schema, relSizeTuples);
	}
}
