package pyroj.sqlruntime.expression;

// aggregate expression
public abstract class AggExpr_t extends Expression {
	private final AggOpType_t opType;
	private final Expression input;

	public AggExpr_t(AggOpType_t opType_a, Expression inp_a) {
		super(ExpressionType.AGG_EXPR_T);
		this.opType = opType_a;
		this.input = inp_a;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: AggOpType_t AggOpType() const
	public final AggOpType_t AggOpType() {
		return opType;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Input() const
	public final Expression Input() {
		assert (input != null);
		return input;
	}

	// equivalence check for aggregate exprs

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEquivalent(const Expr_t *e) const
	@Override
	public final boolean isEquivalent(Expression e) {
		// same object
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		// must be a AGG_EXPR_T -- so cast safe
		final AggExpr_t a = (AggExpr_t) e;
		if (a.AggOpType() != opType)
			return false;

		Expression i = a.Input();
		if (!input.isEquivalent(i))
			return false;

		return true;
	}

}
