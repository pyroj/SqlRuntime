package pyroj.sqlruntime.expression;

import java.util.Collection;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// and predicate
public class AndPredicate extends BinarySymmetricPredicate {
	public AndPredicate(Expression left_a, Expression right_a) {
		super(PredOpType_t.AND_T, left_a, right_a);
	}

	@Override
	public final Expression clone() {
		return new AndPredicate(_input[0], _input[1]);
	}

	@Override
	public final MutablePair<Predicate, Predicate> decompose(LogicalProperty parSchema, LogicalProperty childSchema) {


		Predicate left = (Predicate) _input[0];
		Predicate right = (Predicate) _input[1];

		MutablePair<Predicate, Predicate> leftResult = left.decompose(parSchema, childSchema);

		MutablePair<Predicate, Predicate> rightResult = right.decompose(parSchema, childSchema);
		
		Predicate leftPredicate = null;
		Predicate rightPredicate = null;

		if (leftResult.getLeft().isEquivalent(ConstantPredicates.True())) {
			// left input can be pushed down
			leftPredicate = rightResult.getLeft();
		}
		else if (rightResult.getLeft().isEquivalent(ConstantPredicates.True())) {
			// right input can be pushed down
			leftPredicate = leftResult.getLeft();
		}
		else {
			// none of the inputs can be pushed down
			if (leftResult.getLeft().isEquivalent(left) && rightResult.getLeft().isEquivalent(right))
				leftPredicate = this;
			else
				leftPredicate = new AndPredicate(leftResult.getLeft(), rightResult.getLeft());
		}

		if (leftResult.getRight().isEquivalent(ConstantPredicates.True())) {
			// left input can be pushed down
			rightPredicate = rightResult.getRight();
		}
		else if (rightResult.getRight().isEquivalent(ConstantPredicates.True())) {
			// right input can be pushed down
			rightPredicate = leftResult.getRight();
		}
		else {
			// none of the inputs can be pushed down
			if (leftResult.getRight().isEquivalent(left) && rightResult.getRight().isEquivalent(right))
				rightPredicate = this;
			else
				rightPredicate = new AndPredicate(leftResult.getRight(), rightResult.getRight());
		}

		SqlRuntimeMemoryManager.putPredicatePair(leftResult);
		SqlRuntimeMemoryManager.putPredicatePair(rightResult);
		
		return SqlRuntimeMemoryManager.getPredicatePair(leftPredicate, rightPredicate);
	}

	// infer selectivity in view of the predicate

	@Override
	public boolean findEqPredicateBinding(LogicalProperty leftSchema, LogicalProperty rightSchema,
			Collection<Integer> leftEqAttribute, Collection<Integer> rightEqAttribute) {

		Predicate leftPred = (Predicate) _input[0];
		Predicate rightPred = (Predicate) _input[1];

		boolean isLeftValid = leftPred.findEqPredicateBinding(leftSchema, rightSchema, leftEqAttribute,
				rightEqAttribute);
		boolean isRightValid = rightPred.findEqPredicateBinding(leftSchema, rightSchema, leftEqAttribute,
				rightEqAttribute);

		if (isLeftValid || isRightValid)
			return true;

		return false;
	}

	// parSchema is NULL => do not push correlated predicates
	@Override
	public final boolean implies(Predicate p) {
		if (super.implies(p))
			return true;

		Predicate left = (Predicate) _input[0];
		Predicate right = (Predicate) _input[1];
		return left.implies(p) || right.implies(p);
	}

	@Override
	public boolean findEqToConstantPredicateBinding(LogicalProperty schema, Collection<Integer> bindings) {

		Predicate leftPred = (Predicate) _input[0];
		boolean isLeftValid = leftPred.findEqToConstantPredicateBinding(schema, bindings);

		Predicate rightPred = (Predicate) _input[1];
		boolean isRightValid = rightPred.findEqToConstantPredicateBinding(schema, bindings);

		if (isLeftValid || isRightValid)
			return true;

		return false;
	}

	@Override
	public final boolean findEqAndLessThanEqPredicateBnd(LogicalProperty schema, Collection<Integer> bindings) {

		Predicate leftPred = (Predicate) _input[0];

		boolean isLeftValid = leftPred.findEqAndLessThanEqPredicateBnd(schema, bindings);

		if (isLeftValid)
			return true;

		Predicate rightPred = (Predicate) _input[1];

		boolean isRightValid = rightPred.findEqAndLessThanEqPredicateBnd(schema, bindings);

		if (isRightValid)
			return true;

		return false;
	}

	@Override
	public double computeSelectivity(LogicalProperty schemaProp, boolean updateAttributeProperty) {
		Predicate right = (Predicate) _input[1];
		double rightsel = right.computeSelectivity(schemaProp, updateAttributeProperty);

		Predicate left = (Predicate) _input[0];
		double leftsel = left.computeSelectivity(schemaProp, updateAttributeProperty);

		return leftsel * rightsel;
	}

	@Override
	public final boolean isPresendInChildOrCorrelated(LogicalProperty parSchema, LogicalProperty childSchema) {

		Predicate left = (Predicate) _input[0];
		Predicate right = (Predicate) _input[1];

		return left.isPresendInChildOrCorrelated(parSchema, childSchema)
				&& right.isPresendInChildOrCorrelated(parSchema, childSchema);
	}

	@Override
	public final void printExpr(boolean isFullExp) {
		System.out.print("AND[ ");
		_input[0].printExpr(isFullExp);
		System.out.print(" ");
		_input[1].printExpr(isFullExp);
		System.out.print(" ]");
	}
}
