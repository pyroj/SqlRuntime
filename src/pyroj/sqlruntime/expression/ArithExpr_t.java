package pyroj.sqlruntime.expression;

// arithmetic expression
public abstract class ArithExpr_t extends Expression {
	private final ArithOpType_t opType;
	private final Expression[] inp = new Expression[2];

	public ArithExpr_t(ArithOpType_t opType_a, Expression left_a, Expression right_a) {
		super(ExpressionType.ARITH_EXPR_T);
		this.opType = opType_a;
		inp[0] = left_a;
		// //GlobalMembersRefCount.Refer(inp[0]);

		inp[1] = right_a;
		// //GlobalMembersRefCount.Refer(inp[1]);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: ArithOpType_t ArithOpType() const
	public final ArithOpType_t ArithOpType() {
		return opType;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Input(int i) const
	public final Expression Input(int i) {
		assert i == 0 || i == 1;
		return inp[i];
	}

	// equivalence check for arithmetic expressions

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEquivalent(const Expr_t *e) const
	@Override
	public boolean isEquivalent(Expression e) {
		// assert e;
		// same object
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		// must be a ARITH_EXPR -- so cast safe
		final ArithExpr_t ae = (ArithExpr_t) e;
		if (opType != ae.ArithOpType())
			return false;

		return true;
	}

}
