package pyroj.sqlruntime.expression;


// unary predicate
public abstract class UnaryPredicate extends Predicate {
	protected final Expression _input;

	public UnaryPredicate(PredOpType_t opType, Expression inp) {
		super(opType);
		_input = inp;
	}

	@Override
	public final Expression getInput(int i) {
		if(i != 0)
			return null;
		return _input;
	}

	@Override
	public final boolean isEquivalent(Expression e) {
		assert e != null;
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		UnaryPredicate u = (UnaryPredicate) e;

		Expression uInp = u._input;
		return _input.isEquivalent(uInp);
	}

	@Override
	public final int getNumInputs() {
		return 1;
	}
}
