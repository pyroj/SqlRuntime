package pyroj.sqlruntime.expression;


// supplies shared copies of constant predicates
public class ConstantPredicates {
	private static TruePredicate truePred = new TruePredicate();
	private static FalsePredicate falsePred = new FalsePredicate();

	public static Predicate True() {
		return truePred;
	}

	public static Predicate False() {
		return falsePred;
	}
}
