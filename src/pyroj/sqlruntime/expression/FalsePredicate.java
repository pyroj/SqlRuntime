package pyroj.sqlruntime.expression;

import pyroj.sqlruntime.properties.logical.LogicalProperty;

// false predicate
public class FalsePredicate extends ConstPredicate {
	public FalsePredicate() {
		super(PredOpType_t.FALSE_T);
	}

	@Override
	public final Expression clone() {
		return new FalsePredicate();
	}

	@Override
	public final boolean implies(Predicate UnnamedParameter1) {
		return true;
	}

	@Override
	public final double computeSelectivity(LogicalProperty schemaProp, boolean updateAttributeProperty) {
		return 0.0;
	}

	@Override
	public final int getNumInputs() {
		return 0;
	}

	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print("FALSE");
	}
}
