package pyroj.sqlruntime.expression;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// less-Eq predicate
public class LessThanEqualsPredicate extends BinaryAssymmetricPredicate {
	public LessThanEqualsPredicate(Expression left_a, Expression right_a) {
		super(PredOpType_t.LEQ_T, left_a, right_a);
	}

	@Override
	public final Expression clone() {
		return new LessThanEqualsPredicate( _input[0],  _input[1]);
	}

	// infer schema properties in view of the predicate
	@Override
	public final MutablePair<Predicate, Predicate> decompose(LogicalProperty parSchema, LogicalProperty childSchema) {
	
		if (isPresendInChildOrCorrelated(parSchema, childSchema)) {
			// can be pushed down
			return SqlRuntimeMemoryManager.getPredicatePair(ConstantPredicates.True(), this);
		}
		else {
			// cannot be pushed down
			return SqlRuntimeMemoryManager.getPredicatePair(this, ConstantPredicates.True());
		}
	}

	@Override
	public final double computeSelectivity(LogicalProperty logProp, boolean updateAttributeProperty) {
		double sel = 1.0;

		ExpressionType leftExprType =  _input[0].getExprType();
		ExpressionType rightExprType =  _input[1].getExprType();

		if (leftExprType == ExpressionType.REL_ARG_REF_T) {
			RelArgRef_t leftArgRef = (RelArgRef_t)  _input[0];
			int leftBnd = leftArgRef.getBinding(logProp);
			assert leftBnd != -1;

			if (rightExprType == ExpressionType.VALUE_T) {
				Value_t rightVal = (Value_t)  _input[1];

				logProp.setUpperBoundAttributeValue(leftBnd, rightVal, updateAttributeProperty);
			}
			else if (rightExprType == ExpressionType.REL_ARG_REF_T) {
				RelArgRef_t rightArgRef = (RelArgRef_t)  _input[1];
				int rightBnd = rightArgRef.getBinding(logProp);
				logProp.boundAttributes(leftBnd, rightBnd, true);
			}
		}
		else if (leftExprType == ExpressionType.VALUE_T) {
			Value_t leftVal = (Value_t)  _input[0];

			rightExprType =  _input[1].getExprType();

			if (rightExprType == ExpressionType.REL_ARG_REF_T) {
				RelArgRef_t rightArgRef = (RelArgRef_t)  _input[1];

				int rightBnd = rightArgRef.getBinding(logProp);
				logProp.setLowerBoundAttributeValue(rightBnd, leftVal, updateAttributeProperty);
			}
			else if (rightExprType == ExpressionType.VALUE_T) {
				Value_t rightVal = (Value_t)  _input[1];

				if (leftVal.IsLess(rightVal))
					sel = 1.0;
				else
					sel = 0.0;
			} // for arbit expression
			else
				sel = Config.DefaultSelectivity;
		}

		return sel;
	}

	// is the predicate valid over the schema?
	// schema valid if all the variables used bind successfully
	@Override
	public final boolean isPresendInChildOrCorrelated(LogicalProperty parSchema, LogicalProperty childSchema) {

		return childSchema.isPresent(_input[0]) && childSchema.isPresent(_input[1]);
	}

	@Override
	public final void printExpr(boolean isFullExp) {
		_input[0].printExpr(isFullExp);
		System.out.print(" <= ");
		_input[1].printExpr(isFullExp);
	}
}
