package pyroj.sqlruntime.expression;

import pyroj.sqlruntime.properties.logical.LogicalProperty;

// true predicate
public class TruePredicate extends ConstPredicate {
	public TruePredicate() {
		super(PredOpType_t.TRUE_T);
	}

	@Override
	public final Expression clone() {
		return new TruePredicate();
	}

	@Override
	public final boolean implies(Predicate p) {
		assert p != null;
		return isEquivalent(p);
	}

	@Override
	public final double computeSelectivity(LogicalProperty schemaProp, boolean updateAttributeProperty) {
		return 1.0;
	}

	@Override
	public final int getNumInputs() {
		return 0;
	}

	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print("TRUE");
	}
}
