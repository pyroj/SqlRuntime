package pyroj.sqlruntime.expression;

// value types
public enum ValType_t {
	INT_T, FLOAT_T, DOUBLE_T, STRING_T, VARCHAR_T, TRUTHVAL_T, NULL_T, STAR_T;

	public int getValue() {
		return this.ordinal();
	}

	public static ValType_t forValue(int value) {
		return values()[value];
	}
}
