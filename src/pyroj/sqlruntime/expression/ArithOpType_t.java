package pyroj.sqlruntime.expression;

// arithmetic expression type
public enum ArithOpType_t {
	PLUS_T, MINUS_T, MULT_T, DIV_T;

	public int getValue() {
		return this.ordinal();
	}

	public static ArithOpType_t forValue(int value) {
		return values()[value];
	}
}
