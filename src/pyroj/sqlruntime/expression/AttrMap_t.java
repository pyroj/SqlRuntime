package pyroj.sqlruntime.expression;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang3.mutable.Mutable;

import pyroj.sqlruntime.expression.Expression.ExpressionType;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// mapping of the output attributes to expressions

public class AttrMap_t {
	private final int numProj; // number of entries in the projection list
	private AttrMapEntry_t[] proj;

	// projection list --
	// maps input schema to output schema

	public AttrMap_t(int numProj_a) {
		this.numProj = numProj_a;
		this.proj = null;
		assert this.numProj >= 0;

		if (this.numProj > 0) {
			this.proj = new AttrMapEntry_t[this.numProj];
			for (int i = 0; i < this.numProj; i++)
				this.proj[i] = null;
		}
	}

	// partition the project map entries into valid and invalid entries
	// with respect to the schema
	public final void Decompose(LogicalProperty schema, Mutable<AttrMap_t> valid, Mutable<AttrMap_t> invalid) {
		assert null != schema;
		assert null != valid;
		assert null != invalid;

		ArrayList<AttrMapEntry_t> validList = new ArrayList<AttrMapEntry_t>();
		ArrayList<AttrMapEntry_t> invalidList = new ArrayList<AttrMapEntry_t>();

		int numValid = 0;
		int numInvalid = 0;

		for (int i = 0; i < this.numProj; i++) {
			AttrMapEntry_t e = Entry(i);
			assert null != e;

			Expression expr = e.Expr();
			assert null != expr;
			assert expr.getExprType() == ExpressionType.REL_ARG_REF_T;

			RelArgRef_t ref = (RelArgRef_t) expr;
			int bnd = ref.getBinding(schema);

			if (bnd == -1) {
				// does not bind -- invalid entry
				invalidList.add(0, e);
				numInvalid++;
			}
			else {
				// binds -- valid entry
				validList.add(0, e);
				numValid++;
			}
		}

		valid.setValue(new AttrMap_t(numValid));

		Iterator<AttrMapEntry_t> validIter = validList.iterator();

		int validIndex = 0;
		while (validIter.hasNext()) {
			AttrMapEntry_t e = validIter.next();

			valid.getValue().SetProj(validIndex++, e);
		}

		invalid.setValue(new AttrMap_t(numInvalid));

		Iterator<AttrMapEntry_t> invalidIter = invalidList.iterator();

		int invalidIndex = 0;
		while (invalidIter.hasNext()) {
			AttrMapEntry_t e = invalidIter.next();

			invalid.getValue().SetProj(invalidIndex++, e);
		}
	}

	public void dispose() {
		if (this.numProj > 0) {

			this.proj = null;
		}
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: AttrMapEntry_t *Entry(int i) const
	public final AttrMapEntry_t Entry(int i) {
		assert i >= 0 && i < this.numProj;
		assert null != this.proj[i];

		return this.proj[i];
	}

	// partition the project map entries into valid and invalid entries
	// with respect to the schema

	public boolean isEqualAsSet(AttrMap_t other) {
		if (this.numProj != other.numProj)
			return false;

		for (int i = 0; i < this.numProj; i++) {
			boolean found = false;
			for (int j = 0; j < other.numProj; j++) {
				if (this.proj[i].Expr().isEquivalent(other.proj[j].Expr()))
					found = true;
			}
			if (!found)
				return false;
		}
		return true;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int NumProj() const
	public final int NumProj() {
		return this.numProj;
	}

	public void print() {
		for (int i = 0; i < this.numProj; i++) {
			if (i > 0)
				System.out.print(", ");
			Expression e = this.proj[i].Expr();

			try {
				e.print();
			}
			catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public final void SetProj(int i, AttrMapEntry_t e) {
		assert i >= 0 && i < this.numProj;
		assert null != e;
		assert this.proj[i] == null;

		this.proj[i] = e;

	}
}
