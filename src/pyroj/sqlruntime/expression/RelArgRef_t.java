package pyroj.sqlruntime.expression;

import pyroj.sqlruntime.properties.logical.LogicalProperty;

// relation reference
public class RelArgRef_t extends Expression {
	private String relName;
	private String attrName;

	public RelArgRef_t(String rel_a, String attr_a) {
		super(ExpressionType.REL_ARG_REF_T);
		this.relName = null;
		this.attrName = null;
		assert rel_a != null;
		this.relName = rel_a;
		assert attr_a != null;
		this.attrName = attr_a;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: String AttrName() const
	public final String AttrName() {
		return this.attrName;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new RelArgRef_t(this.relName, this.attrName);
	}

	// equivalence check for relation references

	// binding of relation attribute reference wrt the given schema
	// binding is returned as the index into the schema

	// binding of relation attribute reference wrt the given schema
	// binding is returned as the index into the schema

	// find the correlated attribute -- returns the binding in the schema
	@Override
	public final int findCorrelated(LogicalProperty s, int level) throws Exception {
		assert s != null;
		assert level >= 0;

		int bnd = getBinding(s);
		if (level > 0)
			return bnd;

		// on the same level of nesting of predicates, there should be
		// no unbound rel arg ref
		assert bnd != -1;
		return -1;
	}

	public final int getBinding(LogicalProperty lp) {

		return lp.getAttributeIndex(this.attrName, this.relName);
	}

	@Override
	public final boolean isEquivalent(Expression e) {
		// same object
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		// must be a REL_ARG_REF_T -- so cast safe
		final RelArgRef_t r = (RelArgRef_t) e;
		String rname = r.RelName();
		assert rname != null;
		assert this.relName != null;
		if (!this.relName.equalsIgnoreCase(rname))
			return false;

		String aname = r.AttrName();
		assert aname != null;
		assert this.attrName != null;
		if (!this.attrName.equalsIgnoreCase(aname))
			return false;

		return true;
	}

	public final boolean isPresent(LogicalProperty schema) {
		return schema.getAttributeIndex(this.attrName, this.relName) >= 0;
	}

	// mark the node with correlated pred
	@Override
	public final boolean markCorrelated(LogicalProperty s, int level) throws Exception {
		if (findCorrelated(s, level) != -1)
			return true;
		else
			return false;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print(this.relName);
		System.out.print(":");
		System.out.print(this.attrName);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: String RelName() const
	public final String RelName() {
		return this.relName;
	}
}
