package pyroj.sqlruntime.expression;


// null value
public class Null_t extends Value_t {
	public Null_t() {
		super(ValType_t.NULL_T);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new Null_t();
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEq(const Value_t *) const
	@Override
	public final boolean IsEq(Value_t UnnamedParameter1) {
		return false;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsLess(const Value_t *) const
	@Override
	public final boolean IsLess(Value_t UnnamedParameter1) {
		return false;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print("NULL");
	}

	@Override
	public void dispose() {
	}
}
