package pyroj.sqlruntime.expression;


// base class for primary values
public abstract class Value_t extends Expression {
	private ValType_t valType;

	public Value_t(ValType_t valType_a) {
		super(ExpressionType.VALUE_T);
		this.valType = valType_a;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: ValType_t ValType() const
	public final ValType_t ValType() {
		return valType;
	}

	// equivalence check for values

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: virtual int IsEquivalent(const Expr_t *e) const
	@Override
	public boolean isEquivalent(Expression e) {
		// same object
		if (isEqual(e))
			return true;

		if (e == null)
			return false;

		if (!super.isEquivalent(e))
			return false;

		// must be a VALUE_T -- so cast safe
		final Value_t v = (Value_t) e;
		if (valType != v.ValType())
			return false;

		return true;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: virtual int IsEq(const Value_t *e) const = 0;
	public abstract boolean IsEq(Value_t e);

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: virtual int IsLess(const Value_t *e) const = 0;
	public abstract boolean IsLess(Value_t e);

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: virtual void PrintExpr(int isFullExp = 0) const = 0;
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public abstract void printExpr(boolean isFullExp);

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: virtual Expr_t *Copy() const = 0;
	@Override
	public abstract Expression clone();

	public void dispose() {
	}
}
