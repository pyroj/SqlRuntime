package pyroj.sqlruntime.expression;


// string value
public class String_t extends Value_t {
	private String value;
	private int len;

	public String_t(String v) {
		super(ValType_t.STRING_T);
		assert v != null;
		this.value = v;
		this.len = v.length();
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: String Value() const
	public final String Value() {
		return value;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int Length() const
	public final int Length() {
		return len;
	}

	// equivalence check for strings

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEquivalent(const Expr_t *e) const
	@Override
	public final boolean isEquivalent(Expression e) {
		// same object
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		// must be a STRING_T -- so cast safe
		final String_t s = (String_t) e;
		if (len != s.Length())
			return false;

		String v = s.Value();
		// for (int i = 0; i < len; i++)
		if (value.equals(v))
			return true;
		return false;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new String_t(value);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEq(const Value_t *) const
	@Override
	public final boolean IsEq(Value_t UnnamedParameter1) {
		return false;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsLess(const Value_t *) const
	@Override
	public final boolean IsLess(Value_t UnnamedParameter1) {
		return false;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print(value);
	}

	@Override
	public void dispose() {
		value = null;
	}
}
