package pyroj.sqlruntime.expression;


// count star
public class CountStar_t extends AggExpr_t {
	public CountStar_t(Expression inp_a) {
		super(AggOpType_t.COUNTSTAR_T, inp_a);
		//// assert 0;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new CountStar_t(Input());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print("COUNT(*)");
	}

}
