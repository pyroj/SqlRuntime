package pyroj.sqlruntime.expression;

// aggregate types: group-by not supported yet
public enum AggOpType_t {
	MAX_T, MIN_T, SUM_T, AVG_T, COUNT_T, COUNTDISTINCT_T, COUNTSTAR_T;

	public int getValue() {
		return this.ordinal();
	}

	public static AggOpType_t forValue(int value) {
		return values()[value];
	}
}
