package pyroj.sqlruntime.expression;

import java.util.Collection;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// equality predicate
public class EqualsPredicate extends BinarySymmetricPredicate {
	public EqualsPredicate(Expression left_a, Expression right_a) {
		super(PredOpType_t.EQ_T, left_a, right_a);
	}

	@Override
	public final Expression clone() {
		return new EqualsPredicate(_input[0], _input[1]);
	}

	// infer schema properties in view of the predicate
	// only consider the case when operands are values/relargrefs

	@Override
	public final MutablePair<Predicate, Predicate> decompose(LogicalProperty parSchema, LogicalProperty childSchema) {

		if (isPresendInChildOrCorrelated(parSchema, childSchema)) {
			// can be pushed down
			return SqlRuntimeMemoryManager.getPredicatePair(ConstantPredicates.True(), this);
		}
		else {
			// cannot be pushed down
			return SqlRuntimeMemoryManager.getPredicatePair(this, ConstantPredicates.True());
		}
	}

	// infer selectivity in view of the predicate

	// parSchema is NULL => do not push correlated predicates

	@Override
	public boolean findEqPredicateBinding(LogicalProperty leftSchema, LogicalProperty rightSchema,
			Collection<Integer> leftEqAttribute, Collection<Integer> rightEqAttribute) {

		Expression leftInp = _input[0];
		Expression rightInp = _input[1];

		if (leftInp.getExprType() != ExpressionType.REL_ARG_REF_T || rightInp.getExprType() != ExpressionType.REL_ARG_REF_T)
			return false;

		RelArgRef_t leftRef = (RelArgRef_t) leftInp;
		RelArgRef_t rightRef = (RelArgRef_t) rightInp;

		int leftAttrBnd = leftRef.getBinding(leftSchema);
		int rightAttrBnd = rightRef.getBinding(rightSchema);

		// in-valid binding
		if (leftAttrBnd < 0 || rightAttrBnd < 0) {
			leftAttrBnd = rightRef.getBinding(leftSchema);
			rightAttrBnd = leftRef.getBinding(rightSchema);
		}

		// in-valid binding
		if (leftAttrBnd < 0 || rightAttrBnd < 0)
			return false;

		leftEqAttribute.add(leftAttrBnd);
		rightEqAttribute.add(rightAttrBnd);

		return true;
	}

	@Override
	public boolean findEqToConstantPredicateBinding(LogicalProperty schema, Collection<Integer> bindings) {

		Expression leftInp = _input[0];
		Expression rightInp = _input[1];

		if (leftInp.getExprType() == ExpressionType.REL_ARG_REF_T && rightInp.getExprType() == ExpressionType.VALUE_T) {

			RelArgRef_t leftRef = (RelArgRef_t) leftInp;
			int lBnd = leftRef.getBinding(schema);

			if (lBnd != -1) {
				bindings.add(lBnd);
				return true;
			}
		}
		else if (rightInp.getExprType() == ExpressionType.REL_ARG_REF_T && leftInp.getExprType() == ExpressionType.VALUE_T) {

			RelArgRef_t rightRef = (RelArgRef_t) rightInp;
			int rBnd = rightRef.getBinding(schema);

			if (rBnd != -1) {
				bindings.add(rBnd);
				return true;
			}
		}

		return false;
	}

	@Override
	public final boolean findEqAndLessThanEqPredicateBnd(LogicalProperty schema, Collection<Integer> bindings) {
	
		Expression leftInp = _input[0];
		Expression rightInp = _input[1];

		if (leftInp.getExprType() == ExpressionType.REL_ARG_REF_T){
			
		
			RelArgRef_t leftRef = (RelArgRef_t) leftInp;
			int lBnd = leftRef.getBinding(schema);
			
			if (rightInp.getExprType() == ExpressionType.REL_ARG_REF_T) {

				RelArgRef_t rightRef = (RelArgRef_t) rightInp;
				int rBnd = rightRef.getBinding(schema);
	
				if (lBnd == -1 && rBnd == -1) {
					// parameter == parameter
					return false;
				}
				
				if (lBnd != -1) {
					bindings.add(lBnd);
					return true;
				}
	
				if (rBnd != -1) {
					bindings.add(rBnd);
					return true;
				}
			}
			else if (lBnd != -1) {
				bindings.add(lBnd);
				return true;				
			}
		}

		else if (rightInp.getExprType() == ExpressionType.REL_ARG_REF_T) {

			RelArgRef_t rightRef = (RelArgRef_t) rightInp;
			int rBnd = rightRef.getBinding(schema);

			if (rBnd != -1) {
				bindings.add(rBnd);
				return true;
			}
		}

		return false;
	}

	@Override
	public final double computeSelectivity(LogicalProperty schemaProp, boolean updateAttributeProperty) {
		double sel = 1.0;

		Expression left = _input[0];
		Expression right = _input[1];

		ExpressionType leftExprType = left.getExprType();
		ExpressionType rightExprType = right.getExprType();

		if (leftExprType == ExpressionType.REL_ARG_REF_T) {
			RelArgRef_t leftArgRef = (RelArgRef_t) left;
			int leftBnd = leftArgRef.getBinding(schemaProp);
			if (leftBnd != -1) {
				if (rightExprType == ExpressionType.VALUE_T) {
					Value_t rightVal = (Value_t) right;

					sel = schemaProp.fixAttributeValue(leftBnd, rightVal, updateAttributeProperty);
				}
				else if (rightExprType == ExpressionType.REL_ARG_REF_T) {
					RelArgRef_t rightArgRef = (RelArgRef_t) right;

					int rightBnd = rightArgRef.getBinding(schemaProp);
					assert rightBnd != -1;
					sel = schemaProp.equateAttributes(leftBnd, rightBnd, updateAttributeProperty);
				} // for arbit expressions
				else
					sel = Config.DefaultSelectivity;
			}
			else {
				// left expr is a parameter --- assign arbitrary value

				if (rightExprType == ExpressionType.REL_ARG_REF_T) {
					RelArgRef_t rightArgRef = (RelArgRef_t) right;

					int rightBnd = rightArgRef.getBinding(schemaProp);
					if (rightBnd != -1) {
						Int_t v = new Int_t(1);
						sel = schemaProp.fixAttributeValue(rightBnd, v, updateAttributeProperty);
					} // parameter == parameter
					else
						sel = 0.0;
				}
				else if (rightExprType == ExpressionType.VALUE_T) {
					sel = 0.0; // parameter == val
				} // arbitrary estimate
				else
					sel = Config.DefaultSelectivity;
			}
		}
		else if (leftExprType == ExpressionType.VALUE_T) {
			Value_t leftVal = (Value_t) left;

			if (rightExprType == ExpressionType.REL_ARG_REF_T) {
				RelArgRef_t rightArgRef = (RelArgRef_t) right;

				int rightBnd = rightArgRef.getBinding(schemaProp);
				if (rightBnd != -1)
					sel = schemaProp.fixAttributeValue(rightBnd, leftVal, updateAttributeProperty);
				else
					// val == parameter
					sel = 0.0;
			}
			else if (rightExprType == ExpressionType.VALUE_T) {
				Value_t rightVal = (Value_t) right;

				if (leftVal.IsEq(rightVal))
					sel = 1.0;
				else
					sel = 0.0;
			} // for arbit expression
			else
				sel = Config.DefaultSelectivity;
		}

		return sel;
	}

	@Override
	public final boolean isPresendInChildOrCorrelated(LogicalProperty parSchema, LogicalProperty childSchema) {

		Expression left = _input[0];
		Expression right = _input[1];

		return childSchema.isPresent(left) && childSchema.isPresent(right);
	}

	@Override
	public final void printExpr(boolean isFullExp) {
		_input[0].printExpr(isFullExp);
		System.out.print(" == ");
		_input[1].printExpr(isFullExp);
	}
}
