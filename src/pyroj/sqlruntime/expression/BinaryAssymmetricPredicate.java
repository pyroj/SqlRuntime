package pyroj.sqlruntime.expression;

import java.util.Collection;

import pyroj.sqlruntime.properties.logical.LogicalProperty;

// binary assymmetric predicate
public abstract class BinaryAssymmetricPredicate extends BinaryPredicate {
	public BinaryAssymmetricPredicate(PredOpType_t opType, Expression left, Expression right) {
		super(opType, left, right);
	}

	@Override
	public boolean isEquivalent(Expression e) {
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		BinarySymmetricPredicate b = (BinarySymmetricPredicate) e;

		return (_input[0].isEquivalent(b._input[0]) && _input[1].isEquivalent(b._input[1]));
	}

	@Override
	public boolean findEqToConstantPredicateBinding(LogicalProperty schema, Collection<Integer> bindings) {

		if (_input[0].getExprType() == ExpressionType.REL_ARG_REF_T && _input[1].getExprType() == ExpressionType.VALUE_T) {

			RelArgRef_t leftRef = (RelArgRef_t) _input[0];
			int lBnd = leftRef.getBinding(schema);

			if (lBnd != -1) {
				bindings.add(lBnd);
				return true;
			}
		}
		else if (_input[1].getExprType() == ExpressionType.REL_ARG_REF_T && _input[0].getExprType() == ExpressionType.VALUE_T) {

			RelArgRef_t rightRef = (RelArgRef_t) _input[1];
			int rBnd = rightRef.getBinding(schema);

			if (rBnd != -1) {
				bindings.add(rBnd);
				return true;
			}
		}

		return false;
	}
}
