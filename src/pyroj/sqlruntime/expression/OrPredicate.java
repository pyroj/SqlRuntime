package pyroj.sqlruntime.expression;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// or predicate
public class OrPredicate extends BinarySymmetricPredicate {
	public OrPredicate(Expression left_a, Expression right_a) {
		super(PredOpType_t.OR_T, left_a, right_a);
	}

	@Override
	public final Expression clone() {
		return new OrPredicate(_input[0], _input[1]);
	}

	@Override
	public final MutablePair<Predicate, Predicate> decompose(LogicalProperty parSchema, LogicalProperty childSchema) {

		Predicate left = (Predicate) _input[0];
		Predicate right = (Predicate) _input[1];

		MutablePair<Predicate, Predicate> leftResult = left.decompose(parSchema, childSchema);

		MutablePair<Predicate, Predicate> rightResult = right.decompose(parSchema, childSchema);

		Predicate leftPredicate = null;
		Predicate rightPredicate = null;

		if (leftResult.getLeft().isEquivalent(ConstantPredicates.True())
				&& rightResult.getLeft().isEquivalent(ConstantPredicates.True())) {
			// both inputs can be pused --- so the or clause can be pushed
			leftPredicate = ConstantPredicates.True();
			rightPredicate = this;
		}
		else {
			// cannot be pushed
			leftPredicate = this;
			rightPredicate = ConstantPredicates.True();
		}

		SqlRuntimeMemoryManager.putPredicatePair(leftResult);
		SqlRuntimeMemoryManager.putPredicatePair(rightResult);
		
		return SqlRuntimeMemoryManager.getPredicatePair(leftPredicate, rightPredicate);
	}

	@Override
	public final boolean implies(Predicate p) {
		assert p != null;
		if (super.implies(p))
			return true;

		Predicate left = (Predicate) _input[0];
		Predicate right = (Predicate) _input[1];

		return (left.implies(p) && right.implies(p));
	}

	@Override
	public final double computeSelectivity(LogicalProperty schemaProp, boolean updateAttributeProperty) {
		Predicate left = (Predicate) _input[0];
		assert left != null;
		Predicate right = (Predicate) _input[1];
		assert right != null;

		double leftsel = left.computeSelectivity(schemaProp, false);

		if (right.implies(left))
			return leftsel;

		double rightsel = right.computeSelectivity(schemaProp, false);

		if (left.implies(right))
			return rightsel;

		double sel = 1.0 - (1.0 - leftsel) * (1.0 - rightsel);

		// arbitrary, to avoid considering null-result queries
		if (sel == 0)
			sel = 0.0001;
		return sel;
	}

	@Override
	public final boolean isPresendInChildOrCorrelated(LogicalProperty parSchema, LogicalProperty schema) {
		assert schema != null;

		Predicate left = (Predicate) _input[0];
		assert left != null;
		Predicate right = (Predicate) _input[1];
		assert right != null;

		return (left.isPresendInChildOrCorrelated(parSchema, schema) && right.isPresendInChildOrCorrelated(parSchema,
				schema));
	}

	@Override
	public final void printExpr(boolean isFullExp) {
		System.out.print("OR[ ");
		_input[0].printExpr(isFullExp);
		System.out.print(" ");
		_input[1].printExpr(isFullExp);
		System.out.print(" ]");
	}

}
