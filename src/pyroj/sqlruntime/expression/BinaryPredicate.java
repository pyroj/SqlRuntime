package pyroj.sqlruntime.expression;


// binary predicate
public abstract class BinaryPredicate extends Predicate {
	protected final Expression[] _input = new Expression[2];

	public BinaryPredicate(PredOpType_t opType, Expression left, Expression right) {
		super(opType);
		_input[0] = left;
		_input[1] = right;
	}

	@Override
	public final Expression getInput(int i) {
		return _input[i];
	}

	@Override
	public final int getNumInputs() {
		return _input.length;
	}
	
}
