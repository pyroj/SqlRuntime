package pyroj.sqlruntime.expression;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// not predicate
public class NotPredicate extends UnaryPredicate {
	public NotPredicate(Expression inp_a) {
		super(PredOpType_t.NOT_T, inp_a);
	}

	@Override
	public final Expression clone() {
		return new NotPredicate(_input);
	}

	@Override
	public final MutablePair<Predicate, Predicate> decompose(LogicalProperty parSchema, LogicalProperty childSchema) {

		Predicate inpPred = (Predicate) _input;

		MutablePair<Predicate, Predicate> inpResult = inpPred.decompose(parSchema, childSchema);

		Predicate leftPredicate = null;
		Predicate rightPredicate = null;
		
		if (inpResult.getLeft().isEquivalent(ConstantPredicates.True())) {
			// input can be pushed down
			leftPredicate = ConstantPredicates.True();
			rightPredicate = this;
		}
		else {
			// input cannot be pushed down
			leftPredicate = this;
			rightPredicate = ConstantPredicates.True();
		}

		SqlRuntimeMemoryManager.putPredicatePair(inpResult);
		
		return SqlRuntimeMemoryManager.getPredicatePair(leftPredicate, rightPredicate);
	}

	@Override
	public final boolean implies(Predicate p) {
		assert p != null;
		if (super.implies(p))
			return true;

		if (p.getPredOpType() == PredOpType_t.NOT_T) {
			Predicate x = (Predicate) p.getInput(0);
			assert x != null;

			Predicate y = (Predicate) _input;
			assert y != null;

			return x.implies(y);
		}

		return false;
	}

	@Override
	public double computeSelectivity(LogicalProperty schemaProp, boolean updateAttributeProperty) {
		Expression e = _input;
		assert e.getExprType() == ExpressionType.PREDICATE_T;

		Predicate p = (Predicate) e;
		double inpSel = p.computeSelectivity(schemaProp, false);
		return 1.0 - inpSel;
	}

	@Override
	public final boolean isPresendInChildOrCorrelated(LogicalProperty parSchema, LogicalProperty schema) {
		assert schema != null;
		Predicate inpPred = (Predicate) _input;
		assert inpPred != null;

		return inpPred.isPresendInChildOrCorrelated(parSchema, schema);
	}

	@Override
	public final void printExpr(boolean isFullExp) {
		System.out.print("NOT[ ");
		_input.printExpr(isFullExp);
		System.out.print(" ]");
	}
}
