package pyroj.sqlruntime.expression;


// entries to map the output attributes to expressions

public class AttrMapEntry_t{
	private Expression expr; // mapped attribute's index in input schema
	private String name; // new name assigned to the mapped attribute


	public AttrMapEntry_t(Expression expr_a, String string)
	{
		this.expr = expr_a;
		this.name = null;
		if (string != null)
			name = string;
		
	}

//C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
//ORIGINAL LINE: Expr_t *Expr() const
	public final Expression Expr()
	{
		return expr;
	}

//C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
//ORIGINAL LINE: sbyte *Name() const
	public final String Name()
	{
		return name;
	}

	public void dispose()
	{
		if (name != null)
//C++ TO JAVA CONVERTER TODO TASK: The memory management function 'free' has no equivalent in Java:
		//	free(name)
			;
		
	}
}
