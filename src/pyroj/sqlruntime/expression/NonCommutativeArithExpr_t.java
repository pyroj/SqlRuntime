package pyroj.sqlruntime.expression;


// non-commutative operator expression
public abstract class NonCommutativeArithExpr_t extends ArithExpr_t {
	public NonCommutativeArithExpr_t(ArithOpType_t opType_a, Expression left_a,
			Expression right_a) {
		super(opType_a, left_a, right_a);
	}

	// equivalence check for non-commutative arithmetic operator expressions

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEquivalent(const Expr_t *e) const
	@Override
	public final boolean isEquivalent(Expression e) {
		assert e != null;
		// same object
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		final NonCommutativeArithExpr_t ae = (NonCommutativeArithExpr_t) e;

		Expression aeInpL = ae.Input(0);
		Expression aeInpR = ae.Input(1);
		Expression inpL = Input(0);
		Expression inpR = Input(1);

		if ((!inpL.isEquivalent(aeInpL) || !inpR.isEquivalent(aeInpR)))
			return false;

		return true;
	}
}
