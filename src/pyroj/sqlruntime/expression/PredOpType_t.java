package pyroj.sqlruntime.expression;

// predicate relation operator types
public enum PredOpType_t {
	EQ_T, NEQ_T, LT_T, LEQ_T, OR_T, AND_T, NOT_T, TRUE_T, FALSE_T, IN_T;

	public int getValue() {
		return this.ordinal();
	}

	public static PredOpType_t forValue(int value) {
		return values()[value];
	}
}
