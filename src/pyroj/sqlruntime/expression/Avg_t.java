package pyroj.sqlruntime.expression;


// avg
public class Avg_t extends AggExpr_t {
	public Avg_t(Expression inp_a) {
		super(AggOpType_t.AVG_T, inp_a);
		//// assert 0;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new Avg_t(Input());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int isFullExp = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean isFullExp) {
		System.out.print("AVG(");
		Input().printExpr(isFullExp);
		System.out.print(")");
	}

}
