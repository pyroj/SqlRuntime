package pyroj.sqlruntime.expression;


// double float value
public class Double_t extends Value_t {
	private double value;

	public Double_t() {
		this(0);
	}

	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	// ORIGINAL LINE: Double_t(double v = 0) : Value_t(DOUBLE_T), value(v)
	public Double_t(double v) {
		super(ValType_t.DOUBLE_T);
		this.value = v;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: double Value() const
	public final double Value() {
		return value;
	}

	// equivalence check for double

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEquivalent(const Expr_t *e) const
	@Override
	public final boolean isEquivalent(Expression e) {
		// same object
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		// must be a DOUBLE_T -- so cast safe
		final Double_t d = (Double_t) e;
		if (d.Value() != value)
			return false;

		return true;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new Double_t(value);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEq(const Value_t *e) const
	@Override
	public final boolean IsEq(Value_t e) {
		assert e.ValType() == ValType_t.DOUBLE_T;
		final Double_t v = (Double_t) e;
		return (value == v.Value());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print(value);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsLess(const Value_t *e) const
	@Override
	public final boolean IsLess(Value_t e) {
		assert e.ValType() == ValType_t.DOUBLE_T;
		final Double_t v = (Double_t) e;
		return value < v.Value();
	}

	@Override
	public void dispose() {
	}
}
