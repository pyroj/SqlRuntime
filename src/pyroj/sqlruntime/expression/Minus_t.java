package pyroj.sqlruntime.expression;


// minus
public class Minus_t extends NonCommutativeArithExpr_t {
	public Minus_t(Expression left, Expression right) {
		super(ArithOpType_t.MINUS_T, left, right);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new Minus_t(Input(0), Input(1));
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int isFullExp = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean isFullExp) {
		System.out.print("( ");
		Input(0).printExpr(isFullExp);
		System.out.print(" - ");
		Input(1).printExpr(isFullExp);
		System.out.print(" )");
	}

}
