package pyroj.sqlruntime.expression;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// constant predicate
public abstract class ConstPredicate extends Predicate {
	
	public ConstPredicate(PredOpType_t t) {
		super(t);
	}

	@Override
	public final MutablePair<Predicate, Predicate> decompose(LogicalProperty parSchema, LogicalProperty childSchema) {
		return SqlRuntimeMemoryManager.getPredicatePair(this, ConstantPredicates.True());
	}

	@Override
	public final Expression getInput(int UnnamedParameter1) {
		return null;
	}

	@Override
	public final boolean isPresendInChildOrCorrelated(LogicalProperty UnnamedParameter1, LogicalProperty UnnamedParameter2) {
		return true;
	}

	@Override
	public int getNumInputs() {
		return 0;
	}
}
