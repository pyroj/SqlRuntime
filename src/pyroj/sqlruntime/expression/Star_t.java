package pyroj.sqlruntime.expression;


// star value
public class Star_t extends Value_t {
	public Star_t() {
		super(ValType_t.STAR_T);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new Star_t();
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEq(const Value_t *) const
	@Override
	public final boolean IsEq(Value_t UnnamedParameter1) {
		return false;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsLess(const Value_t *) const
	@Override
	public final boolean IsLess(Value_t UnnamedParameter1) {
		return false;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print("*");
	}

	@Override
	public void dispose() {
	}
}
