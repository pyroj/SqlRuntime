package pyroj.sqlruntime.expression;

// binary symmetric predicate
public abstract class BinarySymmetricPredicate extends BinaryPredicate {
	public BinarySymmetricPredicate(PredOpType_t opType, Expression left, Expression right) {
		super(opType, left, right);
	}

	@Override
	public final boolean isEquivalent(Expression e) {
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		BinarySymmetricPredicate b = (BinarySymmetricPredicate) e;

		return ((_input[0].isEquivalent(b._input[0]) &&  _input[1].isEquivalent(b. _input[1])) 
				|| (_input[0].isEquivalent(b. _input[1]) &&  _input[1].isEquivalent(b._input[0])));
	}
}
