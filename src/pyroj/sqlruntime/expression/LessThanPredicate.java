package pyroj.sqlruntime.expression;

import java.util.Collection;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// less predicate
public class LessThanPredicate extends BinaryAssymmetricPredicate {
	public LessThanPredicate(Expression left, Expression right) {
		super(PredOpType_t.LT_T, left, right);
	}

	@Override
	public final Expression clone() {
		return new LessThanPredicate(_input[0],  _input[1]);
	}

	@Override
	public final MutablePair<Predicate, Predicate> decompose(LogicalProperty parSchema, LogicalProperty childSchema) {

		if (isPresendInChildOrCorrelated(parSchema, childSchema)) {
			// can be pushed down
			return SqlRuntimeMemoryManager.getPredicatePair(ConstantPredicates.True(), this);
		}
		else {
			// cannot be pushed down
			return SqlRuntimeMemoryManager.getPredicatePair(this, ConstantPredicates.True());
		}
	}

	@Override
	public final boolean implies(Predicate p) {
		if (super.implies(p))
			return true;
		if (p.getPredOpType() != PredOpType_t.LT_T)
			return false;

		if (_input[0].getExprType() != ExpressionType.REL_ARG_REF_T)
			return false;

		if ( _input[1].getExprType() != ExpressionType.VALUE_T)
			return false;

		Expression pLeft = p.getInput(0);
		if (pLeft.getExprType() != ExpressionType.REL_ARG_REF_T)
			return false;

		Expression pRight = p.getInput(1);
		if (pRight.getExprType() != ExpressionType.VALUE_T)
			return false;

		if (! _input[0].isEquivalent(pLeft))
			return false;

		Value_t val = (Value_t)  _input[1];
		Value_t pVal = (Value_t) pRight;

		return val.IsLess(pVal);
	}

	@Override
	public final boolean findEqAndLessThanEqPredicateBnd(LogicalProperty schema, Collection<Integer> bindings) {

		if (_input[0].getExprType() == ExpressionType.REL_ARG_REF_T &&  _input[1].getExprType() == ExpressionType.REL_ARG_REF_T) {
			RelArgRef_t leftRef = (RelArgRef_t) _input[0];
			int lBnd = leftRef.getBinding(schema);

			RelArgRef_t rightRef = (RelArgRef_t)  _input[1];
			int rBnd = rightRef.getBinding(schema);

			if (lBnd == -1 && rBnd == -1) {
				// parameter == parameter
				return false;
			}

			if (lBnd != -1) {
				bindings.add(lBnd);
				return true;
			}

			if (rBnd != -1) {
				bindings.add(rBnd);
				return true;
			}
		}
		else if (_input[0].getExprType() == ExpressionType.REL_ARG_REF_T) {

			RelArgRef_t leftRef = (RelArgRef_t) _input[0];
			int lBnd = leftRef.getBinding(schema);

			if (lBnd != -1) {
				bindings.add(lBnd);

				return true;
			}
		}
		else if ( _input[1].getExprType() == ExpressionType.REL_ARG_REF_T) {

			RelArgRef_t rightRef = (RelArgRef_t)  _input[1];
			int rBnd = rightRef.getBinding(schema);

			if (rBnd != -1) {
				bindings.add(rBnd);

				return true;
			}
		}

		return false;
	}

	@Override
	public final double computeSelectivity(LogicalProperty logProp, boolean updateAttributeProperty) {
		double sel = 1;

		ExpressionType leftExprType = _input[0].getExprType();
		ExpressionType rightExprType =  _input[1].getExprType();

		if (leftExprType == ExpressionType.REL_ARG_REF_T) {
			RelArgRef_t leftArgRef = (RelArgRef_t) _input[0];
			int leftBnd = leftArgRef.getBinding(logProp);

			if (rightExprType == ExpressionType.VALUE_T) {
				Value_t rightVal = (Value_t)  _input[1];

				sel = logProp.setUpperBoundAttributeValue(leftBnd, rightVal, updateAttributeProperty);
			}
			else if (rightExprType == ExpressionType.REL_ARG_REF_T) {
				RelArgRef_t rightArgRef = (RelArgRef_t)  _input[1];
				int rightBnd = rightArgRef.getBinding(logProp);

				sel = logProp.boundAttributes(leftBnd, rightBnd, updateAttributeProperty);
			} // for arbit expressions
			else
				sel = Config.DefaultSelectivity;
		}
		else if (leftExprType == ExpressionType.VALUE_T) {
			Value_t leftVal = (Value_t) _input[0];

			if (rightExprType == ExpressionType.REL_ARG_REF_T) {
				RelArgRef_t rightArgRef = (RelArgRef_t)  _input[1];

				int rightBnd = rightArgRef.getBinding(logProp);
				assert rightBnd != -1;
				sel = logProp.setLowerBoundAttributeValue(rightBnd, leftVal, updateAttributeProperty);
			}
			else if (rightExprType == ExpressionType.VALUE_T) {
				Value_t rightVal = (Value_t)  _input[1];

				if (leftVal.IsLess(rightVal))
					sel = 1;
				else
					sel = 0;
			} // for arbit expression
			else
				sel = Config.DefaultSelectivity;
		}

		return sel;
	}

	@Override
	public final boolean isPresendInChildOrCorrelated(LogicalProperty parSchema, LogicalProperty childSchema) {

		return childSchema.isPresent(_input[0]) && childSchema.isPresent(_input[1]);
	}

	@Override
	public final void printExpr(boolean isFullExp) {
		_input[0].printExpr(isFullExp);
		System.out.print(" < ");
		_input[1].printExpr(isFullExp);
	}
}
