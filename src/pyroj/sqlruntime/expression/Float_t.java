package pyroj.sqlruntime.expression;


// float value
public class Float_t extends Value_t {
	private float value;

	public Float_t() {
		this(0);
	}

	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	// ORIGINAL LINE: Float_t(float v = 0) : Value_t(FLOAT_T), value(v)
	public Float_t(float v) {
		super(ValType_t.FLOAT_T);
		this.value = v;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: float Value() const
	public final float Value() {
		return value;
	}

	// equivalence check for floats

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEquivalent(const Expr_t *e) const
	@Override
	public final boolean isEquivalent(Expression e) {
		// same object
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		// must be a FLOAT_T -- so cast safe
		final Float_t f = (Float_t) e;
		if (f.Value() != value)
			return false;

		return true;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new Float_t(value);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEq(const Value_t *e) const
	@Override
	public final boolean IsEq(Value_t e) {
		assert e.ValType() == ValType_t.FLOAT_T;
		final Float_t v = (Float_t) e;
		return (value == v.Value());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsLess(const Value_t *e) const
	@Override
	public final boolean IsLess(Value_t e) {
		assert e.ValType() == ValType_t.FLOAT_T;
		final Float_t v = (Float_t) e;
		return (value < v.Value());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print(value);
	}

	@Override
	public void dispose() {
	}
}
