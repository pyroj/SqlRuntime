package pyroj.sqlruntime.expression;


// max
public class Max_t extends AggExpr_t {
	public Max_t(Expression inp_a) {
		super(AggOpType_t.MAX_T, inp_a);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new Max_t(Input());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int isFullExp = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean isFullExp) {
		System.out.print("MAX(");
		Input().printExpr(isFullExp);
		System.out.print(")");
	}

}
