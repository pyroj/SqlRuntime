package pyroj.sqlruntime.expression;


// integer value
public class Int_t extends Value_t {
	private int value;

	public Int_t() {
		this(0);
	}

	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	// ORIGINAL LINE: Int_t(int v = 0) : Value_t(INT_T), value(v)
	public Int_t(int v) {
		super(ValType_t.INT_T);
		this.value = v;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int Value() const
	public final int Value() {
		return value;
	}

	// equivalence check for integers

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEquivalent(const Expr_t *e) const
	@Override
	public final boolean isEquivalent(Expression e) {
		// same object
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		// must be a INT_T -- so cast safe
		final Int_t i = (Int_t) e;
		if (i.Value() != value)
			return false;

		return true;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new Int_t(value);
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsEq(const Value_t *e) const
	@Override
	public final boolean IsEq(Value_t e) {
		assert e.ValType() == ValType_t.INT_T;
		final Int_t v = (Int_t) e;
		return (value == v.Value());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: int IsLess(const Value_t *e) const
	@Override
	public final boolean IsLess(Value_t e) {
		assert e.ValType() == ValType_t.INT_T;
		final Int_t v = (Int_t) e;
		return (value < v.Value());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean UnnamedParameter1) {
		System.out.print(value);
	}

	@Override
	public void dispose() {
	}
}
