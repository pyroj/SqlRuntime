package pyroj.sqlruntime.expression;

import pyroj.sqlruntime.properties.logical.LogicalProperty;

// base class for all expressions
public abstract class Expression {
	
	public enum ExpressionType {
		VALUE_T, PREDICATE_T, ARITH_EXPR_T, AGG_EXPR_T, REL_ARG_REF_T;
	}
	
	private static int NumInstances = 0; // unique id generator for the expression node
	private final int _id; // unique id for the expression node
	private final ExpressionType _exprType; // type of the expression node

	public Expression(ExpressionType exprType) {
		_id = NumInstances++;
		_exprType = exprType;
	}

	public final ExpressionType getExprType() {
		return _exprType;
	}

	public final int getId() {
		return _id;
	}

	public final boolean isEqual(Expression e) {
		return (e.getId() == _id);
	}

	// is the expression equivalent to the given expression
	// equivalence check for expressions
	public boolean isEquivalent(Expression e) {
		if (isEqual(e))
			return true;
		return (_exprType == e.getExprType());
	}

	// print expression
	public final void print() {
		printExpr(false);
	}

	public abstract void printExpr(boolean isFullExp);

	@Override
	public abstract Expression clone();

	// find the correlated attribute -- returns the binding in the schema
	public int findCorrelated(LogicalProperty UnnamedParameter1, int UnnamedParameter2)
			throws Exception {
		return -1;
	}

	// mark the correlated nodes
	public boolean markCorrelated(LogicalProperty UnnamedParameter1,
			int UnnamedParameter2) throws Exception {
		return false;
	}

}
