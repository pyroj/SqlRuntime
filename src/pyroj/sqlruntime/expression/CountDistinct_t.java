package pyroj.sqlruntime.expression;


// count distinct
public class CountDistinct_t extends AggExpr_t {
	public CountDistinct_t(Expression inp_a) {
		super(AggOpType_t.COUNTDISTINCT_T, inp_a);
		//// assert 0;
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: Expr_t *Copy() const
	@Override
	public final Expression clone() {
		return new CountDistinct_t(Input());
	}

	// C++ TO JAVA CONVERTER WARNING: 'const' methods are not available in Java:
	// ORIGINAL LINE: void PrintExpr(int isFullExp = 0) const
	// C++ TO JAVA CONVERTER NOTE: Java does not allow default values for
	// parameters. Overloaded methods are inserted above.
	@Override
	public final void printExpr(boolean isFullExp) {
		System.out.print("COUNT DISTINCT(");
		Input().printExpr(isFullExp);
		System.out.print(")");
	}

}
