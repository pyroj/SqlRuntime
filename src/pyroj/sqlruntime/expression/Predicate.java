package pyroj.sqlruntime.expression;

import java.util.Collection;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

// predicate
public abstract class Predicate extends Expression {
	
	public static Predicate joinPredicate(Predicate pred1, Predicate pred2) {
		if (pred1.isEquivalent(ConstantPredicates.True()) || pred2.implies(pred1))
			return pred2;
		else if (pred2.isEquivalent(ConstantPredicates.True()) || pred1.implies(pred2))
			return pred1;
		else
			return new AndPredicate(pred1, pred2);

	}

	private final PredOpType_t _opType;

	public Predicate(PredOpType_t opType) {
		super(ExpressionType.PREDICATE_T);
		_opType = opType;
	}

	// decomposes the predicate to a cascade of two predicates ---
	// returns the predicate to be pushed down as childPred and
	// remainder as parPred
	// parSchema is used to detect correlated variables
	// if a var is not in the par then it is assumed to be a correlated
	// variable from above
	// parSchema is NULL => do not push correlated predicates
	public abstract MutablePair<Predicate, Predicate> decompose(LogicalProperty parSchema, LogicalProperty childSchema);

	// find the correlated attribute -- returns the binding in the schema
	@Override
	public int findCorrelated(LogicalProperty s, int level) throws Exception {
		assert s != null;
		assert level >= 0;

		int n = getNumInputs();
		int bnd = -1;

		for (int i = 0; i < n && bnd == -1; i++) {
			Expression inp = getInput(i);
			assert inp != null;

			bnd = inp.findCorrelated(s, level);
		}

		return bnd;
	}

	// equivalence check for predicates
	public boolean findEqPredicateBinding(LogicalProperty leftSchema, LogicalProperty rightSchema,
			Collection<Integer> leftEqAttribute, Collection<Integer> rightEqAttribute) {
		return false;
	}

	public boolean findEqToConstantPredicateBinding(LogicalProperty schema, Collection<Integer> bindings) {
		return false;
	}

	// get the bindings that are part of the relevant select predicate
	public boolean findEqAndLessThanEqPredicateBnd(LogicalProperty schema, Collection<Integer> bindings){
		return false;
	}
	
	public boolean implies(Predicate p) {
		return isEquivalent(p);
	}

	public void inferProp(LogicalProperty logProp) {

		double selectivity = computeSelectivity(logProp, true);

		double relSizeTuples = Math.ceil(selectivity * logProp.getNumTuples());
		if (relSizeTuples > Config.getCardLimit()) {
			if (Config.DebugMode) 
				System.out.println("--- CARDINALITY OVERFLOW ---");
			
			relSizeTuples = Config.getCardLimit();
		}

		logProp.setNumTuples(relSizeTuples);
	}
	
	public abstract double computeSelectivity(LogicalProperty schemaProp, boolean updateAttributeProperty);

	public abstract Expression getInput(int i);

	@Override
	public boolean isEquivalent(Expression e) {
		assert e != null;
		if (isEqual(e))
			return true;

		if (!super.isEquivalent(e))
			return false;

		// must be a PREDICATE_T -- so cast safe
		final Predicate p = (Predicate) e;
		if (_opType != p.getPredOpType())
			return false;

		return true;
	}

	// is the predicate valid over the schema?
	public abstract boolean isPresendInChildOrCorrelated(LogicalProperty parSchema, LogicalProperty schema);

	// mark the correlated attribute -- returns the binding in the schema
	@Override
	public boolean markCorrelated(LogicalProperty s, int level) throws Exception {
		assert s != null;
		assert level >= 0;

		int n = getNumInputs();
		boolean isCorrelated = false;

		for (int i = 0; i < n; i++) {
			Expression inp = getInput(i);
			assert inp != null;

			boolean isInpCorrelated = inp.markCorrelated(s, level);
			if (isInpCorrelated)
				isCorrelated = true;
		}

		return isCorrelated;
	}

	public abstract int getNumInputs();

	public final PredOpType_t getPredOpType() {
		return _opType;
	}
}
