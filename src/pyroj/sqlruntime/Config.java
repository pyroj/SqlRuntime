package pyroj.sqlruntime;

import pyroj.sqlruntime.catalog.Catalog_t;

public class Config {
	public static boolean IsPartitioningEnabled = true;

	public static boolean PrintQuery = false;
	public static boolean DebugMode = false;
	
	public static double DefaultSelectivity = 0.5;

	private static double _seekTime = 8; // 8ms
	private static double _readTimePerBlock = 2; // 2ms or 2MBps
	private static double _writeTimePerBlock = 4; // 4ms or 1MBps
	private static double _remoteReadPerBlock = 20; // 20ms or 0.2MBps
	private static double _transferTimePerBlock = 2; // 2ms or 2MBps network
	// cpu cost weightage is 0.2ms and 1 probe = 200 instr at 100MIPS
	private static double _processingTimePerBlock = 0.2;
	private static double _vertexCreationTime = 50; // 50 ms
	public static double _numBuffers = 8000;
	private static int _degreeOfParallelism = 100;
	private static int _maxPartitionAttr = 5;
	private static int _blockSize_bytes = 4096;
	private static double _maxErr = 0.5;
	private static double _cardLimit = Long.MAX_VALUE;
	private static int _indexFanout = 20;

	private static Catalog_t _catalog;

	/**
	 * block size in number of bytes
	 */

	public static int getBlockSize_Bytes() {
		return _blockSize_bytes;
	}

	/**
	 * cardinality limit upper bound on size (to check overflows)
	 */
	public static double getCardLimit() {
		return _cardLimit;
	}

	public static Catalog_t getCatalog() {
		return _catalog;
	}

	public static int getDegreeOfParallelism() {
		if (!IsPartitioningEnabled)
			return 1;
		else
			return _degreeOfParallelism;
	}

	/**
	 * subsumption enabled?
	 */

	public static int getIndexFanout() {
		return _indexFanout;
	}

	/**
	 * max rounding error for comparing costs
	 */
	public static double getMaxErr() {
		return _maxErr;
	}

	public static int getMaxPartitionAttr() {
		return _maxPartitionAttr;
	}

	public static double getNumBuffers() {
		return _numBuffers;
	}

	public static double getVertexCreationTime() {
		return _vertexCreationTime;
	}

	public static double getProcessingTimePerBlock() {
		return _processingTimePerBlock;
	}

	public static double getReadTime() {
		return _readTimePerBlock;
	}

	public static double getRemoteRead() {
		return _remoteReadPerBlock;
	}

	public static double getSeekTime() {
		return _seekTime;
	}

	public static double getWriteTime() {
		return _writeTimePerBlock;
	}

	public static double getTransferTime() {
		return _transferTimePerBlock;
	}

	public static void setCatalog(Catalog_t catalog) {
		_catalog = catalog;
	}

	public static void setPrintNone() {
		PrintQuery = false;
		DebugMode = false;
	}
}
