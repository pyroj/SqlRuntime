package pyroj.sqlruntime;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.MutablePair;

import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.IntermediateRelLogProp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class SqlRuntimeMemoryManager {

	public static int NumPairInstancesCreated = 0;
	private static ArrayList<MutablePair<Object, Object>> _pairs = new ArrayList<MutablePair<Object, Object>>();
	
	public static MutablePair<Object, Object> getPair (Object left, Object right) {
		
		if(_pairs.isEmpty()) {
			NumPairInstancesCreated++;
			return new MutablePair<Object, Object> (left, right);
		}
		
		MutablePair<Object, Object> pair = _pairs.remove(_pairs.size() -1);
		pair.setLeft(left);
		pair.setRight(right);
		return pair;
	}
	
	public static void putPair (MutablePair<Object, Object> pair) {
		
		pair.setLeft(null);
		pair.setRight(null);
		_pairs.add(pair);
	}
	
	public static int NumPredicatePairInstancesCreated = 0;
	private static ArrayList<MutablePair<Predicate, Predicate>> _predicatePairs = new ArrayList<MutablePair<Predicate, Predicate>>();
	
	public static MutablePair<Predicate, Predicate> getPredicatePair (Predicate left, Predicate right) {
		
		if(_predicatePairs.isEmpty()) {
			NumPredicatePairInstancesCreated++;
			return new MutablePair<Predicate, Predicate> (left, right);
		}
		
		MutablePair<Predicate, Predicate> pair = _predicatePairs.remove(_predicatePairs.size() -1);
		pair.setLeft(left);
		pair.setRight(right);
		return pair;
	}
	
	public static void putPredicatePair (MutablePair<Predicate, Predicate> pair) {
		
		pair.setLeft(null);
		pair.setRight(null);
		_predicatePairs.add(pair);
	}
	
	public static ArrayList<IntermediateRelLogProp> _intermediateRelLogProps = new ArrayList<IntermediateRelLogProp>();
	
	public static IntermediateRelLogProp getIntermediateRelLogProp (Attribute[] schema, double relSizeTuples) {
		if(_intermediateRelLogProps.isEmpty()) {
			return new IntermediateRelLogProp (schema, relSizeTuples);
		}
		
		IntermediateRelLogProp logProp = _intermediateRelLogProps.remove(_intermediateRelLogProps.size() -1);
		logProp.reset(schema, relSizeTuples);
		return logProp;
		
	}
	
	public static IntermediateRelLogProp getIntermediateRelLogProp (LogicalProperty other) {
		if(_intermediateRelLogProps.isEmpty()) {
			return new IntermediateRelLogProp (other);
		}
		
		IntermediateRelLogProp logProp = _intermediateRelLogProps.remove(_intermediateRelLogProps.size() -1);
		logProp.reset(other);
		return logProp;
		
	}
	
	public static void putLogProp(LogicalProperty logProp) {
		if(logProp instanceof IntermediateRelLogProp) {
			IntermediateRelLogProp intLogProp = (IntermediateRelLogProp)logProp;
			intLogProp.reset(null, 0);
			_intermediateRelLogProps.add(intLogProp);
		}
	}
	
	public static int NumLogicalPropertyArray = 0;
	private static ArrayList<LogicalProperty[]> _logicalPropertyArray_1 = new ArrayList<LogicalProperty[]>();
	private static ArrayList<LogicalProperty[]> _logicalPropertyArray_2 = new ArrayList<LogicalProperty[]>();
	
	public static LogicalProperty[] getLogicalPropertiesArray(int numElements) {

		if (numElements < 1)
			return null;
		
		if(numElements == 1 && !_logicalPropertyArray_1.isEmpty()) {
			return _logicalPropertyArray_1.remove(_logicalPropertyArray_1.size() - 1);
		}
		
		if(numElements == 2 && !_logicalPropertyArray_2.isEmpty()) {
			return _logicalPropertyArray_2.remove(_logicalPropertyArray_2.size() - 1);
		}

		NumLogicalPropertyArray++;
		return new LogicalProperty[numElements];
	}

	public static void putLogicalPropertiesArray(LogicalProperty[] logicalPropertyArray) {
		if (logicalPropertyArray == null)
			return;

		final int numElements = logicalPropertyArray.length;
		if(numElements == 1) {
			_logicalPropertyArray_1.add(logicalPropertyArray);
		}
		
		if(numElements == 2) {
			_logicalPropertyArray_2.add(logicalPropertyArray);
		}

	}
	
	public static int NumPhysicalPropertyArray = 0;
	private static ArrayList<PhysicalProperties[]> _physicalPropertyArray_1 = new ArrayList<PhysicalProperties[]>();
	private static ArrayList<PhysicalProperties[]> _physicalPropertyArray_2 = new ArrayList<PhysicalProperties[]>();
	
	public static PhysicalProperties[] getPhysicalPropertiesArray(PhysicalProperties prop1) {

		PhysicalProperties[] propArray = null;
		
		if(!_physicalPropertyArray_1.isEmpty()) {
			propArray = _physicalPropertyArray_1.remove(_physicalPropertyArray_1.size() - 1);
		}
		else {
			NumPhysicalPropertyArray++;
			propArray = new PhysicalProperties[1];
		}
		
		propArray[0] = prop1;
		return propArray;
	}

	public static PhysicalProperties[] getPhysicalPropertiesArray(PhysicalProperties prop1, PhysicalProperties prop2) {

		PhysicalProperties[] propArray = null;
		
		if(!_physicalPropertyArray_2.isEmpty()) {
			propArray = _physicalPropertyArray_2.remove(_physicalPropertyArray_2.size() - 1);
		}
		else {
			NumPhysicalPropertyArray++;
			propArray = new PhysicalProperties[2];
		}
		
		propArray[0] = prop1;
		propArray[1] = prop2;
		
		return propArray;
	}
	
	public static void putPhysicalPropertiesArray(PhysicalProperties[] physicalPropertyArray) {
		if (physicalPropertyArray == null)
			return;

		final int numElements = physicalPropertyArray.length;
		if(numElements == 1) {
			_physicalPropertyArray_1.add(physicalPropertyArray);
		}
		
		if(numElements == 2) {
			_physicalPropertyArray_2.add(physicalPropertyArray);
		}

	}
	
	public static int NumPhysicalPropertyArrayList = 0;
	private static ArrayList<ArrayList<Object[]>> _physicalPropertyArrayList = new ArrayList<ArrayList<Object[]>>();
	
	public static ArrayList<Object[]> getPhysicalPropertiesArrayList() {

		if(!_physicalPropertyArrayList.isEmpty()) {
			return _physicalPropertyArrayList.remove(_physicalPropertyArrayList.size() - 1);
		}
		
		NumPhysicalPropertyArrayList++;
		return new ArrayList<Object[]>();
	}

	
	public static void putPhysicalPropertiesArray(ArrayList<Object[]> physicalPropertyArray) {
		if (physicalPropertyArray == null)
			return;

		for (Object[] prop : physicalPropertyArray) {
			putPhysicalPropertiesArray ((PhysicalProperties[])prop);
		}
		
		physicalPropertyArray.clear();
		_physicalPropertyArrayList.add(physicalPropertyArray);
	}
}