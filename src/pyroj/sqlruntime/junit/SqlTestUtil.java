package pyroj.sqlruntime.junit;

import pyroj.sqlruntime.expression.AndPredicate;
import pyroj.sqlruntime.expression.EqualsPredicate;
import pyroj.sqlruntime.expression.Expression;
import pyroj.sqlruntime.expression.RelArgRef_t;
import pyroj.sqlruntime.properties.logical.Attribute;
import pyroj.sqlruntime.properties.logical.AttributeProperty;
import pyroj.sqlruntime.properties.logical.BaseRelLogProp;
import pyroj.sqlruntime.properties.logical.IntegerProperty;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

public class SqlTestUtil {
	public static AndPredicate CreateAnd(Expression expr1, Expression expr2) {
		return new AndPredicate(expr1, expr2);
	}

	public static RelArgRef_t CreateColRef(String rel, String col) {
		return new RelArgRef_t(rel, col);
	}

	public static EqualsPredicate CreateEq(Expression expr1, Expression expr2) {
		return new EqualsPredicate(expr1, expr2);
	}

	public static LogicalProperty CreateLogProperties(String relName, String colsStr) {
		Attribute[] schema = CreateSchema(relName, colsStr);
		LogicalProperty logProp = new BaseRelLogProp(relName, schema, 0, 0);

		return logProp;
	}

	public static Attribute[] CreateSchema(String relName, String colsStr) {
		String[] cols = colsStr.split(",");

		Attribute[] schema = new Attribute[cols.length];
		for (int i = 0; i < cols.length; i++) {
			Attribute attr = new Attribute(relName, cols[i], 8, new IntegerProperty(Integer.MIN_VALUE, Integer.MAX_VALUE, 1000));
			schema[i] = attr;
		}
		return schema;
	}

	public static AttributeProperty[] CreateSchemaProperties(Attribute[] schema) {

		int numAttr = schema.length;
		AttributeProperty[] schemaProp = new AttributeProperty[numAttr];

		for (int i = 0; i < numAttr; i++) {
			AttributeProperty attrProp = new IntegerProperty(Integer.MIN_VALUE, Integer.MAX_VALUE, 1000);
			schemaProp[i] = attrProp;
		}
		return schemaProp;
	}

}
