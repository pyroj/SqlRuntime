package pyroj.sqlruntime.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
		pyroj.sqlruntime.operator.physical.algorithmic.junit.AllTests.class,
		pyroj.sqlruntime.rules.transformation.junit.AllTests.class })
public class AllTests {

}
