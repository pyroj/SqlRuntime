package pyroj.sqlruntime.parser;

import java.util.ArrayList;
import java.util.Vector;

import org.apache.derby.iapi.error.StandardException;
import org.apache.derby.iapi.sql.compile.TypeCompiler;
import org.apache.derby.iapi.types.SQLDouble;
import org.apache.derby.iapi.types.SQLInteger;
import org.apache.derby.iapi.types.SQLReal;
import org.apache.derby.impl.sql.compile.AggregateNode;
import org.apache.derby.impl.sql.compile.AllResultColumn;
import org.apache.derby.impl.sql.compile.AndNode;
import org.apache.derby.impl.sql.compile.BinaryArithmeticOperatorNode;
import org.apache.derby.impl.sql.compile.BinaryRelationalOperatorNode;
import org.apache.derby.impl.sql.compile.BooleanConstantNode;
import org.apache.derby.impl.sql.compile.CharConstantNode;
import org.apache.derby.impl.sql.compile.ColumnReference;
import org.apache.derby.impl.sql.compile.ConstantNode;
import org.apache.derby.impl.sql.compile.CursorNode;
import org.apache.derby.impl.sql.compile.FromBaseTable;
import org.apache.derby.impl.sql.compile.FromList;
import org.apache.derby.impl.sql.compile.FromSubquery;
import org.apache.derby.impl.sql.compile.GroupByColumn;
import org.apache.derby.impl.sql.compile.GroupByList;
import org.apache.derby.impl.sql.compile.NotNode;
import org.apache.derby.impl.sql.compile.NumericConstantNode;
import org.apache.derby.impl.sql.compile.OrNode;
import org.apache.derby.impl.sql.compile.QueryTreeNode;
import org.apache.derby.impl.sql.compile.RelationalOperator;
import org.apache.derby.impl.sql.compile.ResultColumn;
import org.apache.derby.impl.sql.compile.ResultColumnList;
import org.apache.derby.impl.sql.compile.ResultSetNode;
import org.apache.derby.impl.sql.compile.SQLParser;
import org.apache.derby.impl.sql.compile.SelectNode;
import org.apache.derby.impl.sql.compile.StatementNode;
import org.apache.derby.impl.sql.compile.ValueNode;

import pyroj.sqlruntime.Config;
import pyroj.sqlruntime.catalog.Catalog_t;
import pyroj.sqlruntime.expression.AndPredicate;
import pyroj.sqlruntime.expression.AttrMapEntry_t;
import pyroj.sqlruntime.expression.AttrMap_t;
import pyroj.sqlruntime.expression.Avg_t;
import pyroj.sqlruntime.expression.ConstantPredicates;
import pyroj.sqlruntime.expression.CountDistinct_t;
import pyroj.sqlruntime.expression.Count_t;
import pyroj.sqlruntime.expression.Div_t;
import pyroj.sqlruntime.expression.Double_t;
import pyroj.sqlruntime.expression.EqualsPredicate;
import pyroj.sqlruntime.expression.Expression.ExpressionType;
import pyroj.sqlruntime.expression.Expression;
import pyroj.sqlruntime.expression.FalsePredicate;
import pyroj.sqlruntime.expression.Float_t;
import pyroj.sqlruntime.expression.Int_t;
import pyroj.sqlruntime.expression.LessThanPredicate;
import pyroj.sqlruntime.expression.Max_t;
import pyroj.sqlruntime.expression.Min_t;
import pyroj.sqlruntime.expression.Minus_t;
import pyroj.sqlruntime.expression.Mult_t;
import pyroj.sqlruntime.expression.NotPredicate;
import pyroj.sqlruntime.expression.OrPredicate;
import pyroj.sqlruntime.expression.Plus_t;
import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.expression.RelArgRef_t;
import pyroj.sqlruntime.expression.Star_t;
import pyroj.sqlruntime.expression.String_t;
import pyroj.sqlruntime.expression.Sum_t;
import pyroj.sqlruntime.expression.TruePredicate;
import pyroj.sqlruntime.operator.logical.AggregateOp;
import pyroj.sqlruntime.operator.logical.GetOp;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.logical.ProjectOp;
import pyroj.sqlruntime.operator.logical.SelectOp;
import pyroj.sqlruntime.operator.logical.UnionOp;
import pyroj.sqlruntime.rules.transformation.SqlExprTree;

public class Parser {
	public static Expression CreateAnd(Expression left, Expression right) {
		assert null != left;
		assert null != right;
		return new AndPredicate(left, right);
	}

	public static Expression CreateEq(Expression left, Expression right) {
		assert null != left;
		assert null != right;
		return new EqualsPredicate(left, right);
	}

	public static Expression CreateGreater(Expression left, Expression right) {
		assert null != left;
		assert null != right;
		return CreateLess(right, left);
	}

	public static Expression CreateGreaterEq(Expression left, Expression right) {
		assert null != left;
		assert null != right;
		return CreateNot(CreateLess(left, right));
	}

	/*
	 * public static Expr_t CreateIn(RelArgRef_t left, Equivalence_t right) {
	 * assert null != left; assert null != right; return new InExpr_t(left,
	 * right); }
	 */

	public static Expression CreateLess(Expression left, Expression right) {
		assert null != left;
		assert null != right;
		return new LessThanPredicate(left, right);
	}

	public static Expression CreateLessEq(Expression left, Expression right) {
		assert null != left;
		assert null != right;
		return CreateNot(CreateGreater(left, right));
	}

	public static Expression CreateNEq(Expression left, Expression right) {
		assert null != left;
		assert null != right;
		return CreateNot(CreateEq(left, right));
	}

	public static Expression CreateNot(Expression inp) {
		assert null != inp;
		return new NotPredicate(inp);
	}

	public static Expression CreateOr(Expression left, Expression right) {
		assert null != left;
		assert null != right;
		return new OrPredicate(left, right);
	}

	private String[] queryString = null;

	public Parser(Catalog_t catalog) {
		Config.setCatalog(catalog);
	}

	public Expression aggregateExprPostOrder(AggregateNode clause) throws Exception {

		AggregateNode an = clause;
		QueryTreeNode qtn = an.getOperand();
		Expression inp = ExprPostOrder(qtn);
		String addDef = an.getAggregateName();
		if (addDef.equals("COUNT")) {
			if (an.isDistinct())
				return new CountDistinct_t(inp);
			else
				return new Count_t(inp);
		}
		else if (addDef.equals("MAX"))
			return new Max_t(inp);
		else if (addDef.equals("MIN"))
			return new Min_t(inp);
		else if (addDef.equals("SUM"))
			return new Sum_t(inp);
		else if (addDef.equals("AVG"))
			return new Avg_t(inp);
		else {
			System.out.print("Invalid Aggregate Operator Type");
			return null;
		}
	}

	public SqlExprTree getLogicalExpressionTree() throws Exception {
		SqlExprTree expr = null;
		SQLParser sqlParser = new SQLParser();
		for (int i = 0; i < queryString.length; i++) {

			StatementNode s = sqlParser.Statement(queryString[i], null);
			ResultSetNode rsNode;
			rsNode = ((CursorNode) s).getResultSetNode();
			// get the expression dag for the statement
			SqlExprTree eq = PostOrder(rsNode);
			if (eq != null) {
				if (expr == null) {
					expr = eq;
				}
				else {
					// create a binary union node as a virtual root
					SqlExprTree rootOp = new SqlExprTree(new UnionOp(2), expr, eq);
					expr = rootOp;
				}
			}
		}

		if (!(expr.getRootOperator() instanceof UnionOp))
			expr = new SqlExprTree(new UnionOp(1), expr);

		if (Config.PrintQuery) {
			System.out.println("\nQuery");
			System.out.println("=====");
			for (int i = 0; i < queryString.length; i++)
				System.out.println(queryString[i]);
			System.out.println("");
		}
		return expr;
	}

	public void SetQueries(ArrayList<String> queries) {
		queryString = new String[queries.size()];
		for (int i = 0; i < queries.size(); i++)
			queryString[i] = queries.get(i);
	}

	private SqlExprTree aggregatePostOrder(ResultSetNode rsNode, SqlExprTree expr) throws Exception {
		GroupByList gbl = ((SelectNode) rsNode).getGroupByList();
		Vector<QueryTreeNode> vgbc = gbl.getNodeVector();
		ResultColumnList rcList = rsNode.getResultColumns();

		AttrMap_t groupByAttrMap = null;
		if (gbl != null) {
			groupByAttrMap = new AttrMap_t(vgbc.size());
			for (int i = 0; i < vgbc.size(); i++) {
				GroupByColumn gbc = (GroupByColumn) vgbc.get(i);
				ColumnReference cr = (ColumnReference) gbc.getColumnExpression();
				String colName = cr.getColumnName();
				String tableName = cr.getTableName();
				Expression groupByExpr = new RelArgRef_t(tableName.toLowerCase(), colName.toLowerCase());
				groupByAttrMap.SetProj(i, new AttrMapEntry_t(groupByExpr, ""));
			}
		}

		AttrMap_t aggAttrMap = new AttrMap_t(rcList.size());
		for (int i = 0; i < rcList.size(); i++) {
			ResultColumn rc = (ResultColumn) rcList.getNodeVector().get(i);
			Expression aggExpr = ExprPostOrder(rc.getExpression());
			aggAttrMap.SetProj(i, new AttrMapEntry_t(aggExpr, ""));
		}
		return new SqlExprTree(new AggregateOp(groupByAttrMap, aggAttrMap), expr);
	}

	private Expression arithmeticExprPostOrder(BinaryArithmeticOperatorNode baoNode) throws Exception {
		Expression left = ExprPostOrder(baoNode.getLeftOperand());
		Expression right = ExprPostOrder(baoNode.getRightOperand());
		String operator = baoNode.getOperator1();
		if (operator.equalsIgnoreCase(TypeCompiler.PLUS_OP))
			return new Plus_t(left, right);
		else if (operator.equalsIgnoreCase(TypeCompiler.MINUS_OP))
			return new Minus_t(left, right);
		else if (operator.equalsIgnoreCase(TypeCompiler.TIMES_OP))
			return new Mult_t(left, right);
		else if (operator.equalsIgnoreCase(TypeCompiler.DIVIDE_OP))
			return new Div_t(left, right);
		else {
			System.out.print("Invalid Arithmetic Operator Type: " + operator);
			return null;
		}
	}

	private Expression constantExprPostOrder(ConstantNode clause) {
		if (clause instanceof NumericConstantNode) {
			if (((NumericConstantNode) clause).getValue() instanceof SQLInteger) {
				return new Int_t(((SQLInteger) ((NumericConstantNode) clause).getValue()).getInt());
			}
			else if (((NumericConstantNode) clause).getValue() instanceof SQLDouble) {
				try {
					return new Double_t((((NumericConstantNode) clause).getValue()).getDouble());
				}
				catch (StandardException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			else if (((NumericConstantNode) clause).getValue() instanceof SQLReal) {
				try {
					return new Float_t((((NumericConstantNode) clause).getValue()).getFloat());
				}
				catch (StandardException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else if (clause instanceof CharConstantNode) {
			try {
				return new String_t(((CharConstantNode) clause).getString());
			}
			catch (StandardException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (clause instanceof BooleanConstantNode) {
			try {
				return (((BooleanConstantNode) clause).getValue()).getBoolean() ? new TruePredicate() : new FalsePredicate();
			}
			catch (StandardException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	private Expression ExprPostOrder(Object clause) throws Exception {
		// TODO Handle name introduced by the "AS" clause in projection
		if (clause == null) {
			return null;
		}
		else if (clause instanceof AggregateNode) {
			AggregateNode an = (AggregateNode) clause;
			return aggregateExprPostOrder(an);
		}
		else if (clause instanceof BinaryArithmeticOperatorNode) {
			BinaryArithmeticOperatorNode baoNode = ((BinaryArithmeticOperatorNode) clause);
			return arithmeticExprPostOrder(baoNode);
		}
		else if (clause instanceof BinaryRelationalOperatorNode) {
			BinaryRelationalOperatorNode broNode = ((BinaryRelationalOperatorNode) clause);
			return relationalExprPostOrder(broNode);
		}
		else if (clause instanceof AndNode) {
			AndNode andNode = ((AndNode) clause);
			if (andNode.getLeftOperand() != null && andNode.getRightOperand() != null) {
				Expression left = ExprPostOrder(andNode.getLeftOperand());
				Expression right = ExprPostOrder(andNode.getRightOperand());
				return CreateAnd(left, right);
			}
		}
		else if (clause instanceof OrNode) {
			OrNode orNode = ((OrNode) clause);
			if (orNode.getLeftOperand() != null && orNode.getRightOperand() != null) {
				Expression left = ExprPostOrder(orNode.getLeftOperand());
				Expression right = ExprPostOrder(orNode.getRightOperand());
				return CreateOr(left, right);
			}
		}
		else if (clause instanceof NotNode) {
			NotNode notNode = ((NotNode) clause);
			if (notNode.getOperand() != null) {
				Expression left = ExprPostOrder(notNode.getOperand());
				return CreateNot(left);
			}
		}
		else if (clause instanceof ConstantNode) {
			return constantExprPostOrder((ConstantNode) clause);
		}
		else if (clause instanceof ColumnReference) {
			ColumnReference columnReference = (ColumnReference) clause;
			String colName = columnReference.getColumnName();
			String tableName = columnReference.getTableName();
			return new RelArgRef_t(tableName.toLowerCase(), colName.toLowerCase());
		}
		/*
		 * TODO Need to handle subquery / nested query else if (clause
		 * instanceof SubqueryNode) { SubqueryNode sqn = (SubqueryNode) clause;
		 * if (sqn.getSubqueryType() == 1 || sqn.getSubqueryType() == 2) { // IN
		 * subquery FromSubquery subq = new FromSubquery();
		 * subq.setSubquery(sqn.getResultSet()); Expr_t left =
		 * ExprPostOrder(sqn.getLeftOperand()); Equivalence_t inEq =
		 * PostOrder(subq.getSubquery()); return CreateIn((RelArgRef_t) left,
		 * inEq);
		 * 
		 * } }
		 */
		return null;
	}

	private SqlExprTree PostOrder(ResultSetNode rsNode) throws Exception {
		FromList fromList = null;
		fromList = rsNode.getFromList();

		Vector<QueryTreeNode> fromTableList = fromList.getNodeVector();

		SqlExprTree childEq = PostOrder(fromTableList);
		// childEq.print();
		ValueNode whereClause = ((SelectNode) rsNode).getWhereClause();
		SqlExprTree expr = selectPostOrder(childEq, whereClause);
		// expr.PrintExpressionDAG(0);
		if (((SelectNode) rsNode).getGroupByList() != null) {
			return aggregatePostOrder(rsNode, expr);
		}
		else {
			return projectPostOrder(rsNode, expr);
		}
	}

	private SqlExprTree FromTablePostOrder(QueryTreeNode baseTable) throws Exception {
		if (baseTable instanceof FromBaseTable) {
			return relationPostOrder((FromBaseTable) baseTable);
		}
		else if (baseTable instanceof FromSubquery) {
			return PostOrder(((FromSubquery) baseTable).getSubquery());
		}
		return null;
	}

	private SqlExprTree PostOrder(Vector<QueryTreeNode> fromTableList) throws Exception {
		QueryTreeNode baseTable = fromTableList.get(0);
		fromTableList.remove(0);

		SqlExprTree leftEq = FromTablePostOrder(baseTable);

		if (fromTableList.size() == 0) {
			return leftEq;
		}
		else {
			Predicate pred = ConstantPredicates.True();
			SqlExprTree rightEq = PostOrder(fromTableList);

			// create the join operator
			return new SqlExprTree(new JoinOp(pred), leftEq, rightEq);
		}
	}

	private SqlExprTree projectPostOrder(ResultSetNode rsNode, SqlExprTree child) throws Exception {
		ResultColumnList rcList = rsNode.getResultColumns();

		AttrMap_t projMap = new AttrMap_t(rcList.size());
		for (int k = 0; k < rcList.size(); k++) {
			Expression projExpr = null;
			if (rcList.getNodeVector().get(k) instanceof AllResultColumn) {
				projExpr = new Star_t();
			}
			else if (rcList.getNodeVector().get(k) instanceof ResultColumn) {
				ResultColumn rc = (ResultColumn) rcList.getNodeVector().get(k);
				ValueNode exp = rc.getExpression();
				projExpr = ExprPostOrder(exp);
			}
			projMap.SetProj(k, new AttrMapEntry_t(projExpr, ""));
		}
		return new SqlExprTree(new ProjectOp(projMap), child);

	}

	private Expression relationalExprPostOrder(BinaryRelationalOperatorNode broNode) throws Exception {
		Expression left = ExprPostOrder(broNode.getLeftOperand());
		Expression right = ExprPostOrder(broNode.getRightOperand());
		int operator = broNode.getOperator();
		switch (operator) {
		case RelationalOperator.EQUALS_RELOP:
			return CreateEq(left, right);
		case RelationalOperator.NOT_EQUALS_RELOP:
			return CreateNEq(left, right);
		case RelationalOperator.LESS_THAN_RELOP:
			return CreateLess(left, right);
		case RelationalOperator.LESS_EQUALS_RELOP:
			return CreateLessEq(left, right);
		case RelationalOperator.GREATER_THAN_RELOP:
			return CreateGreater(left, right);
		case RelationalOperator.GREATER_EQUALS_RELOP:
			return CreateGreaterEq(left, right);
		default:
			break;
		}
		throw new Exception("Invalid Logical Operator Type: " + operator);
	}

	private SqlExprTree relationPostOrder(FromBaseTable fromBaseTable) throws Exception {

		return new SqlExprTree(new GetOp(fromBaseTable.getBaseTableName().toLowerCase()));
	}

	private SqlExprTree selectPostOrder(SqlExprTree child, ValueNode whereClause) throws Exception {
		if (whereClause == null)
			return child;

		Expression expr = ExprPostOrder(whereClause);
		assert expr.getExprType() == ExpressionType.PREDICATE_T;
		Predicate pred = (Predicate) expr;

		// create the select operator
		return new SqlExprTree(new SelectOp(pred), child);
	}
}
