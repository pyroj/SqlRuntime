package pyroj.sqlruntime.rules.enforcer;

import org.apache.commons.lang3.tuple.Pair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.physical.enforcer.RePartitionOp;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;
import pyroj.sqlruntime.properties.physical.PhysicalProperties.PartitionType;

public class RePartitionRule extends PartitionEnforcerRule {
	public RePartitionRule() {
	}

	@Override
	public Pair<Object, Object> getOutputOperator(PhysicalProperties reqdProperties) {

		PhysicalProperties reqdProp = (PhysicalProperties) reqdProperties;
		
		if (reqdProp.getIndexColumns() != null)
			return null;
		if (reqdProp.isSerial() || reqdProp.getPartitionType() == PartitionType.RANDOM)
			return null;
		
		return SqlRuntimeMemoryManager.getPair(null, new RePartitionOp(reqdProp));
	}
}
