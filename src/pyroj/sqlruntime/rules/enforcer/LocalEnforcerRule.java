package pyroj.sqlruntime.rules.enforcer;

import org.apache.commons.lang3.tuple.Pair;

import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public abstract class LocalEnforcerRule {
	
	public abstract Pair<Object, Object> getOutputOperator(PhysicalProperties reqdProperties);

	public static LocalEnforcerRule[] getAllRules() {
		return new LocalEnforcerRule[] { new SortRule() };
	}
}
