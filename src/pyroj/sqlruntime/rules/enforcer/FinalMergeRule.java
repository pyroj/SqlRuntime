package pyroj.sqlruntime.rules.enforcer;

import org.apache.commons.lang3.tuple.Pair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.operator.physical.enforcer.FinalMergeOp;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class FinalMergeRule extends PartitionEnforcerRule {
	public FinalMergeRule() {
	}

	@Override
	public Pair<Object, Object> getOutputOperator(PhysicalProperties reqdProperties) {

		PhysicalProperties reqdProp = (PhysicalProperties) reqdProperties;
		if (reqdProp.getIndexColumns() != null)
			return null;
		if (!reqdProp.isSerial())
			return null;

		return SqlRuntimeMemoryManager.getPair(null, new FinalMergeOp());
	}
}
