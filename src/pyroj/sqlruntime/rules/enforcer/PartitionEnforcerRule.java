package pyroj.sqlruntime.rules.enforcer;

import org.apache.commons.lang3.tuple.Pair;

import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public abstract class PartitionEnforcerRule {

	public abstract Pair<Object, Object> getOutputOperator(PhysicalProperties reqdProperties);

	public static PartitionEnforcerRule[] getAllRules() {
		return new PartitionEnforcerRule[] { 
				new InitialSplitRule(),
				new RePartitionRule(), 
				new FinalMergeRule() };
	}
}