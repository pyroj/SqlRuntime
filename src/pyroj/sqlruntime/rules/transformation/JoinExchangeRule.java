package pyroj.sqlruntime.rules.transformation;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.ConstantPredicates;
import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.properties.logical.GlobalMembersLogProp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

/**
 * (AB) Join (CD) --> (AC) Join (BD) & (BC) Join (AD)
 * 
 * @author sapna
 * 
 */
public class JoinExchangeRule extends TransformationRule {

	/**
	 * (AB) Join (CD) --> (AC) Join (BD) & (BC) Join (AD)
	 */
	public JoinExchangeRule() {

		SqlPatternTree joinAB = new SqlPatternTree(JoinOp.OperatorType,
				new SqlPatternTree(SqlPatternTree.LeafNodeType), new SqlPatternTree(SqlPatternTree.LeafNodeType));
		SqlPatternTree joinCD = new SqlPatternTree(JoinOp.OperatorType,
				new SqlPatternTree(SqlPatternTree.LeafNodeType), new SqlPatternTree(SqlPatternTree.LeafNodeType));
		_inputPattern = new SqlPatternTree(JoinOp.OperatorType, joinAB, joinCD);
		_inputPattern.initializeIds();
	}

	@Override
	public SqlExprTree[] getOutputExpr(Pair<Object, Object>[] binding) {

		JoinOp joinABCD = (JoinOp) binding[_inputPattern.getId()].getLeft();

		if (joinABCD.isTransformationDisabled())
			return null;

		Predicate predABCD = (joinABCD).getJoinPredicate();
		if (predABCD.isEquivalent(ConstantPredicates.True()))
			return null;

		SqlPatternTree patternAB = (SqlPatternTree) _inputPattern.getChild(0);
		SqlPatternTree patternA = (SqlPatternTree) patternAB.getChild(0);
		SqlPatternTree patternB = (SqlPatternTree) patternAB.getChild(1);
		SqlPatternTree patternCD = (SqlPatternTree) _inputPattern.getChild(1);
		SqlPatternTree patternC = (SqlPatternTree) patternCD.getChild(0);
		SqlPatternTree patternD = (SqlPatternTree) patternCD.getChild(1);

		Predicate predAB = ((JoinOp) binding[patternAB.getId()].getLeft()).getJoinPredicate();

		predABCD = Predicate.joinPredicate(predABCD, predAB);

		Predicate predCD = ((JoinOp) binding[patternCD.getId()].getLeft()).getJoinPredicate();

		predABCD = Predicate.joinPredicate(predABCD, predCD);

		LogicalProperty lpABCD = (LogicalProperty) binding[_inputPattern.getId()].getRight();

		Pair<Object, Object> relA = binding[patternA.getId()];
		Pair<Object, Object> relB = binding[patternB.getId()];
		Pair<Object, Object> relC = binding[patternC.getId()];
		Pair<Object, Object> relD = binding[patternD.getId()];

		// two cases due to commutativity of (AB)
		return new SqlExprTree[] { BuildJoinTree(lpABCD, relA, relB, relC, relD, predABCD),
				BuildJoinTree(lpABCD, relB, relA, relC, relD, predABCD) };
	}

	/**
	 * (AB)(CD) --> (AC)(BD)
	 * 
	 * @param eqClassABCD
	 * @param inpA
	 * @param inpB
	 * @param inpC
	 * @param inpD
	 * @param pred
	 *            : overall join predicate
	 * @param memo
	 */
	private SqlExprTree BuildJoinTree(LogicalProperty lpABCD, Pair<Object, Object> relA, Pair<Object, Object> relB,
			Pair<Object, Object> relC, Pair<Object, Object> relD, Predicate pred) {

		// merge the schemas of A and C to get the left table schema
		LogicalProperty schemaAC = GlobalMembersLogProp.MergeLogicalProperties((LogicalProperty) relA.getRight(), (LogicalProperty) relC.getRight());

		// merge the schemas of B and D to get the left table schema
		LogicalProperty schemaBD = GlobalMembersLogProp.MergeLogicalProperties((LogicalProperty) relB.getRight(), (LogicalProperty) relD.getRight());

		// decompose the predicate based on the inner tables' schema
		MutablePair<Predicate, Predicate> newPred = pred.decompose(lpABCD, schemaAC);
		Predicate predABCD = newPred.getLeft();
		Predicate predAC = newPred.getRight();
		SqlRuntimeMemoryManager.putPredicatePair(newPred);

		newPred = predABCD.decompose(lpABCD, schemaBD);
		predABCD = newPred.getLeft();
		Predicate predBD = newPred.getRight();
		SqlRuntimeMemoryManager.putPredicatePair(newPred);

		SqlRuntimeMemoryManager.putLogProp(schemaAC);
		SqlRuntimeMemoryManager.putLogProp(schemaBD);

		JoinOp joinAC = new JoinOp(predAC);
		JoinOp joinBD = new JoinOp(predBD);
		JoinOp joinABCD = new JoinOp(predABCD);

		// to avoid duplicate expressions
		joinABCD.disableTransformation();

		SqlExprTree treeNodeA = new SqlExprTree(relA.getLeft());
		SqlExprTree treeNodeB = new SqlExprTree(relB.getLeft());
		SqlExprTree treeNodeC = new SqlExprTree(relC.getLeft());
		SqlExprTree treeNodeD = new SqlExprTree(relD.getLeft());

		SqlExprTree treeNodeAC = new SqlExprTree(joinAC, treeNodeA, treeNodeC);
		SqlExprTree treeNodeBD = new SqlExprTree(joinBD, treeNodeB, treeNodeD);

		SqlExprTree outputExpr = new SqlExprTree(joinABCD, treeNodeAC, treeNodeBD);

		return outputExpr;
	}

}
