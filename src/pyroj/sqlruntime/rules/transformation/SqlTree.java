package pyroj.sqlruntime.rules.transformation;

public class SqlTree {

	protected SqlTree _parent = null;
	protected final SqlTree[] _children;

	protected int _id;
	protected int _totalNumNodes = -1;

	public SqlTree(SqlTree[] children) {
		this._children = children;
		if (this._children != null)
			for (int i = 0; i < this._children.length; i++)
				this._children[i]._parent = this;
	}

	public int getNumChildren() {
		if (_children == null)
			return 0;
		else
			return _children.length;
	}

	public Object[] getChildren() {
		return _children;
	}

	public SqlTree getChild(int index) {
		return _children[index];
	}

	public SqlTree getParent() {
		return _parent;
	}

	public int getId() {
		return this._id;
	}

	public int getNumNodes() {
		if (this._totalNumNodes == 0)
			initializeIds();
		return this._totalNumNodes;
	}

	public void initializeIds() {
		initializeIds(0);
	}

	private int initializeIds(int id) {
		this._id = id++;

		if (this._children != null) {
			for (int i = 0; i < this._children.length; i++) {
				if (this._children[i] != null)
					id = this._children[i].initializeIds(id);
			}
		}

		this._totalNumNodes = id - this._id;
		return id;
	}
}
