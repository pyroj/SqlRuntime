package pyroj.sqlruntime.rules.transformation;

import org.apache.commons.lang3.tuple.Pair;

import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.logical.SelectOp;

public class PredicatePushDownToJoinRule extends TransformationRule {

	public PredicatePushDownToJoinRule() {
		SqlPatternTree joinNode = new SqlPatternTree(JoinOp.OperatorType, new SqlPatternTree(
				SqlPatternTree.LeafNodeType), new SqlPatternTree(SqlPatternTree.LeafNodeType));
		_inputPattern = new SqlPatternTree(SelectOp.OperatorType, joinNode);
		_inputPattern.initializeIds();
	}

	@Override
	public SqlExprTree[] getOutputExpr(Pair<Object, Object>[] binding) {
		SelectOp selectOp = (SelectOp) binding[_inputPattern.getId()].getLeft();
		SqlPatternTree joinPattern = (SqlPatternTree) _inputPattern.getChild(0);
		JoinOp joinOp = (JoinOp) binding[joinPattern.getId()].getLeft();

		Object child0 = binding[joinPattern.getChild(0).getId()].getLeft();
		Object child1 = binding[joinPattern.getChild(1).getId()].getLeft();

		Predicate predicate = selectOp.getPredicate();

		SqlExprTree output = new SqlExprTree(
				new JoinOp(Predicate.joinPredicate(predicate, joinOp.getJoinPredicate())), new SqlExprTree(child0),
				new SqlExprTree(child1));
		return new SqlExprTree[] { output };
	}

}
