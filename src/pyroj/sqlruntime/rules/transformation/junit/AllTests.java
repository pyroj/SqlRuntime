package pyroj.sqlruntime.rules.transformation.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ JoinAssociativityTest.class, JoinExchangeTest.class })
public class AllTests {

}
