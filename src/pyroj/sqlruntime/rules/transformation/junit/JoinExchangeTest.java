package pyroj.sqlruntime.rules.transformation.junit;

import static org.junit.Assert.fail;

import java.util.BitSet;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.junit.SqlTestUtil;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.rules.transformation.JoinExchangeRule;
import pyroj.sqlruntime.rules.transformation.SqlExprTree;

public class JoinExchangeTest {

	@Test
	public void test() {
		String grpA = new String("groupA");
		String grpB = new String("groupB");
		String grpC = new String("groupC");
		String grpD = new String("groupD");

		LogicalProperty logPropA = SqlTestUtil.CreateLogProperties("A", "a,b,c");
		LogicalProperty logPropB = SqlTestUtil.CreateLogProperties("B", "a,b,c");
		LogicalProperty logPropC = SqlTestUtil.CreateLogProperties("C", "a,b,c");
		LogicalProperty logPropD = SqlTestUtil.CreateLogProperties("D", "a,b,c");

		Predicate predAB = SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef("A", "a"),
				SqlTestUtil.CreateColRef("B", "a"));
		LogicalOp opJoinAB = new JoinOp(predAB);
		LogicalProperty joinABLogProp = opJoinAB
				.computeLogicalProperties(new Object[] { logPropA, logPropB });

		Predicate predCD = SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef("C", "a"),
				SqlTestUtil.CreateColRef("D", "a"));
		LogicalOp opJoinCD = new JoinOp(predCD);
		LogicalProperty joinCDLogProp = opJoinAB
				.computeLogicalProperties(new Object[] { logPropC, logPropD });

		Predicate predABCD = SqlTestUtil.CreateAnd(SqlTestUtil.CreateAnd(
				SqlTestUtil.CreateEq(SqlTestUtil.CreateColRef("A", "a"),
						SqlTestUtil.CreateColRef("C", "a")), SqlTestUtil
						.CreateEq(SqlTestUtil.CreateColRef("A", "a"),
								SqlTestUtil.CreateColRef("D", "a"))),
				SqlTestUtil.CreateAnd(SqlTestUtil.CreateEq(
						SqlTestUtil.CreateColRef("B", "a"),
						SqlTestUtil.CreateColRef("C", "a")), SqlTestUtil
						.CreateEq(SqlTestUtil.CreateColRef("B", "a"),
								SqlTestUtil.CreateColRef("D", "a"))));
		LogicalOp opJoinABCD = new JoinOp(predABCD);
		LogicalProperty joinABCDLogProp = opJoinABCD
				.computeLogicalProperties(new Object[] { joinABLogProp,
						joinCDLogProp });

		@SuppressWarnings("unchecked")
		Pair<Object, Object>[] binding = new MutablePair[7];
		binding[0] = new MutablePair<Object, Object>(opJoinABCD,
				joinABCDLogProp);
		binding[1] = new MutablePair<Object, Object>(opJoinAB, joinABLogProp);
		binding[2] = new MutablePair<Object, Object>(grpA, logPropA);
		binding[3] = new MutablePair<Object, Object>(grpB, logPropB);
		binding[4] = new MutablePair<Object, Object>(opJoinCD, joinCDLogProp);
		binding[5] = new MutablePair<Object, Object>(grpC, logPropC);
		binding[6] = new MutablePair<Object, Object>(grpD, logPropD);

		try {
			JoinExchangeRule rule = new JoinExchangeRule();

			SqlExprTree[] expectedOutput = getExpectedOutput(grpA, grpB, grpC,
					grpD);
			BitSet alreadyMatched = new BitSet(expectedOutput.length);
			System.out.println("\n\nJoin exchange output expression : ");
			SqlExprTree[] outExpr = rule.getOutputExpr(binding);
			for (int k = 0; k < outExpr.length; k++) {

				outExpr[k].print();
				System.out.println();

				boolean found = false;
				for (int i = 0; i < expectedOutput.length; i++) {
					if (!alreadyMatched.get(i)
							&& outExpr[k].equals(expectedOutput[i])) {
						alreadyMatched.set(i);
						found = true;
					}

				}

				if (!found) {
					System.out.println("\nExpected outputs");
					for (int i = 0; i < expectedOutput.length; i++) {
						System.out.println();
						expectedOutput[i].print();
					}
					fail("Un-expected output expression");
				}
			}
			if (alreadyMatched.nextClearBit(0) < expectedOutput.length)
				fail("Number of expressions not same");

		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	private SqlExprTree CreateJoin(String grpA, String grpB, String grpC,
			String grpD, String ArelName, String BrelName, String CRelName,
			String DRelName) {
		Predicate predAC = SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef(ArelName, "a"),
				SqlTestUtil.CreateColRef(CRelName, "a"));
		Predicate predBD = SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef(BrelName, "a"),
				SqlTestUtil.CreateColRef(DRelName, "a"));
		Predicate predABCD = SqlTestUtil.CreateAnd(SqlTestUtil.CreateAnd(
				SqlTestUtil.CreateAnd(SqlTestUtil.CreateEq(
						SqlTestUtil.CreateColRef(ArelName, "a"),
						SqlTestUtil.CreateColRef(DRelName, "a")), SqlTestUtil
						.CreateEq(SqlTestUtil.CreateColRef(BrelName, "a"),
								SqlTestUtil.CreateColRef(CRelName, "a"))),
				SqlTestUtil.CreateEq(SqlTestUtil.CreateColRef(ArelName, "a"),
						SqlTestUtil.CreateColRef(BrelName, "a"))), SqlTestUtil
				.CreateEq(SqlTestUtil.CreateColRef(CRelName, "a"),
						SqlTestUtil.CreateColRef(DRelName, "a")));
		JoinOp joinAC = new JoinOp(predAC);
		JoinOp joinBD = new JoinOp(predBD);
		JoinOp joinABCD = new JoinOp(predABCD);

		SqlExprTree treeNodeA = new SqlExprTree(grpA);
		SqlExprTree treeNodeB = new SqlExprTree(grpB);
		SqlExprTree treeNodeC = new SqlExprTree(grpC);
		SqlExprTree treeNodeD = new SqlExprTree(grpD);

		SqlExprTree treeNodeAC = new SqlExprTree(joinAC, treeNodeA, treeNodeC);
		SqlExprTree treeNodeBD = new SqlExprTree(joinBD, treeNodeB, treeNodeD);

		SqlExprTree outputExpr = new SqlExprTree(joinABCD, treeNodeAC,
				treeNodeBD);

		return outputExpr;
	}

	private SqlExprTree[] getExpectedOutput(String grpA, String grpB,
			String grpC, String grpD) {
		SqlExprTree[] output = new SqlExprTree[2];
		output[0] = CreateJoin(grpA, grpB, grpC, grpD, "A", "B", "C", "D");
		output[1] = CreateJoin(grpB, grpA, grpC, grpD, "B", "A", "C", "D");
		return output;
	}

}
