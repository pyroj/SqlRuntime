package pyroj.sqlruntime.rules.transformation.junit;

import static org.junit.Assert.fail;

import java.util.BitSet;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.junit.SqlTestUtil;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.rules.transformation.JoinAssociativityRule;
import pyroj.sqlruntime.rules.transformation.SqlExprTree;

public class JoinAssociativityTest {

	@Test
	public void test() {

		String grpA = new String("groupA");
		String grpB = new String("groupB");
		String grpC = new String("groupC");

		LogicalProperty logPropA = SqlTestUtil.CreateLogProperties("A", "a,b,c");
		LogicalProperty logPropB = SqlTestUtil.CreateLogProperties("B", "a,b,c");
		LogicalProperty logPropC = SqlTestUtil.CreateLogProperties("C", "a,b,c");

		Predicate predBC = SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef("B", "a"),
				SqlTestUtil.CreateColRef("C", "a"));

		LogicalOp opJoinBC = new JoinOp(predBC);
		LogicalProperty logPropBC = opJoinBC.computeLogicalProperties(new Object[] {
				logPropB, logPropC });

		Predicate predABC = SqlTestUtil.CreateAnd(SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef("A", "a"),
				SqlTestUtil.CreateColRef("B", "a")), SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef("A", "a"),
				SqlTestUtil.CreateColRef("C", "a")));

		LogicalOp opJoinABC = new JoinOp(predABC);
		LogicalProperty logPropABC = opJoinABC.computeLogicalProperties(new Object[] {
				logPropA, logPropBC });

		@SuppressWarnings("unchecked")
		Pair<Object, Object>[] binding = new MutablePair[5];
		binding[0] = new MutablePair<Object, Object>(opJoinABC, logPropABC);
		binding[1] = new MutablePair<Object, Object>(grpA, logPropA);
		binding[2] = new MutablePair<Object, Object>(opJoinBC, logPropBC);
		binding[3] = new MutablePair<Object, Object>(grpB, logPropB);
		binding[4] = new MutablePair<Object, Object>(grpC, logPropC);

		try {
			JoinAssociativityRule rule = new JoinAssociativityRule(false);

			SqlExprTree[] expectedOutput = getExpectedOutput(grpA, grpB, grpC);
			BitSet alreadyMatched = new BitSet(expectedOutput.length);

			System.out.println("\n\nJoin associativity output expression : ");
			SqlExprTree[] outExpr = rule.getOutputExpr(binding);

			for (int j = 0; j < outExpr.length; j++) {

				outExpr[j].print();
				System.out.println();

				boolean found = false;
				for (int i = 0; i < expectedOutput.length; i++) {
					if (!alreadyMatched.get(i)
							&& outExpr[j].equals(expectedOutput[i])) {
						alreadyMatched.set(i);
						found = true;
					}

				}

				if (!found) {
					System.out.println("\nExpected outputs");
					for (int i = 0; i < expectedOutput.length; i++) {
						System.out.println();
						expectedOutput[i].print();
					}
					fail("Un-expected output expression");
				}
			}

			if (alreadyMatched.nextClearBit(0) < expectedOutput.length)
				fail("Number of expressions not same");

		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	private SqlExprTree CreateJoin(String grpA, String grpB, String grpC,
			String ArelName, String BrelName, String cRelName) {

		Predicate predAB = SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef(ArelName, "a"),
				SqlTestUtil.CreateColRef(BrelName, "a"));
		Predicate predABC = SqlTestUtil.CreateAnd(SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef(ArelName, "a"),
				SqlTestUtil.CreateColRef(cRelName, "a")), SqlTestUtil.CreateEq(
				SqlTestUtil.CreateColRef(BrelName, "a"),
				SqlTestUtil.CreateColRef(cRelName, "a")));

		JoinOp joinAB = new JoinOp(predAB);
		JoinOp joinABC = new JoinOp(predABC);

		SqlExprTree treeNodeA = new SqlExprTree(grpA);
		SqlExprTree treeNodeB = new SqlExprTree(grpB);
		SqlExprTree treeNodeC = new SqlExprTree(grpC);

		SqlExprTree treeNodeAB = new SqlExprTree(joinAB, treeNodeA, treeNodeB);

		SqlExprTree outputExpr = new SqlExprTree(joinABC, treeNodeAB, treeNodeC);

		return outputExpr;
	}

	private SqlExprTree[] getExpectedOutput(String grpA, String grpB,
			String grpC) {
		SqlExprTree[] output = new SqlExprTree[2];
		output[0] = CreateJoin(grpA, grpB, grpC, "A", "B", "C");
		output[1] = CreateJoin(grpA, grpC, grpB, "A", "C", "B");
		return output;
	}

}
