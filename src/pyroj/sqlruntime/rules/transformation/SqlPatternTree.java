package pyroj.sqlruntime.rules.transformation;

import pyroj.sqlruntime.operator.logical.LogicalOp;

public class SqlPatternTree extends SqlTree {

	public static int LeafNodeType = -100;

	private final int _rootOperatorType;

	public SqlPatternTree(int rootOperatorType) {
		this(rootOperatorType, (SqlPatternTree[]) null);
	}

	public SqlPatternTree(int rootOperatorType, SqlPatternTree child0) {
		this(rootOperatorType, new SqlPatternTree[] { child0 });
	}

	public SqlPatternTree(int rootOperatorType, SqlPatternTree child0, SqlPatternTree child1) {
		this(rootOperatorType, new SqlPatternTree[] { child0, child1 });
	}

	public SqlPatternTree(int rootOperatorType, SqlPatternTree[] children) {
		super(children);
		this._rootOperatorType = rootOperatorType;
	}

	public boolean isRootMatches(LogicalOp logicalOperator) {
		return _rootOperatorType == logicalOperator.getOperatorType();
	}

	public boolean shouldRootBeLogicalOperator() {
		return _rootOperatorType != LeafNodeType;
	}
}
