package pyroj.sqlruntime.rules.transformation;

import pyroj.sqlruntime.operator.logical.LogicalOp;

public class SqlExprTree extends SqlTree {
	private final Object _root;

	public SqlExprTree(Object rootOperator) {
		this(rootOperator, (SqlExprTree[]) null);
	}

	public SqlExprTree(Object rootOperator, SqlExprTree child0) {
		this(rootOperator, new SqlExprTree[] { child0 });
	}

	public SqlExprTree(Object rootOperator, SqlExprTree child0, SqlExprTree child1) {
		this(rootOperator, new SqlExprTree[] { child0, child1 });
	}

	public SqlExprTree(Object rootOperator, SqlExprTree[] children) {
		super(children);
		_root = rootOperator;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof SqlExprTree))
			return false;

		SqlExprTree otherTree = (SqlExprTree) other;

		if (!_root.equals(otherTree._root))
			return false;

		if (_children == null && otherTree._children != null)
			return false;

		if (_children != null && otherTree._children == null)
			return false;

		if (_children != null) {
			if (_children.length != otherTree._children.length)
				return false;
			for (int i = 0; i < _children.length; i++) {
				if (!_children[i].equals(otherTree._children[i]))
					return false;
			}
		}

		return true;
	}

	public Object getRootOperator() {
		return _root;
	}

	public void print() {
		print(0);
	}

	private void print(int indentLevel) {
		System.out.println();
		for (int i = 0; i < indentLevel; i++)
			System.out.print("| ");
		if (_root instanceof LogicalOp)
			((LogicalOp) _root).print(null);
		else
			System.out.print(_root.toString());
		if (_children != null) {
			for (int i = 0; i < _children.length; i++) {
				((SqlExprTree) _children[i]).print(indentLevel + 1);
			}
		}
	}
}
