package pyroj.sqlruntime.rules.transformation;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.ConstantPredicates;
import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.logical.SelectOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

public class PredicatePushDownFromJoin extends TransformationRule {

	public PredicatePushDownFromJoin() {
		_inputPattern = new SqlPatternTree(JoinOp.OperatorType, new SqlPatternTree(SqlPatternTree.LeafNodeType),
				new SqlPatternTree(SqlPatternTree.LeafNodeType));
		_inputPattern.initializeIds();
	}

	@Override
	public SqlExprTree[] getOutputExpr(Pair<Object, Object>[] binding) {
		JoinOp joinOp = (JoinOp) binding[_inputPattern.getId()].getLeft();
		Predicate joinPred = joinOp.getJoinPredicate();
		if (joinPred.isEquivalent(ConstantPredicates.True()))
			return null;

		Object input0 = binding[_inputPattern.getChild(0).getId()].getLeft();
		Object input1 = binding[_inputPattern.getChild(1).getId()].getLeft();

		LogicalProperty outSchema = ((LogicalProperty) binding[_inputPattern.getId()].getRight());
		LogicalProperty child0Schema = ((LogicalProperty) binding[_inputPattern.getChild(0).getId()].getRight());
		LogicalProperty child1Schema = ((LogicalProperty) binding[_inputPattern.getChild(1).getId()].getRight());

		MutablePair<Predicate, Predicate> newPred = joinPred.decompose(outSchema, child0Schema);
		Predicate newJoinPred = newPred.getLeft();
		Predicate childPred0 = newPred.getRight();
		SqlRuntimeMemoryManager.putPredicatePair(newPred);
		
		newPred = newJoinPred.decompose(outSchema, child1Schema);
		newJoinPred = newPred.getLeft();
		Predicate childPred1 = newPred.getRight();
		SqlRuntimeMemoryManager.putPredicatePair(newPred);

		if (joinPred.isEquivalent(newJoinPred))
			return null;

		SqlExprTree child0 = new SqlExprTree(input0);
		SqlExprTree child1 = new SqlExprTree(input1);

		// TODO: add all other combination as well.
		if (childPred0 != null && !childPred0.isEquivalent(ConstantPredicates.True())) {
			child0 = new SqlExprTree(new SelectOp(childPred0), child0);
		}

		if (childPred1 != null && !childPred1.isEquivalent(ConstantPredicates.True())) {
			child1 = new SqlExprTree(new SelectOp(childPred1), child1);
		}

		JoinOp join = new JoinOp(newJoinPred);

		return new SqlExprTree[] { new SqlExprTree(join, child0, child1) };
	}
}
