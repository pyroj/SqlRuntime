package pyroj.sqlruntime.rules.transformation;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import pyroj.sqlruntime.SqlRuntimeMemoryManager;
import pyroj.sqlruntime.expression.ConstantPredicates;
import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.properties.logical.GlobalMembersLogProp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;

public class JoinAssociativityRule extends TransformationRule {

	private final boolean _isLeftChildJoin;
	
	public JoinAssociativityRule (boolean isLeftChildJoin) {
		_isLeftChildJoin = isLeftChildJoin;

		SqlPatternTree joinBC = new SqlPatternTree(JoinOp.OperatorType,
				new SqlPatternTree(SqlPatternTree.LeafNodeType), new SqlPatternTree(SqlPatternTree.LeafNodeType));
		SqlPatternTree relA = new SqlPatternTree(SqlPatternTree.LeafNodeType);

		if(_isLeftChildJoin) {
			_inputPattern = new SqlPatternTree(JoinOp.OperatorType, joinBC, relA);
		}
		else {
			_inputPattern = new SqlPatternTree(JoinOp.OperatorType, relA, joinBC);
		}
		_inputPattern.initializeIds();
	}
	
	@Override
	public SqlExprTree[] getOutputExpr(Pair<Object, Object>[] binding) {

		JoinOp joinABC = (JoinOp) binding[_inputPattern.getId()].getLeft();
		if (joinABC.isTransformationDisabled())
			return null;

		Predicate predABC = (joinABC).getJoinPredicate();
		LogicalProperty lpABC = (LogicalProperty) binding[_inputPattern.getId()].getRight();
		
		SqlPatternTree patternBC;
		SqlPatternTree patternA;

		if(_isLeftChildJoin) {
			patternBC = (SqlPatternTree) _inputPattern.getChild(0);
			patternA = (SqlPatternTree) _inputPattern.getChild(1);			
		}
		else {
			patternBC = (SqlPatternTree) _inputPattern.getChild(1);
			patternA = (SqlPatternTree) _inputPattern.getChild(0);						
		}
		

		Predicate predBC = ((JoinOp) binding[patternBC.getId()].getLeft()).getJoinPredicate();

		predABC = Predicate.joinPredicate(predABC, predBC);
		if (predABC.isEquivalent(ConstantPredicates.True()))
			return null;

		Object eqA = binding[patternA.getId()].getLeft();
		Object eqB = binding[patternBC.getChild(0).getId()].getLeft();
		Object eqC = binding[patternBC.getChild(1).getId()].getLeft();

		LogicalProperty lpA = (LogicalProperty) binding[patternA.getId()].getRight();
		LogicalProperty lpB = (LogicalProperty) binding[patternBC.getChild(0).getId()].getRight();
		LogicalProperty lpC = (LogicalProperty) binding[patternBC.getChild(1).getId()].getRight();

		SqlExprTree expr_AB_C = createJoinTree (lpABC, lpA, lpB, predABC, eqA, eqB, eqC);
		SqlExprTree expr_AC_B = createJoinTree (lpABC, lpA, lpC, predABC, eqA, eqC, eqB);

		if (expr_AB_C == null) 
			if (expr_AC_B == null)
				return null;
			else
				return new SqlExprTree[] { expr_AC_B };
		else if (expr_AC_B == null)
			return new SqlExprTree[] {expr_AB_C};
		else
			return new SqlExprTree[] {expr_AB_C, expr_AC_B};

	}

	private SqlExprTree createJoinTree(LogicalProperty lpABC, LogicalProperty lpA, LogicalProperty lpB, 
										Predicate pred,
										Object inpA, Object inpB, Object inpC) {

		// merge the schemas of A and B to get the inner table schema
		LogicalProperty schemaAB = GlobalMembersLogProp.MergeLogicalProperties(lpA, lpB);

		MutablePair<Predicate, Predicate> newPred = pred.decompose(lpABC, schemaAB);

		SqlRuntimeMemoryManager.putLogProp(schemaAB);
		
		// PRASAN: don't introduce cross products
		if (newPred.getRight().isEquivalent(ConstantPredicates.True())) {
			SqlRuntimeMemoryManager.putPredicatePair(newPred);
			return null;
		}

		JoinOp joinAB = new JoinOp(newPred.getRight());
		JoinOp joinABC = new JoinOp(newPred.getLeft());
		
		SqlRuntimeMemoryManager.putPredicatePair(newPred);
		
		// to avoid duplicate expressions
		joinABC.disableTransformation();

		SqlExprTree treeNodeA = new SqlExprTree(inpA);
		SqlExprTree treeNodeB = new SqlExprTree(inpB);
		SqlExprTree treeNodeC = new SqlExprTree(inpC);

		SqlExprTree treeNodeAB = new SqlExprTree(joinAB, treeNodeA, treeNodeB);

		SqlExprTree outputExpr = new SqlExprTree(joinABC, treeNodeAB, treeNodeC);

		return outputExpr;
	}
}
