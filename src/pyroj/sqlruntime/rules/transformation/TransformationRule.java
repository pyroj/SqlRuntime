package pyroj.sqlruntime.rules.transformation;

import org.apache.commons.lang3.tuple.Pair;

public abstract class TransformationRule {

	protected SqlPatternTree _inputPattern;

	public SqlPatternTree getInputPattern() {
		return _inputPattern;
	}

	public abstract SqlExprTree[] getOutputExpr(Pair<Object, Object>[] binding);

	public static TransformationRule[] getAllRules() {
		return new TransformationRule[] { new PredicatePushDownToJoinRule(), new PredicatePushDownFromJoin(),
				new JoinAssociativityRule(false), new JoinAssociativityRule(true), new JoinExchangeRule()

		};
	}
}
