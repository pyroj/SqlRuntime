package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.logical.UnionOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.UnionPhyOp;
import pyroj.sqlruntime.operator.physical.algorithmic.UnionPhyOp.PartProp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class UnionImpRuleAllSerial extends ImplementationRule {
	public UnionImpRuleAllSerial() {
		super(UnionOp.OperatorType);
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator,
			LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {
		UnionOp unionOp = (UnionOp) logicalOperator;
		return new UnionPhyOp(unionOp.getNumChildren(), PartProp.AllSerial);

	}

	@Override
	public String getRuleName() {
		return "UnionImpRuleAllSerial";
	}
}
