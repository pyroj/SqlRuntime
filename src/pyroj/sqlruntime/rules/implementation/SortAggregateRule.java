package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.AggregateOp;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.SortAggregateOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class SortAggregateRule extends ImplementationRule {

	public SortAggregateRule() {

		super(AggregateOp.OperatorType);
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator,
			LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {

		PhysicalProperties phyReqdProps = (PhysicalProperties) reqdProperties;
		if (phyReqdProps.getIndexColumns() != null)
			return null;

		AggregateOp aggNode = (AggregateOp) logicalOperator;
		if (!SortAggregateOp.canDeliverProperties(aggNode, logicalProperties,
				childrenlogicalProps, reqdProperties))
			return null;
		return new SortAggregateOp(aggNode.getGroupByAttr(),
				aggNode.getAggAttrMap());
	}

	@Override
	public String getRuleName() {
		return "SortAggregateRule";
	}
}
