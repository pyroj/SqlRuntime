package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.GetOp;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.SequentialScanOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class SequentialScanRule extends ImplementationRule {

	public SequentialScanRule() {
		super(GetOp.OperatorType);
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator,
			LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {

		GetOp getOp = (GetOp) logicalOperator;
		if (!SequentialScanOp.canDeliverProperties(getOp.getRelationName(),
				logicalProperties, reqdProperties))
			return null;
		return new SequentialScanOp(getOp.getRelationName());
	}

	@Override
	public String getRuleName() {
		return "SequentialScanRule";
	}
}
