package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.MergeJoinOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class MergeJoinRule extends ImplementationRule {

	public MergeJoinRule() {
		super(JoinOp.OperatorType);
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator,
			LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {

		JoinOp joinNode = (JoinOp) logicalOperator;

		if (!MergeJoinOp.canDeliverProperties(joinNode, logicalProperties,
				childrenlogicalProps, reqdProperties))
			return null;

		return new MergeJoinOp(joinNode);
	}

	@Override
	public String getRuleName() {
		return "MergeJoinRule";
	}
}
