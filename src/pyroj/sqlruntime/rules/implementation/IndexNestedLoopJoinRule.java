package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.IndexNestedLoopJoin;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class IndexNestedLoopJoinRule extends ImplementationRule {

	private final int _outerRelationIndex;

	public IndexNestedLoopJoinRule(int outerRelationIndex) {
		super(JoinOp.OperatorType);
		_outerRelationIndex = outerRelationIndex;
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator, LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {
		JoinOp joinNode = (JoinOp) logicalOperator;

		if (!IndexNestedLoopJoin
				.canDeliverProperties(joinNode, logicalProperties, childrenlogicalProps, reqdProperties, _outerRelationIndex))
			return null;

		return new IndexNestedLoopJoin(joinNode, _outerRelationIndex);
	}

	@Override
	public String getRuleName() {
		return "IndexNestedLoopJoin<" + _outerRelationIndex + ">";
	}

}
