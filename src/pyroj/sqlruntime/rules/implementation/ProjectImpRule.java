package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.logical.ProjectOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.ProjectImpOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class ProjectImpRule extends ImplementationRule {

	public ProjectImpRule() {
		super(ProjectOp.OperatorType);
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator,
			LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {

		ProjectOp projectNode = (ProjectOp) logicalOperator;
		return new ProjectImpOp(projectNode.getProjectMap());
	}

	@Override
	public String getRuleName() {
		return "ProjectImpRule";
	}
}
