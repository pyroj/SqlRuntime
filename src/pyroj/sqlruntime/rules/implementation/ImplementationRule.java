package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public abstract class ImplementationRule {
	private final int _logicalOperatorType;

	protected ImplementationRule(int logicalOperatorType) {
		_logicalOperatorType = logicalOperatorType;
	}

	public boolean isApplicable(LogicalOp logicalOperator) {
		return _logicalOperatorType == logicalOperator.getOperatorType();
	}

	abstract public PhysicalOp getOutputOperator(LogicalOp logicalOperator, LogicalProperty logicalProperties,
			LogicalProperty[] childrenlogicalProps, PhysicalProperties reqdProperties);

	abstract public String getRuleName();

	public static ImplementationRule[] getAllRules() {
		return new ImplementationRule[] { new MergeJoinRule(), new IndexNestedLoopJoinRule(0),
				new IndexNestedLoopJoinRule(1), new NestedLoopJoinRule(0), new NestedLoopJoinRule(1),
				new ProjectImpRule(), new IndexSelectRule(), new SelectImpRule(), new SequentialScanRule(),
				new SortAggregateRule(), new HashAggregateRule(), new UnionImpRuleAllParallel(),
				new UnionImpRuleLeftSerial(), new UnionImpRuleRightSerial(), new UnionImpRuleAllSerial()

		};
	}
}
