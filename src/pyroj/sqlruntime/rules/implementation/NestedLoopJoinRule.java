package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.JoinOp;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.NestedLoopJoinOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class NestedLoopJoinRule extends ImplementationRule {

	private final int _outerRelationIndex;

	public NestedLoopJoinRule(int outerRelationIndex) {
		super(JoinOp.OperatorType);
		_outerRelationIndex = outerRelationIndex;
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator, LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {
		JoinOp joinNode = (JoinOp) logicalOperator;

		if (!NestedLoopJoinOp.canDeliverProperties(joinNode, logicalProperties, childrenlogicalProps, reqdProperties, _outerRelationIndex))
			return null;

		return new NestedLoopJoinOp(joinNode, _outerRelationIndex);
	}

	@Override
	public String getRuleName() {
		return "NestedLoopJoinRule<" + _outerRelationIndex + ">";
	}
}
