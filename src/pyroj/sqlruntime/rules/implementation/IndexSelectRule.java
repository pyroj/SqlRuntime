package pyroj.sqlruntime.rules.implementation;

import java.util.ArrayList;

import pyroj.sqlruntime.expression.Predicate;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.logical.SelectOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.IndexSelectOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class IndexSelectRule extends ImplementationRule {

	public IndexSelectRule() {
		super(SelectOp.OperatorType);
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator, LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {
		SelectOp selectOp = (SelectOp) logicalOperator;

		PhysicalProperties phyReqdProperties = (PhysicalProperties) reqdProperties;

		int[] indexColumns = computeIndexColumns(selectOp.getPredicate(), (LogicalProperty)childrenlogicalProps[0], phyReqdProperties.getSortColumns());
		if (indexColumns == null)
			return null;
		
		return new IndexSelectOp(selectOp.getPredicate(), indexColumns);
	}

	@Override
	public String getRuleName() {
		return "IndexSelectRule";
	}

	static final ArrayList<Integer> IndexColBinding = new ArrayList<Integer>();

	private static int[] computeIndexColumns(Predicate p, LogicalProperty schema, int[] reqdSortColumns) {
		// predicate

		IndexColBinding.clear();
		boolean isValid = p.findEqToConstantPredicateBinding(schema, IndexColBinding);
		if (!isValid)
			return null;

		if (reqdSortColumns == null) {

			return new int[] { IndexColBinding.get(0) };
		}
		else {
			// index columns will be prefix of sort and index column binding.
			int numColumns = IndexColBinding.size();

			for (int j = 0; j < numColumns; j++)
				if (reqdSortColumns[0] == IndexColBinding.get(j))
					return new int[] { reqdSortColumns[0] };
		}

		return null;
	}
}
