package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.AggregateOp;
import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.HashAggregateOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class HashAggregateRule extends ImplementationRule {

	public HashAggregateRule() {
		super(AggregateOp.OperatorType);
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator,
			LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {

		AggregateOp aggNode = (AggregateOp) logicalOperator;

		if (reqdProperties.getIndexColumns() != null || reqdProperties.getSortColumns() != null)
			return null;

		if (!HashAggregateOp.canDeliverPartitionProperty(aggNode, logicalProperties, childrenlogicalProps[0], reqdProperties))
			return null;
		
		return new HashAggregateOp(aggNode.getGroupByAttr(), aggNode.getAggAttrMap());
	}

	@Override
	public String getRuleName() {
		return "HashAggregateRule";
	}
}
