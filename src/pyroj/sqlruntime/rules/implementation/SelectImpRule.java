package pyroj.sqlruntime.rules.implementation;

import pyroj.sqlruntime.operator.logical.LogicalOp;
import pyroj.sqlruntime.operator.logical.SelectOp;
import pyroj.sqlruntime.operator.physical.PhysicalOp;
import pyroj.sqlruntime.operator.physical.algorithmic.SeqSelectOp;
import pyroj.sqlruntime.properties.logical.LogicalProperty;
import pyroj.sqlruntime.properties.physical.PhysicalProperties;

public class SelectImpRule extends ImplementationRule {

	public SelectImpRule() {
		super(SelectOp.OperatorType);
	}

	@Override
	public PhysicalOp getOutputOperator(LogicalOp logicalOperator,
			LogicalProperty logicalProperties, LogicalProperty[] childrenlogicalProps,
			PhysicalProperties reqdProperties) {

		PhysicalProperties phyReqdProperties = (PhysicalProperties) reqdProperties;
		if (phyReqdProperties.getIndexColumns() != null)
			return null;
		
		SelectOp selectOp = (SelectOp) logicalOperator;
		return new SeqSelectOp(selectOp.getPredicate());
	}

	@Override
	public String getRuleName() {
		return "SelectImpRule";
	}
}
